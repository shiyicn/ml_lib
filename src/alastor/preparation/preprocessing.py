import numpy as np

def min_max_norm(X, outlier_detect=False, threshold=0.05):
    """
    
    Warning : Sensible to extreme outliers

    Normaliza time series within each sample

    Inputs:
        X : (batch_size, sequence_length, features_num)
    
    Outputs:
        X_norm : (batch_size, sequence_length, features_num)

    """
    assert len(X.shape) == 3, "wrong shape"
    if outlier_detect :
        mins, maxs = outlier_detection(X, threshold=threshold) # (features_num, )
    else :
        mins = np.min(np.min(X, axis=0), axis=0) # (features_num, )
        maxs = np.max(np.max(X, axis=0), axis=0) # (features_num, )
    
    # X_norm = (X - mins.reshape(1, 1, -1) ) / (maxs - mins).reshape(1, 1, -1)
    X_norm = (X - mins) / (maxs - mins)

    return X_norm

def outlier_detection(X, threshold=0.05):
    
    features_num = X.shape[-1]
    X_flattened = X.reshape(-1, features_num) # (batch_size * sequence_length, features_num)
    # X_sorted = np.sort(X_flattened, axis=0) # (batch_size * sequence_length, features_num)
    quantiles_max = np.quantile(X_flattened, 1-threshold, axis=0)
    quantiles_min = np.quantile(X_flattened, threshold, axis=0)

    return quantiles_min, quantiles_max

def centred_reduced(X):

    """
    
    Warning : Sensible to extreme outliers

    Normaliza time series within each sample

    Parameters
    ----------
        X : array-like
            An array with shape (batch_size, sequence_length, features_num)
    
    Outputs:
        X_norm : centered and reduced tensor with shape 
                (batch_size, sequence_length, features_num)

    """
    features_num = X.shape[-1]
    X_flattened = X.reshape(-1, features_num)
    means = np.mean(X_flattened, axis=0)
    stds = np.std(X_flattened, axis=0)
    X_norm = (X - means) / stds
    
    return X_norm

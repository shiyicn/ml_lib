"""
This code script contains all functions to manipulate the 
file system in google drive
"""

from pydrive.auth import GoogleAuth
from pydrive.drive import GoogleDrive
from google.colab import auth
from oauth2client.client import GoogleCredentials
import os.path

"""
Authorization Code Snippet Example :

!pip install -U -q PyDrive

auth.authenticate_user()
gauth = GoogleAuth()
gauth.credentials = GoogleCredentials.get_application_default()
my_drive = GoogleDrive(gauth)

"""

def get_file_by_id (drive, target, id):
    token = "'" + id + "'" + " in parents and trashed=false"
    file_list = drive.ListFile({'q': token}).GetList()
    for sub_file in file_list:
        if sub_file['title'] == target:
            return sub_file['id'], sub_file['title']
        else:
            res = get_file_by_id(drive, target, sub_file['id'])
            if res != None:
                return res[0], os.path.join(sub_file['title'], res[1])
    return None

def create_file (drive, target):
  id, file_path = get_file_by_id(drive, target, 'root')
  if id == None:
    print("File %s doesn't exist in Google Drive!" % (target))
  else:
    file = drive.CreateFile({'id': id})
    file.GetContentFile(target)
    print("File %s is loaded with path: %s !" % (target, file_path))
  
  return id == None

def create_file_by_id (drive, id):
  file = drive.CreateFile({'id': id})
  file.GetContentFile(target)

def save_file_in_google_drive (drive, file2save, path2save, id2save = None):
  if id2save is None:
    id, file_path = get_file_by_id(my_drive, path2save, 'root')
  else:
    id = id2save
  f = my_drive.CreateFile({"parents": [{"kind": "drive#fileLink", "id": id}]})
  f.SetContentFile( file2save )
  f.Upload()
  print('Uploaded file with ID {}'.format(f.get('id')))
  return None

# Examples
# create_file(my_drive, 'Aggregation.txt')
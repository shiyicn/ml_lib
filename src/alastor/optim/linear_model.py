import numpy as np
import pandas as pd
import time
import matplotlib.pyplot as plt

class LinearRegression(object):

    def __int__(self, pen = 0.):
        self.pen = pen
        self.mu = None
        self.cov = None
        self.corr = None
    
    def fit_sequential(self, X, y, pen = 0., add_cst = True):
        """
        
        Params:
        -------
        X : array-like, the input matrix
        y : array-like, out-put vector
        
        """

        if self.mu is None:
            self.mu = 0
        if self.cov is None:
            self.cov = 0
        if self.corr is None:
            self.corr = 0
        
        n = X.shape[0]
        d = X.shape[1]
        if add_cst:
            # we add the constant 1 column to the original X
            X = np.c_[X, np.ones(n)]
        
        self.cov += X.T @ X
        self.corr += X.T @ y
        
        w_extended = np.linalg.inv(self.cov) @ self.corr
        if add_cst:
            self.w = w_extended[:-1]
            self.w0 = w_extended[-1]
        else:
            self.w = w_extended
            self.w0 = 0
        self.w_extended = w_extended

        return w_extended

    def fit(self, X, y, pen = 0., add_cst = True):
        """

        Params:
        -------
        X : array-like, shape (n, d)
            n is the num of observations
            d is the dimension of every sample
            the design matrix to be regressed
        y : array-like, shape (n, )
            n is the num of samples, target vector
        pen : numeric value
            pen should be a positive float value
        """
        n = X.shape[0]
        d = X.shape[1]
        if add_cst:
            # we add the constant 1 column to the original X
            X = np.c_[X, np.ones(n)]

        w_extended = np.linalg.inv(X.T @ X) @ X.T @ y
        if add_cst:
            self.w = w_extended[:-1]
            self.w0 = w_extended[-1]
        else:
            self.w = w_extended
            self.w0 = 0
        self.w_extended = w_extended
        return w_extended

    def _coef(self):
        """
        
        Params:
        -------
        
        Output:
        -------
        w : array-like, shape (d, )
            the regressor coefficient for d features
        w0 : numeric value
            the regressor coefficient for the constant value
        
        """
        return self.w, self.w0
    
    def _w_extended(self):
        return self.w_extended

class LinearLASSO(object):

    def __int__(self, pen = 0.):
        self.pen = pen
        self.mu = None
        self.cov = None
        self.corr = None
    
    def fit_sgd(self):
        return None

class AutoRegressiveModel(object):
    def __init__(self, pen = 0.):
        self.pen = pen
        self._coef = None
        self._intercept = None
    
    def fit(self, X, q = 5, lag = 0, add_intercept = False):
        assert len(X.shape) == 1
        assert isinstance(q, int) and q >= 1
        
        total_length = X.shape[0]
        past_length = total_length - q - lag
        past = np.zeros((past_length, q))
        future = np.zeros(past_length)

        for ii in np.arange(past_length):
            past[ii, :] = X[ii:(ii+q)]
            future[ii] = X[ii + q + lag]
        
        regressor = LinearRegression()
        weight_coef = regressor.fit(past, future, pen = self.pen, 
                                    add_cst=add_intercept)

        return weight_coef
    
    def predict(self, X, w, q = None):
        if q is None:
            q = w.shape[0]
        
        prediction = np.convolve(X, w, 'valid')

        return prediction
    
    def sign_accuracy(self, X, w, q = None, lag = 0):
        if q is None:
            q = w.shape[0]
        
        # the other form are not yet supported
        assert lag == 0
        
        prediction = self.predict(X, w)[:-1]
        X2predict = X[q:]
        accuracy = np.sum(np.sign(prediction) == np.sign(X2predict))
        accuracy = accuracy / X2predict.shape[0]
        
        pnl = np.sign(prediction) * X2predict

        return accuracy, pnl


    def simulation(self, N, w, q, lag, add_intercept = False):
        assert isinstance(q, int) and q >= 1
        
        X = np.random.normal(0, 1, N)
        for ii in np.arange(q + lag, N):
            X[ii] = X[(ii - q - lag):ii] @ w + X[ii]
        
        return X

def rolling_windows(a, window):

    """
    This function is imported from the library pyfinance : 
    https://github.com/bsolomon1124/pyfinance/blob/master/pyfinance/utils.py

    Creates rolling-window 'blocks' of length `window` from `a`.
    Note that the orientation of rows/columns follows that of pandas.

    Example

    -------
    import numpy as np
    onedim = np.arange(20)
    twodim = onedim.reshape((5,4))

    print(twodim)
    [[ 0  1  2  3]
     [ 4  5  6  7]
     [ 8  9 10 11]
     [12 13 14 15]
     [16 17 18 19]]

    print(rwindows(onedim, 3)[:5])
    [[0 1 2]
     [1 2 3]
     [2 3 4]
     [3 4 5]
     [4 5 6]]

    print(rwindows(twodim, 3)[:5])
    [[[ 0  1  2  3]
      [ 4  5  6  7]
      [ 8  9 10 11]]

     [[ 4  5  6  7]
      [ 8  9 10 11]
      [12 13 14 15]]

     [[ 8  9 10 11]
      [12 13 14 15]
      [16 17 18 19]]]

    """

    if window > a.shape[0]:
        raise ValueError(
            "Specified `window` length of {0} exceeds length of"
            " `a`, {1}.".format(window, a.shape[0])
        )

    if isinstance(a, (pd.Series, pd.DataFrame)):
        a = a.values

    if a.ndim == 1:
        a = a.reshape(-1, 1)

    shape = (a.shape[0] - window + 1, window) + a.shape[1:]
    strides = (a.strides[0],) + a.strides
    windows = np.squeeze(
        np.lib.stride_tricks.as_strided(a, shape=shape, strides=strides)
    )
    # In cases where window == len(a), we actually want to "unsqueeze" to 2d.
    #     I.e., we still want a "windowed" structure with 1 window.
    if windows.ndim == 1:
        windows = np.atleast_2d(windows)

    return windows

def _rolling_lstsq(x, y):
    coefs = np.squeeze(
                np.matmul(
                    np.linalg.inv(np.matmul(x.swapaxes(1, 2), x)),
                    np.matmul(x.swapaxes(1, 2), np.atleast_3d(y)),
                )
            )
    return coefs


def rolling_regression(X_features, target, learning_window = 256, 
                        offset = 1, flag_separate = False, method = 'direct', 
                        verbose = True):

    if not isinstance(X_features, pd.DataFrame):
        raise TypeError("Input features must be of type pd.DataFrame, "
                        "not {}".format(type(X_features)))
    assert X_features.shape[0] == target.shape[0] # check the shape
    assert offset == 1 # now only lag 1 is supported
    
    """
    # filter out the NaN values 
    if not flag_separate:
        filter_nan = np.logical_and(np.logical_not(np.any(X_features.isna(), axis=1)), 
                                    np.logical_not(target.isna()))
        X_features = X_features.iloc[filter_nan.values, :]
        target = target[filter_nan.values]
    
        assert np.all(np.logical_not(X_features.isna())) & \
            np.all(np.logical_not(target.isna()))
    """

    # check if the features and the target have the same indices
    assert np.all(X_features.index == target.index)

    T = X_features.shape[0]

    # start the rolling regression procedure
    coefs = pd.DataFrame(np.nan, index=X_features.index, 
                         columns=X_features.columns)
    
    preds = pd.Series(np.nan, index=target.index)

    # initialize the covariance matrix with None
    cov_mat_features = None
    
    start_time_rolling = time.time()
    if flag_separate:
        # Essential Calculation for separated linear regression of each features
        """
        `rolling_features` * `rolling_target`
        -------------------------------------
                 `rolling_features_sum`
        """
        _prod = X_features * target.values[..., np.newaxis]
        _square = X_features**2
        coefs_before_shift = _prod.rolling(learning_window-1).sum() / \
                             _square.rolling(learning_window-1).sum()
        # shift the results by one step forward
        coefs = coefs_before_shift.shift(offset)
    else:
        if method == 'direct':
            for i in np.arange(learning_window-1, T):
                range_rolling = np.arange(i-learning_window+1, i-offset+1)
                X_features_rolling = X_features.iloc[range_rolling, :]
                target_rolling = target.iloc[range_rolling]
                
                cov_mat_features = np.dot(X_features_rolling.T, X_features_rolling)
                coefs.iloc[i, :] = np.linalg.inv(cov_mat_features) @ \
                                    X_features_rolling.T @ target_rolling
        elif method == 'rolling':
            X_features_rolling = rolling_windows(X_features, learning_window-1)
            # rolling windows must be at least 2-dimensional
            assert X_features_rolling.ndim >= 2
            if X_features_rolling.ndim == 2:
                X_features_rolling = X_features_rolling[..., np.newaxis]
            target_rolling = rolling_windows(target, learning_window-1)
            
            coefs.values[(learning_window-2):] = _rolling_lstsq(
                        X_features_rolling, target_rolling)[..., np.newaxis]
            coefs = coefs.shift(offset)
        else:
            raise ValueError("Input `method` can be 'direct' or 'rolling'"
                            ", not {0}".format(method))
    if verbose:
        print("Finished Rolling Regression with time cost : [{}s]".format(
                time.time() - start_time_rolling))
    
    if flag_separate:
        preds = coefs * X_features
    else:
        preds = (coefs * X_features).sum(axis=1, skipna=False)
    
    return preds, coefs


def plot_estimation(r_hat, dates = None, pdf2save = None, flag_of_title = '', 
                    force_tilte = None, force_ylabel = None):
    fig, ax = plt.subplots()
    if dates is None:
        ax.plot(r_hat, linewidth=0.6)
    else:
        ax.plot(dates, r_hat, linewidth=0.6)
    
    if force_tilte is None:
        ax.set_title(r'$\hat r$' + ' ' + flag_of_title)
    else:
        ax.set_title(r'' + force_tilte + ' ' + flag_of_title)
    
    if force_ylabel is not None:
        ax.set_ylabel(r'' + force_ylabel)
    ax.grid(linestyle='--')
    if pdf2save is not None:
        print('Plot estimation for {} into a pdf page'.format(flag_of_title))
        fig.savefig(pdf2save, format='pdf', bbox_inches = "tight")
    plt.close()
    return None


# import Exceptions Library
import numpy as np

def prox_norm(x, pen, gamma = 1., norm = 'l2'):
    """
    Proximal Operator for Euclidean Norms

    Formulation : 
    prox_{gamma, f} = inf_{y} f(y) + 1/(2 * gamma) * ||y-x||^2 
    
    f(y) = pen * ||y|| with respect to a certain norm

    Args:
    -----
    x : array-like, input vector
    pen : numerical, double
    norm : string, 'l1' or 'l2'
    
    Output:
    -------
    y : array-like, same shape as x
    
    """
    
    if norm == 'l2':
        # 2 * pen * y + 1 / gamma * (y - x) = 0
        y = x / (2 * pen * gamma + 1.)
    elif norm == 'l1':
        # pen * sign(y) + 1 / gamma * (y - x) = 0
        y = np.sign(x) * np.maximum(np.abs(x) - pen * gamma, 0)
    else:
        raise Exception('The norm type {} is not supported.\n'.format(norm))

    return y    
    
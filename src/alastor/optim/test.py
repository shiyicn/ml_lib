# efficiency.py

import timeit

lon1,lat1,lon2,lat2=-72.345,34.323,-61.823,54.826
num=500000 #调用50万次

t=timeit.Timer("v3.great_circle(%f,%f,%f,%f)"%(lon1,lat1,lon2,lat2),
               "import gibbs_potts as v3")

print('纯c版本(Python函数调用)用时:'+str(t.timeit(num))+'sec')

import numpy as np
from numpy.linalg import inv
from numpy.linalg import eigvals

class NewtonMethod(object):
  
	def __init__(self, Q, p, A, b):
		if not self._check_spd(Q):
			print('The matrix Q is not semi-definite positive!')
		# the matrix Q should be symmetric
		self.Q = Q
		self.p = p
		self.A = A
		self.b = b
	
	def _back_tracking(self, t, x, dx, alpha = 0.4, beta = 0.8):
		_t = 1. # initialize the step length to be 1.
		df = self._gradient(x, t)
		fx = self._f(x, t)
		
		# update the step function
		xx = x + _t * dx
		
		while(self._f(xx, t) >= fx + alpha * _t * dx @ df):
			_t = beta * _t
			xx = x + _t * dx

		return _t * dx
  
	def _check_spd(self, Q):
		return np.all(eigvals(Q) >= 0)
	
	def _f (self, v, t):
		# evaluation function given v and t
		return t * (v @ self.Q @ v + self.p @ v) \
				- np.sum(np.log(self.b - self.A @ v))
  
	def evaluate(self, v):
		return v @ self.Q @ v + self.p @ v
  
	def _gradient(self, x, t):
		# compute the gradient of the target function
		return 2 * t * self.Q @ x + t * self.p + np.sum(self.A.T / (self.b - self.A @ x), axis=1)
	
	def _gradient_second(self, x, t):
		# compute the second derivative
		M = self.A.T / (self.b - self.A @ x)
		return 2 * t * self.Q + t * M @ M.T
	
	def centering_step(self, t, v0, eps):
		v = v0
		norm = np.linalg.norm
		df = self._gradient(v, t)
		H = self._gradient_second(v, t)
		dx = -inv(H) @ df
		history = [v0]
		while(- df @ dx / 2 >= eps):
			H = self._gradient_second(v, t)
			df = self._gradient(v, t)
			dx = -inv(H) @ df
			dx_prime = self._back_tracking(t, v, dx)
			v = v + dx_prime
			history.append(v)
		return v, history
	
	def barr_method(self, v0, eps, mu = 10):
		v = v0
		m = self.A.shape[0]
		history = [v0]
		history_all = []
		t = 1
		while(m / t >= eps):
			v, _history = self.centering_step(t, v, 0.01)
			t = mu * t
			print(self.evaluate(v))
			history.append(v)
			history_all.append(_history)
		return v, history, history_all

n = 100
d = 1000

reg = 10

y = np.random.uniform(size = (n))
X = np.random.uniform(size = (n, d))

Q = np.identity(n) / 2
p = -y
A = np.vstack([X.T, -X.T])
b = np.repeat(reg, 2 * d)

qp = NewtonMethod(Q, p, A, b)
v0 = np.random.uniform(size = (n))

mus = [10, 15, 20, 40, 80, 100]

import matplotlib.pyplot as plt

fig, ax = plt.subplots()
for mu in mus:
	v_, h_, h_all = qp.barr_method(v0, 0.00001, mu = mu)
	hv_ = [qp.evaluate(v) for v in h_]
	hv_ = hv_ - min(hv_)
	ax.semilogy(np.arange(len(hv_)), hv_, label = r'$\mu = $'+str(mu))
ax.legend(loc='upper right')
ax.set_title('Precision Against Iterations')
ax.set_ylabel(r'$f(v_t) - f^*$')
ax.set_xlabel('Iteration')

fig.savefig('visual.pdf')
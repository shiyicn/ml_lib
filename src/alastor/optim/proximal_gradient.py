"""

Implement the proximal gradient descent method

"""

import numpy as np
from tqdm import tqdm
import random
import time

def choice(M, p):
    cum_prob = 0
    u = random.random()
    for i in range(M):
        cum_prob += p[i]
        if cum_prob >= u:
            return i

#from gibbs_potts import choice

class PottsModel(object):
    
    def __init__(self, p, M, theta = None):
        """
        
        Params:
        -------
        p : integer
            dimension of X
        theta : array-like, shape (p, p)
        M : integer
            range of possible x_i
        
        """
        self.theta = theta
        self.p = p
        self.M = M
    
    def B0(self, x, is_zero=True):
        if is_zero:
            return 0
        else:
            return x.astype(float)
    
    def B(self, x):
        if x.ndim is 1:
            BX = (x.reshape(1, -1) == x.reshape(-1, 1))
            BX = BX.astype(float)
            np.fill_diagonal(BX, self.B0(x))
        elif x.ndim is 2:
            BX = [self.B(s) for s in x]
            BX = np.array(BX)
        return BX
    
    def H(self, x):
        return self.B(x)
    
    def GibbsSampler(self, x, i, theta):
        """
        
        Params:
        -------
        x : array-like, shape (p, )
            the initial point for Gibbs sampler algorithm
        i : integer
            the index of the x_i to be sampled
        theta : array-like, shape (p, p)
            the parameters of Potts Model
        
        Output:
        -------
        xi_sample : integer, in {0, ..., M-1}
            the sampled x conditioned on x_-i, via the Potts Model
        
        """
        
        theta_i = theta[i].copy()
        theta_i[i] = 0
        prob = (x == np.arange(self.M).reshape(-1, 1)).dot(theta_i)
        prob = np.exp(prob)
        prob = prob / np.sum(prob)
        xi_sample = choice(self.M, prob)
        return xi_sample
    
    def GibbsSampling(self, x0 = None, T = 100, theta = None):
        """
        
        Params:
        -------
        x0 : array-like, shape (p, )
            the initial point for Gibbs sampling algorithm
        T : integer
            the length of the MCMC series
        theta : array-like, shape (p, p)
            the parameters of Potts Model
        
        Output:
        -------
        X : array-like, shape (p, M)
            the sampled MCMC series via Gibbs sampling
        
        """
        if x0 is None:
            x0 = np.random.choice(self.M, size=self.p)
        X = [x0]
        if theta is None:
            theta = self.theta
        
        for t in range(1, T):
            x_new = X[t-1].copy()
            for i in range(self.p):
                x_new[i] = self.GibbsSampler(x_new, i, theta=theta)
            X.append(x_new)
        
        return np.array(X)

    def _proximal_operator(self, gamma, theta, n_itr, pen=None, norm=1):
        if pen is None:
            pen = 2.5 * np.sqrt(np.log(self.p) / 500)
        
        if norm is 1:
            v = np.maximum(np.abs(theta) - gamma * pen, 0) * np.sign(theta)
        else:
            print('Unsupported norm : {}'.format(norm))
        return v

    def proximal_gradient_descent(self, X, theta0, gamma, n_iter = 10, norm = 1,
                                    algo_flag = 'fix_lr'):
        theta_history = []
        theta = theta0
        N = X.shape[0]
        pen = 2.5 * np.sqrt(np.log(self.p) / 500)
        for n_it in tqdm(range(n_iter)):
            theta_history.append(theta)
            T = 500 # defaut value of T is 500
            if algo_flag is 'fix_lr':
                T = (int) (500 + np.power(n_it, 1.2))
            elif algo_flag is 'fix_batch_size':
                gamma = 25 / self.p / np.power(n_it+1, 0.7)
            else:
                print('Unsupported flag {}'.format(algo_flag))
            # simulate z(theta)
            grad_x = -self.log_likelihood_grad(X, theta=theta, T=T)
            theta = theta - gamma * grad_x
            # update the proximal part
            theta = self._proximal_operator(gamma, theta, n_it+1, pen=pen, norm=norm)
        return theta, np.array(theta_history)

    def log_likelihood(self, X, theta = None, T = None):
        """
        
        Params:
        -------
        X : array-like, shape (N, p)
            all entries should be within {0, 1, ..., M}
        
        Output:
        -------
        log_density : numerical
            the normalisation constant is not included
        """

        if theta is None:
            theta = self.theta
        N = X.shape[0]
        theta_expand = theta[np.newaxis, ...]
        l_theta = 1. / N * np.sum(self.B(X) * theta_expand)
        z_theta_estimated = 1
        return l_theta - np.log(z_theta_estimated)
    
    def log_likelihood_grad(self, X, theta = None, T = None):
        """
        
        Params:
        -------
        X : array-like, shape (N, d)
            observations following the distribution f(theta)
        
        Output:
        -------
        grad_x : array-like, shape (N, d, d)
        
        """
        if theta is None:
            theta = self.theta
        grad_x = np.mean(self.B(X), axis=0) # First part of the gradient is fixed

        x0 = np.random.choice(self.M, self.p) # set up the initial point
        # simulate via Gibbs Sampling to estimate the intractable part
        # of the gradient 
        if T is None:
            T = 500
        X_simu_theta = self.GibbsSampling(x0, theta = theta, T = T)
        grad_z_estimated = np.mean(self.B(X_simu_theta), axis=0)
        
        grad_estimated = grad_x - grad_z_estimated
        return grad_estimated

    def f_theta(self, x, theta = None, log_convert = True):

        """
        Params:
        -------
        x : array-like, shape (N, d)
            input value to compute the probability density
        
        theta : array-like, shape (d, d)
            parameter space for pott model

        Output:
        -------
        density : float
                density proportional to real f_theta
        
        """

        if theta is None:
            theta = self.theta
        B_bar = self.B(x)
        exp_comp = (np.sum(theta * B_bar) + \
                    np.diag(B_bar).dot(np.diag(theta))) / 2.
        if not log_convert:
            density = np.exp(exp_comp)
        else:
            density = exp_comp
        return density
    
    @staticmethod
    def simulate_theta_true(d, p = None):
        if p is None:
            p = 2. / d
        theta_true = np.random.uniform(1, 4, (d, d))
        theta_zero = np.random.choice(2, size=(d, d), p=[1-p, p])
        theta_flag = 2. * np.random.choice(2, size=(d, d)) - 1.
        theta_true = theta_true * theta_zero * theta_flag
        np.fill_diagonal(theta_true, 0) # fill the diagonal elements with 0
        for i in range(d):
            for j in range(i, d):
                theta_true[i, j] = theta_true[j, i]
        return theta_true

from sklearn.metrics import f1_score
      
def Fn(theta_infty, thetas):
    theta_infty = (theta_infty.flatten() == 0).astype(int)
    theta_flags = [(theta.flatten() == 0).astype(int) for theta in thetas]
    def aux(y_true, y_pred):
        return f1_score(y_true, y_pred)
    Fs = [aux(theta_infty, theta) for theta in theta_flags]
    return Fs

def relative_error(theta_infty, thetas):
    dist = [np.sum((x - theta_infty)**2) for x in thetas]
    dist = np.array(dist)
    dist = np.sqrt(dist / np.sum(thetas**2))
    return dist

"""
# A test script for the Pott Model
p = 50
M = 20
theta_true = PottsModel.simulate_theta_true(p)

N = 100
theta0 = np.zeros((p, p))

model = PottsModel(p, M, theta_true)

start_time = time.time()
X = model.GibbsSampling(T = 1200, theta = theta_true)[-N:, ]
elapsed_time = time.time() - start_time
print(elapsed_time)


gamma0 = 25 / p / np.sqrt(50)

theta, theta_history = model.proximal_gradient_descent(X, theta0, gamma0, n_iter = 200)

# Fs = Fn(theta_true, theta_history)
dist = relative_error(theta, theta_history)
"""
import numpy as np
import random

def random_argmax(a):
    """
    
    Params:
    -------
    a : array like, one dimensional
    
    Output:
    -------
    m : integer
        random argmax from array a
    
    """
    return np.random.choice(np.flatnonzero(a == a.max()))

def choice(M, p):
    cum_prob = 0
    u = random.random()
    for i in range(M):
        cum_prob += p[i]
        if cum_prob >= u:
            return i

def UCB1(T, mab, rho = 1):
    """
    
    Params:
    -------
    T : integer 
        the horizon of time 
    mab : a list of arms 
        represented as multi-arms bandit
        an arm must have a function sample to generate a reward
    
    Output:
    -------
    A series of decisions
    
    """

    K = len(mab) # num of arms

    if T <= K:
        print('Not enough simulations!')

    def update_bound(Ns, delta, mus_hat):
        return mus_hat + \
                rho * np.sqrt(np.log(1. / delta) / (2 * Ns))
    
    # count the times that an arm is triggered
    Ns = np.ones(K) # an arm should be trigerred at lease once
    mus_hat = np.zeros(K)
    Bs = np.zeros(K)

    history_reward = []
    history_arm = []

    for i in range(K):
        history_arm.append(i)
        reward = mab[i].sample()
        mus_hat[i] = reward
        history_reward.append(reward)
    
    update_bound(Ns, 1./K, mus_hat)

    for t in range(K, T):
        a_t = random_argmax(Bs)
        history_arm.append(a_t)
        reward = mab[a_t].sample()
        history_reward.append(reward)
        mus_hat[a_t] = (reward + Ns[a_t] * mus_hat[a_t]) / (Ns[a_t] + 1.)
        Ns[a_t] += 1
        Bs = update_bound(Ns, 1./t, mus_hat)
        #Bs[a_t] = B(a_t, Ns[a_t], 1. / t, mus_hat[a_t])
    
    return history_reward, history_arm

def TS(T, mab, sampler = None):
    """
    
    Params:
    -------
    T : integer
        determine the length of the simulation
    mab : list of arms
        each arm must contain the function sample
    
    """
    
    K = len(mab)

    S = np.zeros(K)
    F = np.zeros(K)

    history_reward = []
    history_arm = []

    for t in range(T):
        mus_sample = [np.random.beta(S[i]+1, F[i]+1) for i in range(K)]
        a_t = np.argmax(mus_sample)
        reward = mab[a_t].sample()
        history_arm.append(a_t)
        history_reward.append(reward)
        if reward == 1:
            S[a_t] += 1
        else:
            F[a_t] += 1
    
    return history_reward, history_arm

def lower_bound_Lai_Robbins(P, T = None):
    """
    
    Params:
    -------
    P : array-like, one dimensional
        encoder of the binomial distribution
    
    Output:
    -------
    bound : array_like, one dimensional
        encode the upper bound for bandit problem
    
    """
    
    p_star = np.max(P)
    
    def kl_div(p1, p2):
        return p1 * np.log(p1 / p2) + (1-p1) * np.log((1-p1) / (1- p2))

    bound = 0

    for p in P:
        if p < p_star:
            bound += (p_star - p) / kl_div(p, p_star)
    
    if T is None:
        return bound
    else:
        return bound * np.log(np.arange(T) + 1)

def TS_General(T, mab):
    """
    
    Params:
    -------
    T : integer
        determine the length of the simulation
    mab : list of arms
        each arm must contain the function sample
    
    """
    
    K = len(mab)

    S = np.zeros(K)
    F = np.zeros(K)

    history_reward = []
    history_arm = []

    for t in range(T):
        mus_sample = [np.random.beta(S[i]+1, F[i]+1) for i in range(K)]
        a_t = np.argmax(mus_sample)
        reward_tilde = mab[a_t].sample()
        
        history_arm.append(a_t)
        history_reward.append(reward_tilde)
        reward = np.random.binomial(1, reward_tilde)
        if reward == 1:
            S[a_t] += 1
        else:
            F[a_t] += 1
    
    return history_reward, history_arm

def epsilon_greedy(q, epsilon = 0.1):
    arg_max = np.argwhere(q == np.max(q)).reshape(-1)
    others = np.argwhere(np.logical_and(q != np.max(q), q != -np.inf)).reshape(-1)
    u = np.random.uniform()
    if len(others) <= 0 or u > epsilon:
        a = np.random.choice(arg_max)
    else:
        a = np.random.choice(others)
    return a

class LinUCB(object):
    
    def __init__(self, pen, B, L, d):
        self.pen = pen
        self.B = B
        self.L = L
        self.An = pen * np.identity(d)
        self.bn = np.zeros(d)
        self.d = d
    
    def action(self, model, n, re_init = False, internal_update = False):
        if re_init:
            self.An = self.pen * np.identity(self.d)
            self.bn = np.zeros(self.d)

        An_inv = np.linalg.inv(self.An)
        self.theta_n = An_inv @ self.bn
        r_hat_n = model.features @ self.theta_n
        alpha_n = self._alpha_bound(n, 1. / n, self.theta_n)
        r_n = r_hat_n + alpha_n * \
                np.sqrt( np.sum(model.features @ An_inv * model.features, axis=1) )
        # compute the optimistic arm according to current estimation
        a_n = np.argmax(r_n)
        # get the reward from the model wrapper
        if internal_update :
            reward = model.reward(a_n)
            self.update(model, reward, a_n)
        return a_n
    
    def update(self, model, reward, a):
        phi_n = model.features[[a], :]
        self.An += phi_n.T @ phi_n # update the covariance matrix An
        self.bn += phi_n.squeeze() * reward # update the response vector

    def _alpha_bound(self, n, delta, theta_n):
        """
        
        Params: 
        -------
        n : integer
            the round of time
        delta : float or double value
            the trade-off over the uncertainty
        theta_n : array-like 
            the estimation of the real theta
        
        """
        return self.B * np.sqrt(self.d * np.log( (1 + n * self.L / self.pen) / delta )) + \
                np.sqrt(self.pen) * np.linalg.norm(theta_n)

    @staticmethod
    def run(T, model, pen = 1., B = 1., L = 1.):
        
        """
        
        Params:
        -------
        T : integer
            determine the length of the simulation
        model : list of arms
            each arm must contain the function sample
        
        """

        d = model.n_features

        An = pen * np.identity(d)
        bn = np.zeros(d)

        history_arm = []
        history_reward = []

        def alpha_bound(n, delta, theta_n, B = B, L = L):
            return B * np.sqrt(d * np.log( (1 + n * L / pen) / delta )) + \
                    np.sqrt(pen) * np.linalg.norm(theta_n)

        for n in np.arange(1, T+1):
            An_inv = np.linalg.inv(An)
            theta_n = An_inv @ bn
            r_hat_n = model.features @ theta_n
            alpha_n = alpha_bound(n, 1. / n, theta_n)
            r_n = r_hat_n + alpha_n * \
                    np.sqrt( np.sum(model.features @ An_inv * model.features, axis=1) )
            # compute the optimistic arm according to current estimation
            a_n = np.argmax(r_n)
            # get the reward from the model wrapper
            reward = model.reward(a_n)
            history_reward.append(reward) # register the reward into the list
            history_arm.append(a_n) # register the arm into the list
            phi_n = model.features[[a_n], :]
            An += phi_n.T @ phi_n # update the covariance matrix An
            bn += phi_n.squeeze() * reward # update the response vector

        return history_reward, history_arm

class RandomPolicy(object):

    def __init__(self, d, pen):
        self.d = d
        self.An = pen * np.identity(d)
        self.bn = np.zeros(d)
    
    def action(self):
        return np.random.choice(self.d)
    
    def update(self, model, reward, a):
        phi_n = model.features[[a], :]
        self.An += phi_n.T @ phi_n # update the covariance matrix An
        self.bn += phi_n.squeeze() * reward # update the response vector
        An_inv = np.linalg.inv(self.An)
        self.theta_n = An_inv @ self.bn

class EspilonGreedy(object):

    def __init__(self, d, pen, epsilon):
        self.d = d
        self.pen = pen
        self.epsilon = epsilon
        self.An = pen * np.identity(d)
        self.bn = np.zeros(d)
        self.theta_n = np.zeros(d)
    
    def action(self, model):
        r_hat_n = model.features @ self.theta_n
        return epsilon_greedy(r_hat_n, epsilon=self.epsilon)
    
    def update(self, model, reward, a):
        phi_n = model.features[[a], :]
        self.An += phi_n.T @ phi_n # update the covariance matrix An
        self.bn += phi_n.squeeze() * reward # update the response vector
        An_inv = np.linalg.inv(self.An)
        self.theta_n = An_inv @ self.bn

def lower_bound(p1, p2):
    return None

def DUCB(T, mab, rho = 1, gamma = 0.4):
    """
    
    Params:
    -------
    T : integer 
        the horizon of time 
    mab : a list of arms 
        represented as multi-arms bandit
        an arm must have a function sample to generate a reward
    
    Output:
    -------
    A series of decisions
    
    """

    K = len(mab) # num of arms

    if T <= K:
        print('Not enough simulations!')

    def update_bound(Ns, mus_hat):
        nt_gamma = np.sum(Ns)
        return mus_hat + \
                rho * np.sqrt(np.log(nt_gamma) / (2 * Ns))
    
    # count the times that an arm is triggered
    Ns = np.ones(K) # an arm should be trigerred at lease once
    mus_hat = np.zeros(K)
    Bs = np.zeros(K)

    history_reward = []
    history_arm = []

    for i in range(K):
        history_arm.append(i)
        reward = mab[i].sample()
        mus_hat[i] = reward
        Ns = Ns * gamma
        Ns[i] = Ns[i] + 1.
        history_reward.append(reward)
    
    update_bound(Ns, mus_hat)

    for t in range(K, T):
        a_t = random_argmax(Bs)
        history_arm.append(a_t)
        reward = mab[a_t].sample()
        history_reward.append(reward)
        mus_hat = mus_hat * gamma * Ns
        mus_hat[a_t] = reward + mus_hat[a_t]
        # start to update Ns
        Ns = Ns * gamma
        Ns[a_t] += 1.
        # normalize mus
        mus_hat = mus_hat / Ns
        Bs = update_bound(Ns, mus_hat)
        #Bs[a_t] = B(a_t, Ns[a_t], 1. / t, mus_hat[a_t])
    
    return history_reward, history_arm

def EXP3(T, mab, gamma = 0.4):
    K = len(mab) # num of arms

    if T <= K:
        print('Not enough simulations!')
    
    # count the times that an arm is triggered
    Ns = np.ones(K) # an arm should be trigerred at lease once
    omegas = np.ones(K)
    proba_pick = np.zeros(K)

    history_reward = []
    history_arm = []

    for i in range(K):
        history_arm.append(i)
        reward = mab[i].sample()
        history_reward.append(reward)

    for t in range(K, T):
        tmp_sum_omegas = np.sum(np.exp(omegas))
        proba_pick = (1 - gamma) * np.exp(omegas) / tmp_sum_omegas + gamma / K
        a_t = choice(K, proba_pick) # choose an arm according to the proba_pick distribution
        history_arm.append(a_t) # append selected arm to arm history
        reward = mab[a_t].sample() # sample reward from the arm a_t
        omegas[a_t] += gamma / K * reward / proba_pick[a_t] # update omegas
        omegas -= np.max(omegas)
        history_reward.append(reward) # append reward to rewards history
    
    return history_reward, history_arm

def RExp3(T, mab, gamma = None, Dt = 100, flag_non_stat = False):
    periods = int(T / Dt)
    K = len(mab)

    # count the times that an arm is triggered
    Ns = np.ones(K) # an arm should be trigerred at lease once
    omegas = np.ones(K)
    proba_pick = np.zeros(K)

    history_reward = []
    history_arm = []

    if gamma is None:
        gamma = min(1., np.sqrt(K * np.log(K) / (np.e - 1) / Dt))

    for j in range(periods):
        tau = j * Dt
        omegas = np.zeros(K)
        for t in range(tau, min(T, tau + Dt)):
            tmp_sum_omegas = np.sum(np.exp(omegas))
            proba_pick = (1 - gamma) * np.exp(omegas) / tmp_sum_omegas + gamma / K
            a_t = choice(K, proba_pick) # choose an arm according to the proba_pick distribution
            history_arm.append(a_t) # append selected arm to arm history
            if flag_non_stat:
                for bandit in mab:
                    bandit.update(t, K)
            reward = mab[a_t].sample() # sample reward from the arm a_t
            omegas[a_t] += gamma / K * reward / proba_pick[a_t] # update omegas
            omegas -= np.max(omegas)
            history_reward.append(reward) # append reward to rewards history


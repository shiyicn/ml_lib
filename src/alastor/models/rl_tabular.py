# -*- coding: utf-8 -*-

# Load packages
import numpy as np
import matplotlib.pyplot as plt
import os
import scipy as sp
import seaborn as sns
from time import time

# Value iteration 
class ValueIteration(object):
	def __init__(self, A, S, P, R, gamma):
		self.P = P
		self.R = R
		self.S = S
		self.A = A
		self.gamma = gamma
	
	def run(self, v = None, epsilon = 0.0001):
		if v is None:
			v = np.zeros(self.S.shape[0])
    
		errors = []
		
		while(True):
			v_new = np.max( self.R + self.gamma * self.P.dot(v), axis = 1)
			pi = np.argmax( self.R + self.gamma * self.P.dot(v), axis = 1)
			error = np.linalg.norm(v-v_new, np.inf)
			errors.append(error)
			if (error < epsilon):
				break
			v = v_new
    
		self.v = v
		self.pi = pi
		return v, pi, errors

def random_argmax(X, axis = None):
	if len(X.shape) == 1:
		return np.random.choice(np.flatnonzero(X == X.max()))
  
	if axis == 0:
		arg = [ random_argmax(X[:, i]) for i in range(X.shape[1])]
	elif axis == 1:
		arg = [ random_argmax(X[i, :]) for i in range(X.shape[0])]
	else:
		arg = None
		print("Slice is not supported!\n")
	
	return np.array(arg)

# Value iteration 
class PolicyIteration(object):
	def __init__(self, A, S, P, R, gamma):
		self.P = P
		self.R = R
		self.S = S
		self.A = A
		self.gamma = gamma
	
	def policy_valuation(self, pi, epsilon = 0.0001):
    
		if len(pi.shape) is 1:
			pi_m = np.zeros((len(self.S), len(self.A)))
			pi_m[np.arange(len(self.S)), pi] = 1
			pi = pi_m

		v = np.zeros(self.S.shape[0])
		while(True):
			v_pi = np.sum(pi * self.R + self.gamma * pi * (self.P.dot(v)), axis = 1)
			error = np.linalg.norm(v_pi-v, np.inf)
			v = v_pi
			if (error < epsilon):
				break
		return v
  
	def policy_val_direct(self, pi):
    
		if len(pi.shape) is 1:
			pi_m = np.zeros((len(self.S), len(self.A)))
			pi_m[np.arange(len(self.S)), pi] = 1
			pi = pi_m
		
		P_pi = map(lambda s : self.P[s, :, :].T.dot(pi[s, :]), 
				range(len(self.S)))
		P_pi = np.array(list(P_pi))
		I = np.identity(len(self.S))
	
		return sp.linalg.inv(I - self.gamma * P_pi).dot( np.sum(pi * self.R, axis = 1) )
  
	def run(self, pi = None, epsilon = 0.0001):
		if pi is None:
			pi = np.random.dirichlet(np.ones(len(self.S)), size=len(self.A))
		
		errors = []
		
		v = np.zeros((len(self.S), len(self.A))) + np.inf
		
		while(True):
			v_pi = self.policy_val_direct(pi)
			pi = random_argmax(self.R + self.gamma * self.P.dot(v_pi), axis = 1)
			
			error = np.linalg.norm(v-v_pi, np.inf)
			errors.append(error)
			if (error < epsilon):
				break
			v = v_pi
		
		self.v = v
		self.pi = pi
		return v, pi, errors

def epsilon_greedy(q, epsilon = 0.1):
	arg_max = np.argwhere(q == np.max(q)).reshape(-1)
	others = np.argwhere(np.logical_and(q != np.max(q), q != -np.inf)).reshape(-1)
	u = np.random.uniform()
	if len(others) <= 0 or u > epsilon:
		a = np.random.choice(arg_max)
	else:
		a = np.random.choice(others)
	return a

class Q_learning(object):
	def __init__(self, states, actions, gamma, Q):
		"""

		The Q-Learning algorithm:

		From the Bellman Operator:
		"""
		self.states = states
		self.actions = actions
		self.iter = np.zeros((len(states), len(actions)))
		self.gamma = gamma
		self.Q = Q
  
	def run(self, episode, reset = False):
		if reset:
			self.iter = np.zeros((len(self.states), len(self.actions)))

		for s, a, r, s_next in episode:
			self.step(s, a, r, s, s_next)
  
	def step(self, s, a, r, s_next, const = True):
		if const :
			alpha = 0.1
		else :
			alpha = 1. / (self.iter[s, a] + 1.)
			self.iter[s, a] += 1
		q_max = np.max(self.Q[s_next, :])
		self.Q[s, a] = (1 - alpha) * self.Q[s, a] + alpha * (r + self.gamma * q_max)
  
	def value_function(self):
		v = [ np.max(q) for q in self.Q]
		return np.array(v)

class ApproximationValueFunction(object):
	def __init__(self):
		pass
	

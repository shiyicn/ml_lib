# -*- coding: utf-8 -*-

# Load packages
import numpy as np
import matplotlib.pyplot as plt
import scipy as sp
import matplotlib as mpl
import os

"""

Generative model (LDA). Given the class variable, the data are assumed to be 
Gaussian with different means for different classes but with the same covariance 
matrix.

"""

def qda(X, y, N = None):
  
  """
  
  Params:
  -------
    X : \in R^{N * d} design matrix, numpy matrix 
    y : \in {0, 1}^N target vector, numpy array
    N : \in R, number of observations, it can be none
  """
  if N is None :
    N = len(y)
  
  hat_mu_1 = y.dot(X) / sum(y)
  hat_mu_0 = (1 - y).dot(X) / sum(1 - y)
  
  hat_covariance_1 = 1. / sum(y) * ((X - hat_mu_1).T * y).dot(X - hat_mu_1) 
  hat_covariance_0 = 1. / sum(1-y) * ((X - hat_mu_0).T * (1 - y) ).dot(X - hat_mu_0) 
  
  hat_pi = sum(y) / N
  
  return hat_mu_1, hat_mu_0, hat_covariance_1, hat_covariance_0, hat_pi

def gaussian(X, mu, cov):
    d = X.shape[1]
    precision = sp.linalg.inv(cov)
    x_prime = - 1. / 2. * np.sum((X - mu).dot(precision) * (X - mu), axis = 1)
    return np.power(2 * np.pi, -d / 2.0) * np.sqrt(sp.linalg.det(precision)) * np.exp( x_prime )

def label(X, mu1, mu0, cov1, cov0, pi):
  return ( np.sign(pi * gaussian(X, mu1, cov1) - (1 - pi) * gaussian(X, mu0, cov0)) + 1. ) / 2.

def error_rate(label, y):
  return sum(label == y) / (1. * len(label))

def manifold(mu1, mu0, cov1, cov0, pi):
  Q = cov0 - cov1
  w = mu0.dot(cov0) - mu1.dot(cov1)
  c = mu0.dot(cov0).dot(mu0) - mu1.dot(cov1).dot(mu1) 
  - 2 * np.log((1 - pi) / pi * np.sqrt( np.linalg.det(cov0) / np.linalg.det(cov1) ))
  
  return Q, w, c

def qda_classification (X_test, y_test, X_train, y_train, drive = None, name = "test", path2save = 'res'):
  """
  Wrap the algorithms for Linear Discriminant classification : 
  
  """
  
  # step one compute the estimated terms for LDA from trainset
  hat_mu_1, hat_mu_0, hat_covariance_1, hat_covariance_0, hat_pi = qda(X_train, y_train)
  
  train_predicted = label(X_train, hat_mu_1, hat_mu_0, 
                          hat_covariance_1, hat_covariance_0, hat_pi)
  test_predicted = label(X_test, hat_mu_1, hat_mu_0, 
                          hat_covariance_1, hat_covariance_0, hat_pi)
  
  error_train = sum(train_predicted != y_train) * 1. / len(y_train)
  error_test = sum(test_predicted != y_test) * 1. / len(y_test)
  print("Model Estimation Completed!\n")
  
  id2save = get_file_by_id(drive, path2save, 'root')
  print("Find the target save path for %s with id %s .\n" % (path2save, id2save))
  
  def plotter(X, y, path2save = 'res', name = name, title = 'test'):
    
    # Create the mesh of the grid over x and y
    x_range = np.arange(-7, 8, 0.01)
    y_range = np.arange(-7, 7, 0.01)

    X_mesh, Y_mesh = np.meshgrid(x_range, y_range)
    x_mesh_flattened = np.array([X_mesh.flatten(), Y_mesh.flatten()])
    x_mesh_flattened = x_mesh_flattened.T

    n1 = gaussian(x_mesh_flattened, hat_mu_1, hat_covariance_1)
    n0 = gaussian(x_mesh_flattened, hat_mu_0, hat_covariance_0)

    z_mesh_flattened = hat_pi * n1 / (hat_pi * n1 + (1 - hat_pi) * n0)
    Z_mesh = z_mesh_flattened.reshape(X_mesh.shape)
    
    plt.clf()
    plt.style.use('default')
    colors = y
    plt.contour(X_mesh, Y_mesh, Z_mesh, levels = [0.5], colors = 'green')
    plt.scatter(X[:, 0], X[:, 1], c=colors, s=5)
    
    plt.plot([hat_mu_0[0]], [hat_mu_0[1]], marker='o', markersize=5, color="blue")
    plt.plot([hat_mu_1[0]], [hat_mu_1[1]], marker='o', markersize=5, color="red")
    
    plt.title(title)
    file2save = name + '.pdf'
    plt.savefig(file2save)
    plt.show()
  
  return error_train, error_test

# Load packages
import numpy as np
import matplotlib.pyplot as plt
import os
import scipy as sp
import seaborn as sns
import sys

class HMMGaussian(object):
	def __int__(self):
		self.mu_y2z = None
		self.mu_z2z = None
		self.alpha = None
		self.beta = None

	def run(self, pi, A, mu, cov, y_obs, K, max_iter = 30):
		self.K = K
		self.T = y_obs.shape[0] - 1
		for it in range(max_iter):
			# compute expectations
			density_log = self._density(y_obs, mu, cov, K)
			alpha_log = self._alpha_recursion(pi, A, y_obs, mu, cov, K)
			beta_log = self._beta_recursion(y_obs, A, mu, cov, K)
			tau_log = self._tau(alpha_log, beta_log)
			eta_log = self._eta(alpha_log, beta_log, A, density_log, K)
			
			eta = np.exp(eta_log)
			tau = np.exp(tau_log)
			
			# start to compute the optimal solutions
			pi = tau[0, :]
			A = np.sum(eta, axis=0)
			A = A / np.sum(A, axis=0).reshape(1, K)
			mu = tau.T.dot(y_obs) / np.sum(tau.T, axis=1).reshape(K, 1)
			cov = [ (y_obs-mu[i]).T.dot(tau[:, [i]] * (y_obs-mu[i])) / np.sum(tau[:, i]) \
					for i in range(K) ]

		# update all parameters finally
		self.mu = mu
		self.cov = cov
		self.pi = pi
		self.A = A
		
		return A, pi, density_log
  
	def _alpha_recursion(self, pi, A, y_obs, mu, cov, K):
		"""
		
		Params:
		-------
		pi : array-like, shape (K, )
			multinomial over z0, possible values in {1, ..., K}
		A : array-like, shape (k, K)
			transition kernel, A[i, j] = P(Z'=j | Z=i)
		y_obs : array-like, shape (T, d)
			observations, by default d = 2
			y_obs[t, ] means the t+1 element
		mu : array-like, shape (K, d)
			the center of each custer
		cov : list of numpy array-like, shape (K, d, d)
			the covariance for each cluster
		K : integer
			the num of clusters
		
		Output:
		-------
		alpha_log : array-like, shape (T+1, K)
			the alpha probability in log, despite the zero based coding
			alpha_log[t] does mean the log(alpha_t(z_t))
		
		"""
		
		T = y_obs.shape[0]
		alpha_log = np.zeros((T, K))
		
		# the conditional probability z|y
		density_log = self._density(y_obs, mu, cov, K)
		A_log = np.log(A)
		
		# set up the first row of the alpha recursion
		alpha_log[0, :] = np.log(pi) + density_log[0, :]

		for t in range(1, T):
			sum_tmp = A_log + alpha_log[t-1, :].reshape(1, K)
			b = np.max(sum_tmp, axis=1)
			a = sum_tmp - b.reshape(K, 1)
			a_exp = np.exp(a)
			alpha_log[t, :] = b + density_log[t, :] + \
								np.log(np.sum(a_exp, axis=1))
		self.alpha = alpha_log
		return alpha_log

	def _beta_recursion(self, y_obs, A, mu, cov, K):
		"""
		
		Params:
		-------
		A : array-like, shape (k, K)
			transition kernel, A[i, j] = P(Z'=j | Z=i)
		y_obs : array-like, shape (T, d)
			observations, by default d = 2
			y_obs[t, ] means the t+1 element
		mu : array-like, shape (K, d)
			the center of each custer
		cov : list of numpy array-like, shape (K, d, d)
			the covariance for each cluster
		K : integer
			the num of clusters
		
		Output:
		-------
		beta_log : array-like, shape (T+1, K)
			the beta probability in log
		
		"""
		
		T = y_obs.shape[0]
		
		beta_log = np.zeros((T, K))
		density_log = self._density(y_obs, mu, cov, K)
		
		A_log = np.log(A)

		for i in reversed(range(T-1)):
			sum_tmp = A_log.T + density_log[i+1, :].reshape(1, K) +\
						beta_log[i+1, :].reshape(1, K)
			b = np.max(sum_tmp, axis=1)
			a = sum_tmp - b.reshape(K, 1)
			a_exp = np.exp(a)
			beta_log[i, :] = b + np.log(np.sum(a_exp, axis=1))
		self.beta = beta_log
		return beta_log

	def _p_observations(self, alpha_, beta_, t = 1):
		sum_tmp = alpha_[t, :] + beta_[t, :]
		b = np.max(sum_tmp)
		a = sum_tmp - b
		a_exp = np.exp(a)
		p_log = b + np.log(np.sum(a_exp))
		self.p_log = p_log
		return p_log

	def _tau(self, alpha_log, beta_log):
		p_log = self._p_observations(alpha_log, beta_log)
		tau_log = alpha_log + beta_log - p_log
		self.tau_log = tau_log
		return tau_log

	def _eta(self, alpha_log, beta_log, A, density_log, K):
		# compute the time series length
		T = alpha_log.shape[0] - 1
		A_log = np.log(A)
		p_log = self._p_observations(alpha_log, beta_log)
		alpha_log_t = alpha_log[:T, :].reshape(T, 1, K)
		beta_log_t = beta_log[1:, :].reshape(T, K, 1)
		A_log_t = A_log.reshape(1, K, K)
		density_log_t = density_log[1:, :].reshape(T, K, 1) # density_log contains only
		eta_log = -p_log + alpha_log_t + beta_log_t + A_log_t + density_log_t
		self.eta_log = eta_log
		return eta_log

	def _density(self, y_obs, mu, cov, K):
		T = y_obs.shape[0]
		gaussian = sp.stats.multivariate_normal.pdf
		# compute the emission density
		density = map(lambda k : gaussian(y_obs, mu[k], cov[k]), 
					range(K))
		# compute the conditional density y|z for all z in {1, ..., K}
		density = np.array(density).T
		density_log = np.log(density)
		self.density_log = density_log
		return density_log

	def viterbi(self, A, pi, density_log):
		"""

		Implement the Viterbi Algorithm to compute the most probable sequence

		"""

		# config a dynamic programming sequence dp
		A_log = np.log(A)
		T = density_log.shape[0] - 1
		K = density_log.shape[1]
		dp = [{} for i in range(T+1)] # a mapping : ( t x state ) -> dist
									# with t in {0, ..., T} and state in {1, ..., K}
		path = [{} for i in range(T+1)] # a mapping : (t x state) -> state
									# with t in {0, ...., T-1} and state in {1, ..., K}
		pi_log = np.log(pi)

		# initialize the exist probability
		for j in range(K):
			dp[0][j] = pi_log[j] + density_log[0, j]
			path[0][j] = -1

		def aux(t, state, dp, path, A_log, density_log):
			if dp[t].get(state) is not None:
				return dp[t][state]
			
			dist_current = -sys.maxsize
			for s in range(K):
				# s is the successive state
				assert t > 0
				dist_succ = A_log[state, s] + density_log[t, state] + \
							aux(t-1, s, dp, path, A_log, density_log)
				if dist_succ > dist_current:
					path[t][state] = s
					dp[t][state] = dist_succ
					dist_current = dist_succ
			
			return dp[t][state]

		for state in range(K):
			aux(T, state, dp, path, A_log, density_log)
		
		opt_states = [-1] * (T+1)
		sT = 0
		likelihood = dp[0][sT]
		for s in range(K):
			if dp[T][sT] <= dp[T][s]:
				likelihood = dp[T][s]
				sT = s
		opt_states[T] = sT
		
		for t in reversed(range(1, T+1)):
			s_pred = path[t][opt_states[t]]
			opt_states[t-1] = s_pred
		
		return np.array(opt_states), likelihood, dp

	def plot(self, y_obs, path = None):
		
		if path is None:
			density_log = self._density(y_obs, self.mu, self.cov, self.K)
			path, _tmp1, _tmp2 = self.viterbi(self.A, self.pi, density_log)
		
		palette = sns.color_palette().as_hex()
		palette_array = np.array(palette)

		colors = palette_array[path]
		K = self.K
		fig, ax = plt.subplots()
		ax.scatter(*y_obs.T, c = colors, alpha = 0.5)
		ax.scatter(*self.mu.T, c = palette_array[range(K)], s = 50)
		
		gaussian = sp.stats.multivariate_normal.pdf
		
		def aux_plot(mu, cov, ax, c = None):
			x_range = np.arange(-10, 13, 0.01)
			y_range = np.arange(-10, 13, 0.01)

			X_mesh, Y_mesh = np.meshgrid(x_range, y_range)
			x_mesh_flattened = np.array([X_mesh.flatten(), Y_mesh.flatten()])
			x_mesh_flattened = x_mesh_flattened.T

			z_mesh_flattened = gaussian(x_mesh_flattened, mu, cov)
			Z_mesh = z_mesh_flattened.reshape(X_mesh.shape)
			ax.contour(X_mesh, Y_mesh, Z_mesh, colors = c)
		
		for i in range(K):
			aux_plot(self.mu[i], self.cov[i], ax, c = palette[i])
		
		return fig

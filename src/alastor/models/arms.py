import numpy as np

class AbstractArm(object):
    def __init__(self, mean, variance):
        """
        Args:
            mean: expectation of the arm
            variance: variance of the arm
            random_state (int): seed to make experiments reproducible
        """
        self.mean = mean
        self.variance = variance

    def sample(self):
        pass

class BrownianArm(AbstractArm):
    def __init__(self, x0, volatility, random_state):
        """
        Args
        ----
        x0 : initial state of the arm
        volatility : 

        """

class ArmBernoulli(AbstractArm):
    def __init__(self, p, random_state=0):
        """
        Bernoulli arm
        Args:
             p (float): mean parameter
             random_state (int): seed to make experiments reproducible
        """
        self.p = p
        super(ArmBernoulli, self).__init__(mean=p,
                                           variance=p * (1. - p),
                                           random_state=random_state)

    def sample(self):
        return self.local_random.rand(1) < self.p
    
    def set_p(self, p_new):
        self.p = p_new

class ArmSinuous(AbstractArm):
    def __init__(self, p, random_state=0, bonus=0):
        """
        Bernoulli arm
        Args:
            p (float): mean parameter
            random_state (int): seed to make experiments reproducible
        """
        self.p = p
        self.bonus = bonus
    
    def update(self, r, K):
        # change the probability
        self.p = np.cos(2 * r / 20) / 5. + 0.5 + self.bonus

    def sample(self):
        return np.random.uniform() < self.p

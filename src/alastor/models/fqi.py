import numpy as np
import TP3_python.lqg1d as lqg1d
import matplotlib.pyplot as plt
import TP3_python.utils as utils


class GaussianPolicy(object):
    def __init__(self, theta, sigma):
        self.theta = theta
        self.sigma = sigma
    
    def draw_action(self, state):
        a = np.random.normal(self.theta * state, self.sigma)
        a = max(a, -40)
        a = min(a, 40)
        return a
    
    def log_gradient(self, theta, state, action):
        # compute the gradient of the log policy function
        return (action - theta * state) * state / self.sigma**2
    
    def discretize(self, state, action, s_bound = 20, a_bound = 40):
        s_ = int(state / (s_bound * 2.))
        a_ = int(action / (a_bound * 2.))
        return (s_, a_)

def REINFORCE(trajectory, policy, stepper, params, gamma, with_bonus = False, beta = 1.):
    """
    
    Args:
    -------
    trajectory : a list of episodes, N episodes
        each episode is a dictionary containing {'states', 'actions', 'rewards', 'next_states'}
        each element in the episode is an array of size (T, d) where d is the dimension of the corresponding space
    
    step : an optimizer that wraps the annealing process

    params : the set of parameters to be optimized

    log_grad : a function 
        (params x A x S) -> gradient of log(pi_theta(a|s))

    Output:
    -------
    params: the updated parameters after the knowledge of current episode
    
    """
    tau = len(trajectory[0]['states'])
    N = len(trajectory)
    discount_factors = gamma**(np.arange(tau))

    gt = 0

    beta = 0.1
    
    def discount_sum(rewards):
        discounted_res = 0
        for i in range(len(rewards)):
            discounted_res += rewards[i] * (gamma**i)
        return discounted_res

    # compute the gradient
    for episode in trajectory:
        states = episode['states']
        actions = episode['actions']
        rewards = episode['rewards']

        if with_bonus:
            d_visits = {}
            bonus = np.zeros(tau)
            indices = []
            for i in range(tau):
                index = policy.discretize(states[i], actions[i])
                indices.append(index)
                if d_visits.get(index) is None:
                    d_visits[index] = 1
                else:
                    d_visits[index] += 1
            for i in range(tau):
                bonus[i] = beta / np.sqrt(d_visits[indices[i]])
            rewards = rewards + bonus

        grad = policy.log_gradient(params, states[0], actions[0])
        R = discount_sum(rewards)
        gt += np.sum(grad) * R
    
    gt = gt / N
    
    params = params + stepper.update(gt)

    return params

class ConstantStep(object):
    def __init__(self, learning_rate):
        self.learning_rate = learning_rate

    def update(self, gt):
        return self.learning_rate * gt

#####################################################
# Define the environment and the policy
#####################################################
env = lqg1d.LQG1D(initial_state_type='random')

theta = -0.6
sigma = 0.4

policy = GaussianPolicy(theta, sigma)

#####################################################
# Experiments parameters
#####################################################
# We will collect N trajectories per iteration
N = 100
# Each trajectory will have at most T time steps
T = 100
# Number of policy parameters updates
n_itr = 150
# Set the discount factor for the problem
discount = 0.9
# Learning rate for the gradient update
learning_rate = 3e-4


#####################################################
# define the update rule (stepper)
stepper = ConstantStep(learning_rate) # e.g., constant, adam or anything you want

from tqdm import tqdm

# fill the following part of the code with
#  - REINFORCE estimate i.e. gradient estimate
#  - update of policy parameters using the steppers
#  - average performance per iteration
#  - distance between optimal mean parameter and the one at it k

all_mean_parameters = []
all_avg_return = []

for _ in range(5):
    mean_parameters = []
    avg_return = []
    mean_parameters.append(0)
    mean_param = 0
    return_sum = 0
    for _ in tqdm(range(n_itr)):
        policy.theta = mean_param
        paths = utils.collect_episodes(env, policy=policy, horizon=T, n_episodes=N)
        mean_param = REINFORCE(paths, policy, stepper, mean_param, discount, with_bonus = True)
        for path in paths:
            return_sum += np.sum(path['rewards'])
        mean_parameters.append(mean_param)
        avg_return.append(return_sum / (1. * N * T))
    all_mean_parameters.append(mean_parameters)
    all_avg_return.append(avg_return)

all_param_array = np.array(all_mean_parameters)
all_param_array = np.mean(all_param_array, axis=0)

plt.plot(all_param_array)
for params_ in all_mean_parameters:
  plt.plot(params_, alpha = 0.3)
plt.title("Learning Rate 0.0006, beta = 0.1")
plt.savefig('policy_gradient_beta_2.pdf')
plt.show()
from google.colab import files
files.download('policy_gradient_beta_2.pdf') 

# plot the average return obtained by simulating the policy
# at each iteration of the algorithm (this is a rought estimate
# of the performance
plt.figure()
plt.plot(avg_return)

# plot the distance mean parameter
# of iteration k
plt.figure()
plt.plot(mean_parameters)
import torch
import torch.nn as nn
import torch.nn.functional as F

import numpy as np

nclasses = 20 

class Net(nn.Module):
    def __init__(self):
        super(Net, self).__init__()
        self.conv1 = nn.Conv2d(3, 10, kernel_size=5)
        self.conv2 = nn.Conv2d(10, 20, kernel_size=5)
        self.conv3 = nn.Conv2d(20, 20, kernel_size=5)
        self.fc1 = nn.Linear(320, 50)
        self.fc2 = nn.Linear(50, nclasses)

    def forward(self, x):
        x = F.relu(F.max_pool2d(self.conv1(x), 2))
        x = F.relu(F.max_pool2d(self.conv2(x), 2))
        x = F.relu(F.max_pool2d(self.conv3(x), 2))
        x = x.view(-1, 320)
        x = F.relu(self.fc1(x))
        return self.fc2(x)


def train(epoch):
    model.train()
    for batch_idx, (data, target) in enumerate(train_loader):
        if use_cuda:
            data, target = data.cuda(), target.cuda()
        optimizer.zero_grad()
        output = model(data)
        #criterion = torch.nn.CrossEntropyLoss(reduction='elementwise_mean')
        criterion = torch.nn.CrossEntropyLoss()
        loss = criterion(output, target)
        loss.backward()
        optimizer.step()
        if batch_idx % args.log_interval == 0:
            print('Train Epoch: {} [{}/{} ({:.0f}%)]\tLoss: {:.6f}'.format(
                epoch, batch_idx * len(data), len(train_loader.dataset),
                100. * batch_idx / len(train_loader), loss.data.item()))

def validation():
    model.eval()
    validation_loss = 0
    correct = 0
    for data, target in val_loader:
        if use_cuda:
            data, target = data.cuda(), target.cuda()
        output = model(data)
        # sum up batch loss
        #criterion = torch.nn.CrossEntropyLoss(reduction='elementwise_mean')
        criterion = torch.nn.CrossEntropyLoss()
        validation_loss += criterion(output, target).data.item()
        # get the index of the max log-probability
        pred = output.data.max(1, keepdim=True)[1]
        correct += pred.eq(target.data.view_as(pred)).cpu().sum()

    validation_loss /= len(val_loader.dataset)
    print('\nValidation set: Average loss: {:.4f}, Accuracy: {}/{} ({:.0f}%)\n'.format(
        validation_loss, correct, len(val_loader.dataset),
        100. * correct / len(val_loader.dataset)))

def init_resnet():
    model_conv = torchvision.models.resnet152(pretrained=True)
    for param in model_conv.parameters():
        param.requires_grad = False
    # reset the last convolutional layer
    for param in model_conv.layer4[2].conv3.parameters():
        param.requires_grad = True

    # Parameters of newly constructed modules have requires_grad=True by default
    num_ftrs = model_conv.fc.in_features
    #num_ftrs = model_conv.classifier.in_features
    model_conv.fc = nn.Linear(num_ftrs, nclasses)

    model_conv = model_conv.to(device)
    criterion = nn.CrossEntropyLoss()

    params_to_optimize = list(model_conv.fc.parameters()) + \
                            list(model_conv.layer4[2].conv3.parameters())

    optimizer_conv = optim.Adam(params_to_optimize, lr=0.001)

    # Decay LR by a factor of 0.1 every 20 epochs
    exp_lr_scheduler = lr_scheduler.StepLR(optimizer_conv, step_size=20, gamma=0.1)

    model_conv_best = transfer_learning(model_conv, criterion, optimizer_conv,
                                        exp_lr_scheduler, num_epochs=40)

    return model_conv_best


def get_proba(data_dir, model, data_transforms):
    probabilities = []
    model.eval()
    if use_cuda:
        print('Using GPU')
        model.cuda()
    else:
        print('Using CPU')
    for f in tqdm(os.listdir(data_dir)):
        if 'jpg' in f:
            data = data_transforms(pil_loader(data_dir + '/' + f))
            data = data.view(1, data.size(0), data.size(1), data.size(2))
            if use_cuda:
                data = data.cuda()
            output = model(data)
            pred_proba = output.data.cpu().numpy()
            probabilities.append(pred_proba)
    proba = np.exp(np.array(probabilities)).squeeze()
    proba = proba / np.sum(proba, axis=1).reshape(-1, 1)
    return proba

def feature_extraction(model, dataloader, use_cuda = True):

    device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")

    features = []
    targets = []
    model.eval()
    if use_cuda:
        print('Using GPU')
        model.cuda()
    else:
        print('Using CPU')
    for inputs, labels in dataloader:
        inputs = inputs.to(device)
        labels = labels.to(device)
        output = model(inputs)
        feature = output.data.cpu().numpy()
        features.append(feature)
        targets.append(labels.data.cpu().numpy())
    return np.array(features).squeeze(), np.array(targets).squeeze()

"""
Define functions to help plot figures.

"""

def get_xy_limit(ax, flag = 'max'):
    _, top = ax.get_ylim()
    _, right = ax.get_xlim()
    
    return right, top



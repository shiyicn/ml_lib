import numpy as np

class KernelRidge(object):
    
    def __int__(self, method = 'exact'):
        self.method = 'exact'

    def fit(self, K, y, pen, method = 'exact'):
        """
        Function: 
        ---------
        Solve the following problem 
         
        min 1/n * || K * a - y || + pen/2 * a.T * K * a
         a

        f(x[i]) = <K[i, :], a>

        Args:
        -----
        K : the kernel matrix
        y : the label encoded in {-1, 1}
        pen : penalization coefficient

        Return:
        -------
        a : matrix-like, logistic regression coefficient
        
        """

        n = K.shape[0]
        a = np.linalg.inv(K + pen * n * np.eye(n)).dot(y)
        return a

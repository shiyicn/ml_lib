import numpy as np
import quadprog as qp

class KernelSVM(object):
    def __init__(self):
        self.method = None
        
    def fit(self, K, y, pen, method = 'lambda', thresholdSupp = 1e-5, eps = 1e-6):
        """
        Function: 
        ---------
        Solve the following problem 

        if method = 'lambda'

        min 1/n \sum_{i=1} eta_i + pen * alpha.T * K * alpha
        subject to  y_i * [K * alpha]_i + eta_i - 1 >= 0
                    eta_i >= 0
        
        if method = '2-svm'

        min 1 / n * \sum_{i=1} Hinge(y_i * f(x_i))^2 + pen * ||f||_H^2
        subject to  0<=y_i * alpha_i

        Args:
        -----
        K : the kernel matrix
        y : the label encoded in {-1, 1}
        pen : penalization coefficient
        method : 'lambda' or '2-svm'
        thresholdSupp : threshold for support vector coefficient
        eps : perturbation level

        Return:
        -------
        alpha : matrix-like, svm coefficient
        isSuppVec : boolean, indicating whether a data point is support vector
        
        """
        # check the kernel matrix K is semi-positive definite
        n = K.shape[0]

        """
            Solve a strictly convex quadratic program
            Minimize     1/2 x^T G x - a^T x
            Subject to   C.T x >= b
            Using the quadratic programming package in 
            https://github.com/rmcgibbo/quadprog/blob/master/quadprog/quadprog.pyx
            
            Function arguments:
            solve_qp(G, a, C=None, b=None, meq=0, factorized=False)
        """
        
        if method == 'lambda':
            # Quadratic Programming Formulation
            G = 1. / (2 * pen) * np.diag(y).dot(K).dot(np.diag(y))
            # check if G is positive definite
            if not np.all(np.linalg.eigvals(G) > 0):
                G += eps * np.eye(n)
            a = np.ones(n)
            C = np.concatenate((np.eye(n), -np.eye(n)), axis = 1)
            b = np.concatenate((np.zeros(n), -1. / n * np.ones(n)))
            # solve the problem
            mu = qp.solve_qp(G, a, C, b, meq=0)[0]
            alpha = np.diag(y).dot(mu) / (2 * pen)
        elif method == '2-svm':
            G = 2. * (K + n * pen * np.eye(n))
            a = 2. * y
            C = np.diag(y)
            b = np.zeros(n)
            # solve the optimization problem
            alpha = qp.solve_qp(G, a, C, b, meq=0)[0]
        else:
            print("Unsupported SVM method: {}.\n".format(method))

        # retrieve the support vectors
        isSuppVec = (np.abs(alpha) >= thresholdSupp)
        return alpha, isSuppVec
    
    def predict(self, K_new, a):
        scores = K_new.dot(a)
        return np.sign(scores)

"""
Implementation for Transductive-SVM Kernel Methods
                           L                                  L+2U
minimize  1/2 * <w, w> + C sum H1(y[i] * f_theta(x[i])) + C^* sum R_s(y[i] * f_theta(x[i]))
                           i=1                                i=L+1

with      y[i] = 1                    for all  L+1 <= i <= L+U
          y[i] = -1                   for all  L+U+1 <= i <= L+2*U
          x[i] = x[i-U]

                              L+U                      L
balancing constraints:  1/U * sum  f_theta(x[i]) = 1/L sum y[i]
                              i=L+1                    i=1

where H1(z) = max(0, 1 - z) is the normal Hinge Loss, and R_s is the Ramp Loss 
                R_s(z) = min(1-s, max(0, 1 - z))

introduce an extra Lagrangian variable a0 and an example x[0] implicitly defined 
by 
                L+U
Phi(x[0]) = 1/U sum  Phi(x[i])
                i=L+1
                              L+U
Thus, K[i, 0] = K[0, i] = 1/U sum  Phi(x[j])Phi(x[i])
                              j=L+1
"""

import quadprog as qp
import numpy as np
import timeit

class KernelTSVM(object):
    def __init__(self, method = 'cccp'):
        self.method = method

    def fit(self, K, C, y, L, s, U, Cstar, coef_init = None, b0 = None, 
            fit_intercept = False, max_iter = 5):
        """
        
        Args:
        -----
        K : array-like, kernel matrix
            shape (L+2*U+1, L+2*U+1)
        C : numerical value, trade-off on Hinge loss
        Cstart : numerical value, trade-off on Ramp loss
        y : array-like, label in {-1, 1}
            shape (L+2*U+1, )
        s : numerical value, Ramp loss R_s hyperparameter
        
        See Reference in the paper: Large Scale Transductive SVMs
        
        """
        f_theta = lambda K, coef : K.dot(coef)
        N = L + 2 * U + 1 # total number of samples

        if K.shape[0] != N or y.shape[0] != N:
            print('Shape of K or y is not correct, K shape {}, y \
                   shape {} while N ={}'.format(K.shape, y.shape, N))

        if fit_intercept :
            print('Intercept is not yet supported!')

        # initialization
        if coef_init is not None:
            coef = coef_init
        else:
            coef = np.zeros(N)

        beta0 = (y * f_theta(K, coef) < s) * Cstar
        beta0[:(L+1)] = 0
        
        eta = y
        eta[0] = np.mean(y[1:(L+1)])

        beta = beta0 # initialize beta with beta0
        
        for i in range(max_iter):
            # solve the convex optimization problem
            """
            max (<a, eta> - 1/2 * a.T * K * a)
             a
            
            s.t.    <a, 1> = 0
                    0 <= y[i] * a[i] <= C                        for all i<=L
                    -beta[i] <= y[i] * a[i] <= Cstar - beta[i]   for all i >= L+1

            Solve a strictly convex quadratic program
            Minimize     1/2 x^T G x - a^T x
            Subject to   C.T x >= b
            Using the quadratic programming package in 
            https://github.com/rmcgibbo/quadprog/blob/master/quadprog/quadprog.pyx
            
            Function arguments:
            solve_qp(G, a, C=None, b=None, meq=0, factorized=False)
            """
            G_opt = -K
            a_opt = -eta
            meq_opt = 1
            C_opt_p1 = None
            C_opt = np.concatenate(
                (np.ones(N, 1), np.diag(y), -np.diag(y)), 
                axis = 1
            )
            b_opt = np.concatenate(
                (0, np.zeros(L+1), beta[L:], -C * np.ones(L+1), -(Cstar - beta[L:])), 
                axis = 0
            )

            print('Start the iteration : [{}/{}], try to solve the QP.'.format(i, max_iter))
            start_time = timeit.time()
            coef = qp.solve_qp(G_opt, a_opt, C = C_opt, b = b_opt, meq = meq_opt, 
                        factorized = False)
            elapsed_time = timeit.time() - start_time
            print('Time cost: {}s'.format(elapsed_time))
            
            # compute beta
            beta = (y * f_theta(K, coef) < s) * Cstar
            beta[:(L+1)] = 0

        return None

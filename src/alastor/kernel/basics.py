import numpy as np

def sigmoid(X):
    X = np.maximum(X, -40)
    return 1. / (1. + np.exp(-X))

import numpy as np

def gaussian_kernel(X, sigma, X_new = None):
    if X_new is None:
        X_new = X
    pattern_X = np.sum(X * X, 1)
    pattern_X = pattern_X.reshape(-1, 1)
    pattern_X_new = np.sum(X_new * X_new, 1)
    pattern_X_new = pattern_X_new.reshape(-1, 1)
    K = np.exp(-(pattern_X.T + pattern_X_new - 2 * 
                X_new.dot(X.T)) / (2 * sigma**2))
    return K

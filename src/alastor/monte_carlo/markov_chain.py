import numpy as np
from scipy.stats import multivariate_normal
from basic import normal_log, normal_multivariate_log

class MarkovChain(object):
    def __init__(self):
        pass

    def next(self, x):
        pass

class GaussianMarkovChain(MarkovChain):
    def __init__(self, cov, mu = 0):
        self.cov = cov
        self.mu = mu

    def next(self, x, mu = None, cov = None):
        if mu is None:
            mu = self.mu
        if cov is None:
            cov = self.cov
        if type(x) is not 'numpy.ndarray':
            dt = np.random.normal(mu, cov)
        else:
            dt = np.random.multivariate_normal(mu, cov)
        return x + dt

    def kernel(self, x, z):
        if type(x) is 'numpy.ndarray':
            gaussian = multivariate_normal.pdf
        else:
            gaussian = lambda x, mu, cov : \
                        1./np.sqrt(2 * np.pi) * np.exp(-(x - mu)**2 / 2. / cov)
        density = gaussian(z - x, self.mu, self.cov)
        return np.log(density)

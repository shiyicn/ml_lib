import numpy as np

def normal_log(X, mean, sigma):
    pi = np.pi
    log_density = -0.5 * np.log(pi) - np.log(sigma) - \
                    -0.5 / sigma**2 * (X - mean)**2
    return log_density

def normal_multivariate_log(X, mean, cov):
    assert type(X) is np.ndarray
    assert type(mean) is np.ndarray
    assert type(cov) is np.ndarray
    d = cov.shape[0]
    P = np.linalg.inv(cov) # compute the inverse the covariance matrix
    X_prime = X - mean
    log_density = -0.5 * d * np.log(np.pi) + np.log(np.linalg.det(P)) - \
                    0.5 * X_prime * (X_prime.dot(P))
    return np.sum(log_density, axis=1)

def uniformize(X):
    """
    
    Params:
    -------
    X : array like, shape (-1, )

    Output:
    -------
    u : array like, shape (-1, ), same as the input X

    """
    return None

    
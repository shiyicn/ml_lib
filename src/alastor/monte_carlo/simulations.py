import numpy as np
import random


def choice(M, p):
    cum_prob = 0
    u = random.random()
    for i in range(M):
        cum_prob += p[i]
        if cum_prob >= u:
            return i


def poisson_proc(N, lam):
    """
    Simulate the standard poisson point process (counting process)

    Args:
    -----
    N : integer, 
        the length of the signal to be simulated
    lam : float, 
        the parameter for the exponential distribution used for the simulation
    
    Returns:
    --------
    Y : array-like, shape (T+1, )
        the counting at each time point, starting from 0
    
    """
    arrivalTime = 0
    X = np.zeros(N+1)
    while True:
        arrivalTime += np.random.exponential(lam)
        if arrivalTime < N:
            X[(int) (np.ceil(arrivalTime))] = 1
        else:
            break
    
    return np.cumsum(X)


def sign_proc(N, lam, p = 0.5):
    """
    Simulation of a signed Exponential Arrival Time Process

    Args:
    -----
    N : integer, length of the increment series
    lam : float, parameter of the exponential distribution
    p : float, parameter of the Bernoulli distribution
    
    Returns:
    --------
    X : array-like, shape (N+1, )
        the simulated time series
    """
    arrivalTime = 0
    X = np.zeros(N+1)
    sgn = np.random.random(N+1)
    sgn = 2 * (sgn <= p) - 1
    while True:
        arrivalTime += np.random.exponential(lam)
        if arrivalTime < N:
            X[(int) (np.ceil(arrivalTime))] = 1
        else:
            break
    
    X = X * sgn

    return np.cumsum(X)


def piece_wise_constant(N, sigma_duration = 5, sigma_amplitude = 0.01, 
                        fixed_duration = None):
    """
    Args:
    -----
    N : integer, the length of the time-series
    sigma_duration : float, the standard deviation of the gaussian for duration
    sigma_amplitude : float, the standard deviation of the gaussian distribution 
                      for the amplitude

    Returns:
    --------
    


    """
    X = np.zeros(N+1)
    t1 = -1
    t2 = -1
    while True :
        if fixed_duration is None:
            duration = abs(np.random.normal(0, sigma_duration))
        else:
            duration = fixed_duration
        amplitude = np.random.normal(0, sigma_amplitude)
        t1 = t2 + 1
        t2 = (int) (t1 + duration) + 1
        X[t1:max(t2, N+1)] = amplitude
        if t2 >= N + 1:
            break
    
    return np.cumsum(X)


def periodic_cosine(N, period = 20, upper_phase = 20):
    n = np.arange(N+1)
    phase = np.random.uniform(0, upper_phase)
    X = np.cos(n * 2.0 * np.pi / period + phase)
    return np.cumsum(X)


def random_phase(N, period = 20, sigma_phase = np.pi / 8):
    n = np.arange(N + 1)
    phases = np.random.normal(0, sigma_phase, N+1)
    X = np.cos(2 * np.pi / period * n + phases)

    return np.cumsum(X)

import numpy as np

class MetropolisHasting(object):
    def __init__(self, pt, T, sampler, log_flag = False):
        """
        
        Params:
        -------
        pt : a functional, X -> R, the density (or probability) of the 
            target distribution
        T : a transition kernel, X x X -> R, the conditional density (or probability) of 
            the transition kernel
        sampler : a random sampler, X -> X, the markov transition sampler

        Output:
        -------
        mh : a class of simulator MCMC based on Metropolis Hasting algorithm

        """
        
        self.pt = pt
        self.T = T
        self.log_flag = log_flag
        self.sampler = sampler
    
    def _log_alpha(self, x, z, pt = None, T = None, log_flag = None):
        """
        
        Params:
        -------
        x : current value in the chain
        z : next state value in the chain
        pt : (log) target density or probability
        T : (log) transition density or probability
        log_flag : boolean
                    to indicate if the density or probability is stored in log scale
        
        Output:
        -------
        alpha: the transition threshold according to (x, z)
                in log scale according
        """
        if pt is None:
            pt = self.pt
        
        if T is None:
            T = self.T
        
        if log_flag is None:
            log_flag = self.log_flag

        if log_flag:
            return pt(z) + T(z, x) - pt(x) - T(x, z)
        else:
            return np.log(pt(z)) + np.log(T(z, x)) - np.log(pt(x)) - np.log(T(x, z))

    def simulate(self, x0, d, N = 100):
        """
        
        Params:
        -------
        x0 : numerical value (array-like), shape (d, )
            the initial state, value of the MCMC chain
        d : the dimension of x0
            an integer value
        N : integer value
            the number of observations to be sampled in the MCMC chain
        
        Output:
        -------
        X : array-like
            the observations of the markov chain, the distribution should
            converge to target distribution pt
        
        """
        X = np.zeros((N, d))
        X[0] = x0 # set the initial state of the chain to x0
        for i in range(1, N):
            X_prime = self.sampler(X[i-1])
            s = np.log(np.random.rand())
            threshold = self._log_alpha(X[i-1], X_prime)
            if s < threshold:
                X[i] = X_prime
            else:
                X[i] = X[i-1]
        
        return X

# PhaseHarmonic1D
# Copyrigth (c) by Gaspar Rochette, Roberto Leonarduzzi and Stéphane Mallat

# PhaseHarmonic1D is licensed under a
# Creative Commons Attribution-NonCommercial 4.0 International License.

# You should have received a copy of the license along with this
# work. If not, see <https://creativecommons.org/licenses/by-nc/4.0/>.

import os
from os.path import dirname, abspath, join
import torch


CODEPATH = dirname(abspath(__file__))
DATAPATH = abspath(join(join(CODEPATH, os.pardir), 'data'))
RESPATH = abspath(join(join(CODEPATH, os.pardir), 'results'))


Tensor = torch.DoubleTensor

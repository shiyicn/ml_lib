# PhaseHarmonic1D
# Copyrigth (c) by Gaspar Rochette, Roberto Leonarduzzi and Stéphane Mallat

# PhaseHarmonic1D is licensed under a
# Creative Commons Attribution-NonCommercial 4.0 International License.

# You should have received a copy of the license along with this
# work. If not, see <https://creativecommons.org/licenses/by-nc/4.0/>.


import sys
import math
from itertools import product
import numpy as np
import torch
import torch.nn as nn
import torch.nn.functional as F
from torch.autograd import Variable, grad
import complex_utils as cplx
from kymatio.scattering1d.backend.backend_torch import modulus, fft1d_c2c, ifft1d_c2c
from kymatio.scattering1d import filter_bank as fb
from phaseexp import PhaseExp, PhaseHarmonic
import filter_bank as lfb
from utils import HookDetectNan, count_nans
from global_const import Tensor


class PhaseHarmonicBase(nn.Module):
    '''
    Abstract base class for phase harmonic coefficients
    '''
    def __init__(self, N, Q, T, wav_type="battle_lemarie",
                 high_freq=0.5, delta_j=1, delta_k=[-1, 0, 1],
                 num_k_modulus=3, delta_cooc=2, zero_fst=True,
                 max_chunk=None, check_for_nan=False,
                 compute_second_order=False,
                 debug_second_order=False,
                 wav_norm='l1'):
        super(PhaseHarmonicBase, self).__init__()

        self.debug_second_order = debug_second_order

        self.N = N
        self.Q = Q
        self.T = T
        self.wav_type = wav_type
        self.high_freq = high_freq
        self.phase_harmonics = PhaseHarmonic(check_for_nan=check_for_nan)
        self.check_for_nan = check_for_nan
        self.compute_second_order = compute_second_order
        self.wav_norm = wav_norm
        self.init_filters()

        self.delta_j = delta_j if delta_j is not None else len(self.xi)
        self.delta_k = delta_k
        self.num_k_modulus = num_k_modulus
        self.delta_cooc = delta_cooc
        self.zero_fst = zero_fst

        self.max_chunk = max_chunk

        # TODO: clean this. Only indices are needed.
        self.xi_snd = self.compute_second_order_frequencies()

    def init_filters(self):
        Q = self.Q
        high_freq = self.high_freq
        # initialize wavelets
        if self.wav_type == 'morlet':
            xi, sigma, sigma_low = lfb.compute_morlet_parameters(self.N, self.Q, analytic=True)
        elif self.wav_type == 'battle_lemarie':
            if Q != 1:
                print("\nWarning: width of Battle-Lemarie wavelets not adaptative with Q in the current implementation.\n")
            xi, sigma = lfb.compute_battle_lemarie_parameters(self.N, self.Q, high_freq=high_freq)
        elif self.wav_type == 'bump_steerable':
            if Q != 1:
                print("\nWarning: width of Bump-Steerable wavelets not adaptative with Q in the current implementation.\n")
            xi, sigma = lfb.compute_bump_steerable_parameters(self.N, self.Q, high_freq=high_freq)
        elif self.wav_type == 'meyer':
            #if Q != 1:
            #    print("\nWarning: width of Meyer wavelets not adaptative with Q in the current implementation.\n")
            xi, sigma = lfb.compute_meyer_parameters(self.N, self.Q, high_freq=high_freq)
        elif self.wav_type == 'haar':
            xi, sigma = lfb.compute_haar_parameters(self.N, self.Q, high_freq=high_freq)
        elif self.wav_type == 'gammatone':
            xi, sigma = lfb.compute_gammatone_parameters(self.N, self.Q, high_freq=high_freq)
        else:
            raise ValueError("Unkown wavelet type: {}".format(self.wav_type))
        self.xi = xi
        self.sigma = sigma
        psi_hat = self.compute_wavelet_filters(self.xi, self.sigma)

        # initialize low-pass
        if self.wav_type == 'morlet':
            self.sigma.append(sigma_low)
        
        self.xi.append(0) # additional filter for Morlet Wavelet
        phi_hat = self.compute_low_pass()

        # join wavelets and low-pass into a filter bank (low-frequencies at the end)
        filt_hat = np.concatenate((psi_hat, phi_hat), axis=0)

        # pytorch parameter
        filt_hat = nn.Parameter(cplx.from_numpy(filt_hat), requires_grad=False)
        self.register_parameter('filt_hat', filt_hat)

    def compute_wavelet_filters(self, xis, sigmas):
        """Computes the wavelets Fourier transforms given their parameters
        in xis and sigmas.
        """

        if self.wav_type == "morlet":
            psi_hat = [fb.morlet_1d(self.T, xi, sigma, normalize=self.wav_norm) for xi, sigma in zip(xis, sigmas)]
        elif self.wav_type == "battle_lemarie":
            # psi_hat = [lfb.battle_lemarie_psi(self.T, self.Q, xi) for xi in xis]
            psi_hat = [lfb.battle_lemarie_psi(self.T, 1, xi) / np.sqrt(self.Q) for xi in xis]
        elif self.wav_type == "bump_steerable":
            psi_hat = [lfb.bump_steerable_psi(self.T, 1, xi) / np.sqrt(self.Q) for xi in xis]
        elif self.wav_type == 'meyer':
            psi_hat = [lfb.meyer_psi(self.T, 1, xi) for xi in xis]
        elif self.wav_type == 'haar':
            psi_hat = [lfb.haar_psi(self.T, 1, xi) for xi in xis]
        elif self.wav_type == 'gammatone':
            psi_hat = [lfb.gammatone_psi(self.T, 1, xi) for xi in xis]
        
        psi_hat = np.stack(psi_hat, 0)

        return psi_hat

    def compute_low_pass(self):
        """Compute the low-pass Fourier transforms assuming it has the same variance
        as the lowest-frequency wavelet.
        """
        if self.wav_type == "morlet":
            sigma_low = self.sigma[-1]
            phi_hat = fb.gauss_1d(self.T, sigma_low)
        elif self.wav_type == "battle_lemarie":
            xi_low = self.xi[-2]  # Because 0 was appended for Morlet
            # phi_hat = lfb.battle_lemarie_phi(self.T, self.Q, xi_low)
            phi_hat = lfb.battle_lemarie_phi(self.T, 1, xi_low)
        elif self.wav_type == "bump_steerable":
            xi_low = self.xi[-2]  # Because 0 was appended for Morlet
            # phi_hat = lfb.battle_lemarie_phi(self.T, self.Q, xi_low)
            phi_hat = lfb.bump_steerable_phi(self.T, 1, xi_low)
        elif self.wav_type == 'meyer':
            xi_low = self.xi[-2]
            phi_hat = lfb.meyer_phi(self.T, 1, xi_low)
        elif self.wav_type == 'haar':
            xi_low = self.xi[-2] # Because 0 was appended for Morlet
            phi_hat = lfb.haar_phi(self.T, 1, xi_low)
        elif self.wav_type == 'gammatone':
            xi_low = self.xi[-2]
            phi_hat = lfb.gammatone_phi(self.T, 1, xi_low)
        
        return phi_hat[None, :]

    def compute_wavelet_coefficients(self, x):

        if self.debug_second_order:
            return self.compute_wavelet_coefficients_restricted(x)

        # filter x with wavelets and low-pass in self.filt_hat
        x_hat = fft1d_c2c(x)

        # apply wavelet filters
        x_filt_hat = cplx.mul(x_hat.unsqueeze(2), self.filt_hat.unsqueeze(0).unsqueeze(1))
        x_filt_fst = ifft1d_c2c(x_filt_hat)

        if not self.compute_second_order:
            return (x_filt_fst,)
        else:
            x_filt_fst_mod = cplx.modulus(x_filt_fst)
            x_filt_fst_mod = torch.stack((x_filt_fst_mod, torch.zeros_like(x_filt_fst_mod)), dim=-1)
            x_filt_fst_mod_hat = fft1d_c2c(x_filt_fst_mod)

            x_filt_snd = []
            n_channel = len(self.xi) - 1
            for j1 in range(n_channel):
                for j2 in range(j1 + 1, n_channel):
                    #print(j1, j2)
                    tmp = cplx.mul(x_filt_fst_mod_hat[:, :, j1, ...],
                                   self.filt_hat[j2, ...].unsqueeze(0).unsqueeze(1))
                    x_filt_snd.append(ifft1d_c2c(tmp))
            x_filt_snd = torch.stack(x_filt_snd, dim=2)

            return (x_filt_fst, x_filt_snd)
    def compute_second_order_frequencies(self):
        if self.debug_second_order:
            return self.compute_second_order_frequencies_restricted()
        '''
        Create a list with the frequencies of second order wavelets, in the
        order in which coefficients  will be computed.
        '''
        xi_fst = []
        xi_snd = []
        n_channel = len(self.xi) - 1
        for j1 in range(n_channel):
            for j2 in range(j1+1, n_channel):  # TODO: check that filters are ordered in decreasing frequency
                xi_snd.append(self.xi[j2])
                xi_fst.append(self.xi[j1])
        return (xi_fst, xi_snd)
    #<-- dbg
    def compute_wavelet_coefficients_restricted(self, x):
        jsel = [[0, 3], [0, 4], [0, 5], [0, 6], [0, 7], [0, 8], [1, 3], [1, 4], [1, 5], [1, 6], [1, 7], [1, 8],
                [2, 3], [2, 4], [2, 5], [2, 6], [2, 7], [2, 8], [3, 4], [3, 5], [3, 6], [3, 7], [3, 8], [4, 5],
                [4, 6], [4, 7], [4, 8], [5, 6], [5, 7], [5, 8], [6, 7], [6, 8], [7, 8]]
        # filter x with wavelets and low-pass in self.filt_hat
        x_hat = fft1d_c2c(x)

        # apply wavelet filters
        x_filt_hat = cplx.mul(x_hat.unsqueeze(2), self.filt_hat.unsqueeze(0).unsqueeze(1))
        x_filt_fst = ifft1d_c2c(x_filt_hat)

        if not self.compute_second_order:
            return (x_filt_fst,)
        else:
            x_filt_fst_mod = cplx.modulus(x_filt_fst)
            x_filt_fst_mod = torch.stack((x_filt_fst_mod, torch.zeros_like(x_filt_fst_mod)), dim=-1)
            x_filt_fst_mod_hat = fft1d_c2c(x_filt_fst_mod)

            x_filt_snd = []
            n_channel = len(self.xi) - 1
            for j1, j2 in jsel: #range(n_channel):
                #print(j1, j2)
                tmp = cplx.mul(x_filt_fst_mod_hat[:, :, j1, ...],
                               self.filt_hat[j2, ...].unsqueeze(0).unsqueeze(1))
                #print('lala')
                x_filt_snd.append(ifft1d_c2c(tmp))
            x_filt_snd = torch.stack(x_filt_snd, dim=2)
            #print('Pre return')
            #print('x_filt_fst: ', x_filt_fst)
            return (x_filt_fst, x_filt_snd)
    def compute_second_order_frequencies_restricted(self):
        '''
        Create a list with the frequencies of second order wavelets, in the
        order in which coefficients  will be computed.
        '''
        jsel = [[0, 3], [0, 4], [0, 5], [0, 6], [0, 7], [0, 8], [1, 3], [1, 4], [1, 5], [1, 6], [1, 7], [1, 8],
                [2, 3], [2, 4], [2, 5], [2, 6], [2, 7], [2, 8], [3, 4], [3, 5], [3, 6], [3, 7], [3, 8], [4, 5],
                [4, 6], [4, 7], [4, 8], [5, 6], [5, 7], [5, 8], [6, 7], [6, 8], [7, 8]]
        xi_fst = []
        xi_snd = []
        n_channel = len(self.xi) - 1
        for j1, j2 in jsel:
            xi_snd.append(self.xi[j2])
            xi_fst.append(self.xi[j1])
        return (xi_fst, xi_snd)
    # dbg -->


    def shape(self):
        """Returns the number of complex coefficients in the embedding."""
        raise NotImplementedError

    def num_coeff(self):
        """Returns the effective number of (real) coefficients in the embedding"""
        raise NotImplementedError

    def compute_idx_info(self, num_k_modulus, delta_k):
        raise NotImplementedError

    def balance_chunks(self, *args):
        """Cuts all torch tensors in args in corresponding balanced chunks, along
        their first dimension.
        For each input tensor, the output is a list of chunks of this tensor.
        """

        if self.max_chunk is None:
            return [[a] for a in args]  # each tensor is divided in only one chunk

        n_idx = args[0].shape[0]

        n_stops = int(np.ceil(n_idx / self.max_chunk))
        base_size, leftover = n_idx // n_stops, n_idx % n_stops
        sizes = [base_size + int(i < leftover) for i in range(n_stops)]
        stops_pos = np.cumsum([0] + sizes)

        chunked_args = []
        for tens in args:
            chunked_tens = [tens[s:e] for s, e in zip(stops_pos[:-1], stops_pos[1:])]
            chunked_args.append(chunked_tens)

        return chunked_args


# class PhaseHarmonicPruned(PhaseHarmonicPrunedBase):
#     def __init__(self, *args, **kwargs):
#         super(PhaseHarmonicPruned, self).__init__(*args, **kwargs)

#         xi_idx, ks, num_coeff_abs, num_coeff_cplx = self.compute_idx_info(
#             self.num_k_modulus, self.delta_k)
#         self.num_coeff_abs = num_coeff_abs
#         self.num_coeff_cplx = num_coeff_cplx

#         nb_idx = xi_idx.shape[0]
#         nb_chunk = 1 if self.max_chunk is None else int(math.ceil(nb_idx / self.max_chunk))
#         xi_idx = np.array_split(xi_idx, nb_chunk, axis=0)
#         ks = np.array_split(ks, nb_chunk, axis=0)
#         self.xi_idx = [torch.LongTensor(xi_id) for xi_id in xi_idx]
#         self.ks = [torch.LongTensor(k) for k in ks]

#     def cuda(self):
#         super(PhaseHarmonicPrunedBase, self).cuda()
#         self.xi_idx = [xi_idx.cuda() for xi_idx in self.xi_idx]
#         self.ks = [ks.cuda() for ks in self.ks]
#         return self

#     def cpu(self):
#         super(PhaseHarmonicPrunedBase, self).cpu()
#         self.xi_idx = [xi_idx.cpu() for xi_idx in self.xi_idx]
#         self.ks = [ks.cpu() for ks in self.ks]
#         return self

#     def shape(self):
#         """Returns the number of complex coefficients in the embedding."""
#         o2_s = sum(idx.size(0) for idx in self.xi_idx)
#         o1_s = len(self.xi) * (1 if self.zero_fst else self.K)
#         return o1_s, o2_s

#     def num_coeff(self):
#         """Returns the effective number of (real) coefficients in the embedding"""
#         J = len(self.xi)

#         # first order coefficients (complex)
#         o1 = J

#         # second order coefficients (some are complex)
#         o2 = self.num_coeff_abs + 2 * self.num_coeff_cplx

#         return o1 + o2

#     def compute_idx_info(self, num_k_modulus, delta_k):
#         if delta_k is None:
#             num_k_modulus = min(num_k_modulus, 2)

#         J = len(self.xi)
#         xi = np.array(self.xi)
#         num_coeff_abs = 0  # number of coefficients which are already real
#         num_coeff_cplx = 0  # number of truly complex coefficients

#         # compute coeffs j = j'
#         jeq = np.arange(np.size(xi))
#         jeq0, jpeq0, jeq1, jpeq1 = jeq, jeq, jeq, jeq
#         keq0, kpeq0 = np.zeros_like(jeq0), np.zeros_like(jpeq0)
#         keq1, kpeq1 = np.zeros_like(jeq0), np.ones_like(jpeq0)

#         num_coeff_abs += jeq.size
#         num_coeff_cplx += jeq.size

#         # compute coefficients j < j'
#         j, jp = np.where(xi[:, None] > xi[None, :])  # j < j'
#         loc = np.where(jp - j <= self.delta_j * self.Q)  # |j - j'| < delta_j (* Q)
#         j, jp = j[loc], jp[loc]

#         # compute <|x*psi_j|, [x*psi_j']^k>
#         kp0 = np.ravel(np.repeat(np.arange(num_k_modulus)[None, :], j.size, axis=0))
#         k0 = np.zeros_like(kp0)
#         j0 = np.ravel(np.repeat(j[:, None], num_k_modulus, axis=1))
#         jp0 = np.ravel(np.repeat(jp[:, None], num_k_modulus, axis=1))

#         num_coeff_abs += j.size
#         num_coeff_cplx += j.size * (num_k_modulus - 1)

#         # compute <x*psi_j, [x*psi_j']^(2^(j-j') +- 1)>
#         # num_k = 2 * self.K + 1
#         if delta_k is None:  # only compute k=1, k'=1
#             num_k = 1
#             k1 = np.ones_like(j[:])
#             kp1 = np.ones_like(j[:])
#             j1 = j.copy()
#             jp1 = jp.copy()

#         else:
#             num_k = len(delta_k)
#             delta_k = np.array(delta_k)

#             # center = (jp - j)[:, None] / self.Q + .2 * delta_k[None, :]
#             center = (jp - j)[:, None] / self.Q
#             kp1 = np.ravel(np.power(2., center) + delta_k[None, :])  # moves j' to j
#             k1 = np.ones_like(kp1)
#             j1 = np.ravel(np.repeat(j[:, None], num_k, axis=1))
#             jp1 = np.ravel(np.repeat(jp[:, None], num_k, axis=1))


#         num_coeff_cplx += j.size * num_k

#         j = np.concatenate((jeq0, jeq1, j0, j1))
#         jp = np.concatenate((jpeq0, jpeq1, jp0, jp1))
#         k = np.concatenate((keq0, keq1, k0, k1))
#         kp = np.concatenate((kpeq0, kpeq1, kp0, kp1))

#         keep_k = np.stack((k, kp), axis=1)
#         keep_idx_xi = np.stack((j, jp), axis=1)


#         # k fractionnary would lead to discontinuity
#         keep_k = np.floor(keep_k).astype(int)

#         return keep_idx_xi, keep_k, num_coeff_abs, num_coeff_cplx


#     def forward(self, x):
#         # # filter x with wavelets and low-pass in self.filt_hat
#         # x_hat = fft1d_c2c(x)

#         # # apply wavelet filters
#         # x_filt_hat = cplx.mul(x_hat.unsqueeze(2), self.filt_hat.unsqueeze(0).unsqueeze(1))

#         # x_filt = ifft1d_c2c(x_filt_hat)
#         x_filt, _ = self.compute_wavelet_coefficients(x)

#         # first order
#         k0 = torch.zeros_like(x_filt[0, 0, ..., 0, 0]).view(-1)[:x_filt.size(2)].long()
#         fst_order = self.phase_harmonics(x_filt, k0)
#         for spatial_dim in x.size()[2:-1]:
#             fst_order = torch.mean(fst_order, dim=-2)

#         # second order
#         scd_order = []
#         for xi_idx, ks in zip(self.xi_idx, self.ks):
#             x_filt0 = torch.index_select(x_filt, 2, xi_idx[:, 0])
#             x_filt1 = torch.index_select(x_filt, 2, xi_idx[:, 1])
#             k0, k1 = ks[:, 0], ks[:, 1]

#             scd_0 = self.phase_harmonics(x_filt0, k0)
#             scd_1 = self.phase_harmonics(x_filt1, -k1)
#             scd = torch.mean(cplx.mul(scd_0, scd_1), dim=-2)
#             scd_order.append(scd)
#         scd_order = torch.cat(scd_order, dim=2)

#         # for debug, can be ignored
#         if x.requires_grad and self.check_for_nan:
#             x_filt.register_hook(HookDetectNan('x_filt in PhaseHarmonicTransform.compute_phase_harmonics'))
#             fst_order.register_hook(HookDetectNan('fst_order in PhaseHarmonicTransform.compute_phase_harmonics'))
#             scd_order.register_hook(HookDetectNan('scd_order in PhaseHarmonicTransform.compute_phase_harmonics'))

#         return fst_order, scd_order


# class PhaseHarmonicPrunedSelect(PhaseHarmonicPrunedBase):
#     def __init__(self, *args, coeff_select=['harmonic', 'mixed'], **kwargs):
#         super(PhaseHarmonicPrunedSelect, self).__init__(*args, **kwargs)

#         self.coeff_select = coeff_select

#         self.idx_info = self.compute_idx_info(self.num_k_modulus, self.delta_k)

#         self.num_coeff_abs = 0
#         self.num_coeff_cplx = 0
#         for ctype in coeff_select:
#             self.num_coeff_abs += self.idx_info[ctype]['ncoef_real']
#             self.num_coeff_cplx += self.idx_info[ctype]['ncoef_cplx']

#         self.xi_idx = [torch.LongTensor(self.idx_info[ctype]['xi_idx'])
#                        for ctype in coeff_select]
#         self.ks = [torch.LongTensor(self.idx_info[ctype]['k'])
#                    for ctype in coeff_select]

#         self.is_cuda = False

#         # TODO: implement chunks

#         # xi_idx, ks, num_coeff_abs, num_coeff_cplx = self.compute_idx_info()
#         # self.num_coeff_abs = num_coeff_abs
#         # self.num_coeff_cplx = num_coeff_cplx

#         # nb_idx = xi_idx.shape[0]
#         # nb_chunk = 1 if self.max_chunk is None else int(math.ceil(nb_idx / self.max_chunk))
#         # xi_idx = np.array_split(xi_idx, nb_chunk, axis=0)
#         # ks = np.array_split(ks, nb_chunk, axis=0)
#         # self.xi_idx = [torch.LongTensor(xi_id) for xi_id in xi_idx]
#         # self.ks = [torch.LongTensor(k) for k in ks]

#     def cuda(self):
#         super(PhaseHarmonicPrunedSelect, self).cuda()
#         self.xi_idx = [x.cuda() for x in self.xi_idx]
#         self.ks = [k.cuda() for k in self.ks]
#         self.is_cuda = True
#         return self

#     def cpu(self):
#         super(PhaseHarmonicPrunedSelect, self).cpu()
#         self.xi_idx = [x.cpu() for x in self.xi_idx]
#         self.ks = [k.cpu() for k in self.ks]
#         self.is_cuda = False
#         return self

#     def compute_idx_info(self, num_k_modulus, delta_k):
#         J = len(self.xi)
#         xi = np.array(self.xi)
#         num_coeff_abs = 0  # number of coefficients which are already real
#         num_coeff_cplx = 0  # number of truly complex coefficients

#         # compute coeffs j = j'
#         jeq = np.arange(np.size(xi))
#         jeq0, jpeq0, jeq1, jpeq1 = jeq, jeq, jeq, jeq
#         keq0, kpeq0 = np.zeros_like(jeq0), np.zeros_like(jpeq0)
#         keq1, kpeq1 = np.zeros_like(jeq0), np.ones_like(jpeq0)

#         num_coeff_abs += jeq.size
#         num_coeff_cplx += jeq.size

#         # compute coefficients j < j'
#         j, jp = np.where(xi[:, None] > xi[None, :])  # j < j'
#         loc = np.where(jp - j <= self.delta_j * self.Q)  # |j - j'| < delta_j (* Q)
#         j, jp = j[loc], jp[loc]

#         # compute <|x*psi_j|, |x*psi_j'|>  harmonic and informative modulus
#         kp0 = np.zeros((j.size))
#         k0 = np.zeros_like(kp0)
#         j0 = np.ravel(np.repeat(j[:, None], 1, axis=1))
#         jp0 = np.ravel(np.repeat(jp[:, None], 1, axis=1))

#         num_coeff_abs += j.size

#         # compute <x*psi_j, [x*psi_j']^(2^(j-j') +- 1)>
#         # num_k = 2 * self.K + 1
#         # delta_k = np.linspace(-1, 1, num=num_k)
#         # print(delta_k)
#         num_k = len(delta_k)
#         delta_k = np.array(delta_k)

#         # center = (jp - j)[:, None] / self.Q + .2 * delta_k[None, :]
#         center = (jp - j)[:, None] / self.Q
#         kp1 = np.ravel(np.power(2., center) + delta_k[None, :])  # moves j' to j
#         k1 = np.ones_like(kp1)
#         j1 = np.ravel(np.repeat(j[:, None], num_k, axis=1))
#         jp1 = np.ravel(np.repeat(jp[:, None], num_k, axis=1))

#         num_coeff_cplx += j.size * num_k

#         j = np.concatenate((jeq0, jeq1, j0, j1))
#         jp = np.concatenate((jpeq0, jpeq1, jp0, jp1))
#         k = np.concatenate((keq0, keq1, k0, k1))
#         kp = np.concatenate((kpeq0, kpeq1, kp0, kp1))

#         keep_k = np.stack((k, kp), axis=1)
#         keep_idx_xi = np.stack((j, jp), axis=1)

#         # k fractionnary would lead to discontinuity
#         keep_k = np.floor(keep_k).astype(int)

#         dict_harm = {'k': keep_k,
#                      'xi_idx': keep_idx_xi,
#                      'ncoef_cplx': num_coeff_cplx,
#                      'ncoef_real': num_coeff_abs}

#         # Mixed coefficients:  <|x*psi_j|, [x*psi_j']^k>, k>1
#         kpm = np.ravel(np.repeat(np.arange(1, num_k_modulus)[None, :], j.size, axis=0))
#         km = np.zeros_like(kpm)
#         jm = np.ravel(np.repeat(j[:, None], num_k_modulus-1, axis=1))
#         jpm = np.ravel(np.repeat(jp[:, None], num_k_modulus-1, axis=1))

#         keep_k_mix = np.stack((km, kpm), axis=1)
#         keep_idx_xi_mix = np.stack((jm, jpm), axis=1)
#         keep_k_mix = np.floor(keep_k_mix).astype(int)

#         num_coeff_abs_mix = 0
#         num_coeff_cplx_mix = j.size * (num_k_modulus - 1)

#         dict_mix = {'k': keep_k_mix,
#                     'xi_idx': keep_idx_xi_mix,
#                     'ncoef_cplx': num_coeff_cplx_mix,
#                     'ncoef_real': num_coeff_abs_mix}

#         idx_info = {'harmonic':dict_harm, 'mixed':dict_mix}

#         return idx_info

#     def forward(self, x):
#         x_filt, _ = self.compute_wavelet_coefficients(x)

#         # first order
#         k0 = torch.zeros_like(x_filt[0, 0, ..., 0, 0]).view(-1)[:x_filt.size(2)].long()
#         fst_order = self.phase_harmonics(x_filt, k0)
#         for spatial_dim in x.size()[2:-1]:
#             fst_order = torch.mean(fst_order, dim=-2)

#         # second order
#         scd_order = []
#         # TODO: implement blocks
#         for xi_idx, ks in zip(self.xi_idx, self.ks):
#             x_filt0 = torch.index_select(x_filt, 2, xi_idx[:, 0])
#             x_filt1 = torch.index_select(x_filt, 2, xi_idx[:, 1])
#             k0, k1 = ks[:, 0], ks[:, 1]

#             scd_0 = self.phase_harmonics(x_filt0, k0)
#             scd_1 = self.phase_harmonics(x_filt1, -k1)
#             scd = torch.mean(cplx.mul(scd_0, scd_1), dim=-2)
#             scd_order.append(scd)
#         if scd_order:
#             scd_order = torch.cat(scd_order, dim=2)
#         else:
#             fst_order = Tensor([])
#             scd_order = Tensor([])
#             if self.is_cuda == 'cuda':
#                 fst_order = fst_order.cuda()
#                 scd_order = scd_order.cuda()

#         return fst_order, scd_order


#     def num_coeff(self):
#         """Returns the effective number of (real) coefficients in the embedding"""

#         num_coeff = 0
#         for key in self.idx_info.keys():
#             num_coeff += self.idx_info[key]['ncoef_real']
#             num_coeff += 2 * self.idx_info[key]['ncoef_cplx']

#         return num_coeff


class PhaseHarmonicCoeff(PhaseHarmonicBase):
    def __init__(self, *args,
                 coeff_select=[['jeqmod','harmonic'], ['jeqmod','harmonic']],
                 remove_mean_p=False,
                 normalize_coefficients_p=False, eps_renorm=0, **kwargs):
        super(PhaseHarmonicCoeff, self).__init__(*args, **kwargs)
        # check if valid coefficient types
        possible_coeff_select = ['harmonic', 'mixed', 'harmod', 'infmod', 'jeqmod', 'jeqmix']
        for order in coeff_select:
            for cs in order:
                if cs not in possible_coeff_select:
                    raise ValueError("Unknown coefficient type: '{}'".format(cs))
        self.coeff_select = coeff_select
        self.remove_mean_p = remove_mean_p
        self.normalize_coefficients_p = normalize_coefficients_p
        self.eps_renorm=eps_renorm

        self.idx_info = [[], []]
        self.idx_info[0] = self.compute_idx_info(self.xi, self.num_k_modulus, self.delta_k)
        self.idx_info[1] = self.compute_idx_info(self.xi_snd[1], self.num_k_modulus, self.delta_k)

        self.idx_info[1] = self.remove_coefs_different_j1(self.idx_info[1])

        self.num_coeff_abs = 0
        self.num_coeff_cplx = 0
        for order in coeff_select:
            for ctype in order:
                self.num_coeff_abs  += self.idx_info[0][ctype]['ncoef_real']
                self.num_coeff_cplx += self.idx_info[0][ctype]['ncoef_cplx']
                if self.compute_second_order:
                    self.num_coeff_abs  += self.idx_info[1][ctype]['ncoef_real']
                    self.num_coeff_cplx += self.idx_info[1][ctype]['ncoef_cplx']

        xi_idx_fst, ks_fst = self.format_and_chunk_idx_info(self.idx_info[0], self.coeff_select[0])
        xi_idx_snd, ks_snd = self.format_and_chunk_idx_info(self.idx_info[1], self.coeff_select[1])
        self.xi_idx = (xi_idx_fst, xi_idx_snd)
        self.ks = (ks_fst, ks_snd)

        self.is_cuda = False

        # TODO: implement chunks

    def shape(self):
        '''
        Returns the number of real and complex coefficients in the embedding.
        '''
        return self.num_coeff_abs + 2 * self.num_coeff_cplx

    def cuda(self):
        super(PhaseHarmonicCoeff, self).cuda()
        self.xi_idx = [[x.cuda() for x in xi_idx] for xi_idx in self.xi_idx]
        self.ks = [[k.cuda() for k in ks] for ks in self.ks]
        self.is_cuda = True
        return self

    def cpu(self):
        super(PhaseHarmonicCoeff, self).cpu()
        self.xi_idx = [[x.cpu() for x in xi_idx] for xi_idx in self.xi_idx]
        self.ks = [[k.cpu() for k in ks] for ks in self.ks]
        self.is_cuda = False
        return self

    def remove_coefs_different_j1(self, idx_info):
        xi1 = np.array(self.xi_snd[0])
        xi2 = np.array(self.xi_snd[1])

        for key, d in idx_info.items():  # idx_info list of dictionaries
            xi_idx = d['xi_idx']
            k = d['k']

            # Obtain the j1 of each selected coeff, and compare
            keep = xi1[xi_idx[:, 0]] == xi1[xi_idx[:, 1]]

            # d is reference:
            d['xi_idx'] = xi_idx[keep, :]
            d['k'] = k[keep, :]
            d['xi_fst'] = xi1[xi_idx[keep, 0]]
            d['xi_snd'] = xi2[xi_idx[keep, 0]]
            d['xi_snd_idx_parent'] = np.array(
                [self.xi.index(xi_curr) for xi_curr in d['xi_fst']])

            # dbg
            #print('Saving {}...'.format(key))
            #np.savez('dbg_remove_coeffs_j1_{}.npz'.format(key),
            #         xi1=xi1, xi2=xi2, xi_idx=xi_idx, k=k, d=d, keep=keep)

        return idx_info

    def format_and_chunk_idx_info(self, idx_info, coeff_select):
        if len(coeff_select) > 0:
            xi_idx = torch.cat(
                [torch.LongTensor(idx_info[ctype]['xi_idx']) for ctype in coeff_select],
                dim=0)
            ks = torch.cat(
                [torch.LongTensor(idx_info[ctype]['k']) for ctype in coeff_select],
                dim=0)
        else:
            xi_idx = torch.LongTensor([])
            ks = torch.LongTensor([])

        xi_idx_chunked, ks_chunked = self.balance_chunks(xi_idx, ks)
        return xi_idx_chunked, ks_chunked

    def compute_idx_info(self, xi, num_k_modulus, delta_k):
        '''
        Compute the combinations of scales and exponents k for different groups of  phase harmonic coefficients.

        The implemented groups are:
          - jeqmod: <|x * psi_j|, |x * psi_j'|>
          - jeqmix: <|x * psi_j|, x * psi_j'>
          - harmonic: <x*psi_j, [x*psi_j']^k> where k is close to 2^(j-j')
          - harmonic modulus: <|x*psi_j|, |x*psi_j'|> for the same j, j' as in harmonic
          - informative modulus: <|x*psi_j|, |x*psi_j'|> for all other j, j'
          - mixed: <|x*psi_j|, [x*psi_j']^k>, k=1,2, j'>=j

        The output is a dictionary whose keys are the name of each group of coefficients.
        The value is another dictionary with the fields 'xi_idx' and 'k' (lists of size ncoeff x 2),
        containing the indices j, j' and k, k', respectively, for each coefficient.
        '''
        if xi is None:
            empty_dict = {'k': np.array([]),
                          'xi_idx': np.array([]),
                          'ncoef_cplx': 0,
                          'ncoef_real': 0}
            return {'harmonic':empty_dict, 'mixed':empty_dict, 'infmod': empty_dict, 'harmod': empty_dict,
                    'jeqmod': empty_dict, 'jeqmix':empty_dict}

        J = len(xi)
        xi = np.array(xi)

        # compute coeffs j = j'
        jeq = np.arange(np.size(xi))
        jeq0, jpeq0, jeq1, jpeq1 = jeq, jeq, jeq, jeq
        keq0, kpeq0 = np.zeros_like(jeq0), np.zeros_like(jpeq0)
        keq1, kpeq1 = np.zeros_like(jeq0), np.ones_like(jpeq0)

        num_coeff_abs = jeq.size  # number of coefficients which are already real
        num_coeff_cplx = jeq.size  # number of truly complex coefficients

        keep_k = np.stack((keq0, kpeq0), axis=1)
        keep_idx_xi = np.stack((jeq0, jpeq0), axis=1)
        dict_jeq0 = {'k': keep_k,
                     'xi_idx': keep_idx_xi,
                     'ncoef_cplx': num_coeff_cplx,
                     'ncoef_real': num_coeff_abs}

        keep_k = np.stack((keq1, kpeq1), axis=1)
        keep_idx_xi = np.stack((jeq1, jpeq1), axis=1)
        dict_jeq1 = {'k': keep_k,
                     'xi_idx': keep_idx_xi,
                     'ncoef_cplx': num_coeff_cplx,
                     'ncoef_real': num_coeff_abs}


        # compute coefficients j < j'
        j, jp = np.where(xi[:, None] > xi[None, :])  # j < j'
        loc = np.where(jp - j <= self.delta_j * self.Q)  # |j - j'| < delta_j (* Q)
        j, jp = j[loc], jp[loc]

        # compute <x*psi_j, [x*psi_j']^k> where k is close to 2^(j-j')

        # j = 0, 1, 2, ..., j' = 1, 2, 3, ..., 2, 3, 4, ...,
        j1, jp1 = [], []
        kp1 = []
        for i1 in range(J - 1):
            ip1 = np.arange(i1 + self.Q, min(J, i1 + 1 + self.delta_j * self.Q))
            center = (ip1 - i1) / self.Q
            kp1_aux = np.power(2., center)
            kp1_round = np.round(kp1_aux)

            for ku in np.unique(kp1_round):
                idx_ku = np.argmin(np.abs(kp1_aux - ku))  # index of wavelet closest to factor ku
                j1.append(i1)
                jp1.append(ip1[idx_ku])
                kp1.append(ku)
        j1 = np.array(j1)
        jp1 = np.array(jp1)
        kp1 = np.array(kp1)
        k1 = np.ones_like(kp1)

        num_coeff_cplx = j1.size
        num_coeff_abs = 0

        # j = np.concatenate((jeq0, jeq1, j1))
        # jp = np.concatenate((jpeq0, jpeq1, jp1))
        # k = np.concatenate((keq0, keq1, k1))
        # kp = np.concatenate((kpeq0, kpeq1, kp1))
        j  = j1
        jp = jp1
        k  = k1
        kp = kp1


        keep_k = np.stack((k, kp), axis=1)
        keep_idx_xi = np.stack((j, jp), axis=1)

        # k fractionnary would lead to discontinuity
        keep_k = np.floor(keep_k).astype(int)

        dict_harm = {'k': keep_k,
                     'xi_idx': keep_idx_xi,
                     'ncoef_cplx': num_coeff_cplx,
                     'ncoef_real': num_coeff_abs}

        # compute <|x*psi_j|, |x*psi_jk|> harmonic modulus
        k0_hm = np.zeros_like(k1)
        kp0_hm = np.zeros_like(kp1)
        j0_hm, jp0_hm = j1, jp1

        seen = set(zip(j0_hm, jp0_hm))  # set to remember which <|x*psi_j|, |x*psi_jk|> are accounted for

        keep_k_hm = np.stack((k0_hm, kp0_hm), axis=1)
        keep_idx_xi_hm = np.stack((j0_hm, jp0_hm), axis=1)

        dict_harmod = {'k': keep_k_hm,
                       'xi_idx': keep_idx_xi_hm,
                       'ncoef_cplx': 0 ,
                       'ncoef_real': j0_hm.size}

        # compute <|x*psi_j|, |x*psi_j'|> informative modulus
        j0_im, jp0_im = np.where(xi[:, None] > xi[None, :])  # j < j'
        loc = np.where(jp0_im - j0_im <= self.delta_cooc * self.Q)  # |j - j'| < delta_cooc (* Q)
        j0_im, jp0_im = j0_im[loc], jp0_im[loc]

        keep = []
        for i in zip(j0_im, jp0_im):
            if i in seen:
                keep.append(False)
            else:
                keep.append(True)
        keep = np.array(keep)
        j0_im = j0_im[keep]
        jp0_im = jp0_im[keep]

        kp0_im = np.zeros((j0_im.size))
        k0_im = np.zeros_like(kp0_im)

        keep_k_im = np.stack((k0_im, kp0_im), axis=1)
        keep_idx_xi_im = np.stack((j0_im, jp0_im), axis=1)

        dict_infmod = {'k': keep_k_im,
                       'xi_idx': keep_idx_xi_im,
                       'ncoef_cplx': 0 ,
                       'ncoef_real': j0_im.size}


        # Mixed coefficients:  <|x*psi_j|, [x*psi_j']^k>, k>1
        jm = []
        jpm = []
        for jj in range(xi.size):
            for jjp in range(jj, xi.size): #range(jj, xi.size):
                jm.append(jj)
                jpm.append(jjp)
        jm = np.array(jm)
        jpm = np.array(jpm)

        kpm = np.ravel(np.repeat(2**np.arange(0, num_k_modulus)[None, :], jm.size, axis=0))
        km = np.zeros_like(kpm)
        jm = np.ravel(np.repeat(jm[:, None], num_k_modulus, axis=1))
        jpm = np.ravel(np.repeat(jpm[:, None], num_k_modulus, axis=1))

        keep_k_mix = np.stack((km, kpm), axis=1)
        keep_idx_xi_mix = np.stack((jm, jpm), axis=1)
        keep_k_mix = np.floor(keep_k_mix).astype(int)

        num_coeff_abs_mix = 0
        num_coeff_cplx_mix = jm.shape[0]

        dict_mix = {'k': keep_k_mix,
                    'xi_idx': keep_idx_xi_mix,
                    'ncoef_cplx': num_coeff_cplx_mix,
                    'ncoef_real': num_coeff_abs_mix}

        idx_info = {'harmonic':dict_harm, 'mixed':dict_mix, 'infmod':dict_infmod, 'harmod':dict_harmod,
                    'jeqmod': dict_jeq0, 'jeqmix': dict_jeq1}

        return idx_info


    def normalize_coefficients(self, avg, var, cov, var_cov, eps=1e-12):
        '''
        var: length 3 tuple with: variances of x, x * psi_la and |x * psi_la| * psi_la2

        avg[0]: 1 x 1 x J x 2
        avg[1]: 1 x 1 x JJ' x 2

        var[0]: 1 x 1 x 2
        var[1]: 1 x 1 x J x 2
        var[2]: 1 x 1 x JJ' x 2

        cov[0]: 1 x 1 x KJ x 2
        cov[1]: 1 x 1 x KJJ' x 2
        '''

        # Covariances
        den = var_cov[0][..., 0]
        cov[0] /= den[..., None] + eps

        if self.compute_second_order:
            den = var_cov[1][..., 0]
            cov[1] /= den[..., None] + eps

        # Averages
        if self.compute_second_order:
            xi_idx_par = np.array([self.xi.index(xi_curr) for xi_curr in self.xi_snd[0]])
            den = avg[0][..., xi_idx_par, 0]
            avg[1] /= den[..., None] + eps
        avg[0] /= torch.sqrt(var[0]) + eps  # Normalize by variance of x



    def forward(self, x, output_sparse_p=False):
        x_filt_full = self.compute_wavelet_coefficients(x)

        # np.savez('coefs_new.npz', ord1=x_filt_full[0], ord2=x_filt_full[1])
        # raise SystemError()

        if len(self.coeff_select) == 0 or len(self.coeff_select[0]) == 0 or len(self.coeff_select[1]) == 0:
            return Tensor([]), Tensor([])

        avg_full = []
        avg2_full = []
        var_full = [torch.var(x, dim=-2, keepdim=True)]
        cov_full = []
        mean_cov_full = []
        var_cov_full = []
        sparse_full = []

        # Loop first and second order coefficients
        for ord, x_filt in enumerate(x_filt_full):

            # Averages and variances
            k0 = torch.zeros_like(x_filt[0, 0, ..., 0, 0]).view(-1)[:x_filt.size(2)].long()
            tmp = self.phase_harmonics(x_filt, k0)
            avg = torch.mean(tmp, dim=-2)
            var = torch.var(tmp, dim=-2)
            avg2 = torch.mean(tmp**2, dim=-2)

            # Covariances
            cov = []
            means_cov = []
            var_cov = []
            for xi_idx, ks in zip(self.xi_idx[ord], self.ks[ord]):
                x_filt0 = torch.index_select(x_filt, 2, xi_idx[:, 0])
                x_filt1 = torch.index_select(x_filt, 2, xi_idx[:, 1])
                k0, k1 = ks[:, 0], ks[:, 1]

                cov_tmp_0 = self.phase_harmonics(x_filt0, k0)
                cov_tmp_1 = self.phase_harmonics(x_filt1, -k1)

                # Mean removal makes sense for k=0
                mean_tmp = [torch.mean(cov_tmp_0, dim=-2, keepdim=True),
                            torch.mean(cov_tmp_1, dim=-2, keepdim=True)]
                means_cov.append(mean_tmp)

                #var_tmp =  torch.sqrt(torch.var(cov_tmp_0, dim=-2) * torch.var(cov_tmp_1, dim=-2))

                cov_tmp_0_nomean = cov_tmp_0 - mean_tmp[0]
                cov_tmp_1_nomean = cov_tmp_1 - mean_tmp[1]
                if self.remove_mean_p:
                    cov_tmp_0 = cov_tmp_0_nomean
                    cov_tmp_1 = cov_tmp_1_nomean
                var_tmp =  torch.sqrt(torch.mean(cov_tmp_0_nomean.norm(p=2, dim=-1, keepdim=True)**2, dim=-2) *
                                      torch.mean(cov_tmp_1_nomean.norm(p=2, dim=-1, keepdim=True)**2, dim=-2))
                var_cov.append(var_tmp)
                cov_tmp = torch.mean(cplx.mul(cov_tmp_0, cov_tmp_1), dim=-2)
                #import pdb; pdb.set_trace()
                cov.append(cov_tmp)
            # Condition below concatenates the different types of coefficients
            if cov:
                cov = torch.cat(cov, dim=2)
                var_cov = torch.cat(var_cov, dim=2)
            else:
                avg = Tensor([])
                cov = Tensor([])
                var_cov = Tensor([])
                if self.is_cuda == 'cuda':
                    avg_full = avg.cuda()
                    cov_full = cov.cuda()

            # Compute sparsity coefficient before normalization
            sparse = var / avg**2 + 1

            avg_full.append(avg)
            avg2_full.append(avg2)
            var_full.append(var)
            cov_full.append(cov)
            mean_cov_full.append(means_cov)
            var_cov_full.append(var_cov)
            sparse_full.append(sparse)

        if self.normalize_coefficients_p:
            self.normalize_coefficients(avg_full, var_full, cov_full, var_cov_full, eps=self.eps_renorm)

        return avg_full, cov_full, sparse_full, var_cov_full

    def coeffs_to_dict(self, cov_coeffs):
        '''
        Helper function that copies all coefficients to a dictionary, one entry for each coefficient group.
        The keys are the same than in idx_info.
        '''

        # First order coefficients
        cov_coeffs_new = []
        for ord, coef in enumerate(cov_coeffs):  # Loop order
            #print('shape full ', coef.shape)
            dict_tmp = {}
            start = 0
            for key in self.coeff_select[ord]:
                end = start + self.idx_info[ord][key]['k'].shape[0]
                dict_tmp[key] = coef[..., start : end, :]
                start = end
            cov_coeffs_new.append(dict_tmp)
        return cov_coeffs_new


    def num_coeff(self):
        """Returns the effective number of (real) coefficients in the embedding"""

        num_coeff = 0
        for key in self.coeff_select:
            num_coeff += self.idx_info[key]['ncoef_real']
            num_coeff += 2 * self.idx_info[key]['ncoef_cplx']

        return num_coeff

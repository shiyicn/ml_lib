# Various helper functions for the synthesis of random processes.
#
# Roberto Fabio Leonarduzzi
# January, 2019


import numpy as np
import warnings

def gaussian_cme(cov, N, R):
    '''
    Create R realizations of a gaussian process of length N with the specified
    autocovariance through circulant matrix embedding.
    '''

    # Circulant matrix embedding: fft of periodized autocovariance:
    cov = np.concatenate((cov, np.flip(cov[1:-1])), axis=0)
    L = np.fft.fft(cov)[:, None]
    if np.any(np.real(L) < 0):
        warnings.warn('Found FFT of covariance < 0. Embedding matrix is not non-negative definite.')

    # Random noise in Fourier domain
    z = np.random.randn(2*N - 2, R) + 1j * np.random.randn(2*N - 2, R)

    # Impose covariance and invert
    # Use fft to ignore normalization, because only real part is needed.
    x = np.real(np.fft.fft(z * np.sqrt(L / (2*N - 2)), axis=0))

    # First N samples have autocovariance cov:
    x = x[:N, :]

    return x

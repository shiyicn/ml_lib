# Synthesis of fractional brownian motions through circulant matrix embedding.
#
# Roberto Fabio Leonarduzzi
# January, 2019

import numpy as np
import warnings
from simulation.pzutils import gaussian_cme

def fbm(shape, H, sigma=1, dt=None):
    '''
    Create a realization of fractional Brownian motion using circulant
    matrix embedding.

    Inputs:
      - shape: if scalar, it is the  number of samples. If tuple it is (N, R), the
               number of samples and realizations, respectively.
      - H (scalar): Hurst exponent.
      - sigma (scalar): variance of process

    Outputs:
      - fbm: synthesized fbm realizations. If 'shape' is scalar, fbm is of shape (N,).
             Otherwise, it is of shape (N, R).
    '''

    try:
        N, R = shape
        do_squeeze = False
    except TypeError:  # shape is scalar
        N, R = shape, 1
        do_squeeze = True

    if not 0 <= H <= 1:
        raise ValueError('H must satisfy 0 <= H <=1')

    if not dt:
        dt = 1 / N

    # Create covariance of fGn
    n = np.arange(N)
    r = dt**(2*H) * sigma**2 / 2 * (np.abs(n+1)**(2*H) + np.abs(n-1)**(2*H) - 2 * np.abs(n)**(2*H))
    # # Circulant matrix embedding: fft of periodized autocovariance:
    # r = np.concatenate((r, np.flip(r[1:-1])), axis=0)
    # L = np.fft.fft(r)[:, None]
    # if np.any(np.real(L) < 0):
    #     warnings.warn('Found FFT of covariance < 0. Embedding matrix is not non-negative definite.')

    # # Random noise in Fourier domain
    # z = np.random.randn(2*N - 2, R) + 1j * np.random.randn(2*N - 2, R)

    # # Impose covariance and invert
    # # Use fft to ignore normalization, because only real part is needed.
    # x = np.real(np.fft.fft(z * np.sqrt(L) / (2*N - 2), axis=0))

    # # First N samples have the correct covariance:
    # fbm = np.cumsum(x[:N, :], axis=0)

    fbm = np.cumsum(gaussian_cme(r, N, R), axis=0)

    return fbm.squeeze() if do_squeeze else fbm

# PhaseHarmonic1D
# Copyrigth (c) by Gaspar Rochette, Roberto Leonarduzzi and Stéphane Mallat

# PhaseHarmonic1D is licensed under a
# Creative Commons Attribution-NonCommercial 4.0 International License.

# You should have received a copy of the license along with this
# work. If not, see <https://creativecommons.org/licenses/by-nc/4.0/>.

import sys
import math
from math import factorial as fct
import numpy as np
from kymatio.scattering1d import filter_bank as fb
from numba import jit


def compute_anti_aliasing_filt(N, p):
    freq = np.fft.fftfreq(N)
    freq[N // 2:] += 1
    aaf = np.ones_like(freq)  # initialize Anti Aliasing Filter
    idx = freq >= 0.75
    freq = freq[idx] * 4 - 3.
    freq_pow = np.power(freq, p)
    acc, a = np.zeros_like(freq), 0
    for k in range(p + 1):
        freq_pow *= freq
        bin = fct(p) / (fct(k) * fct(p - k))
        sgn = 1. if k % 2 == 0 else -1.
        acc += bin * sgn * freq_pow / (p + k + 1)
        a += bin * sgn / (p + k + 1)
    dom_coeff = -1 / a
    aaf[idx] = dom_coeff * acc + 1.
    return aaf

@jit
def compute_morlet_parameters(N, Q, analytic=False):
    sigma0 = 0.1
    sigma_low = sigma0 / math.pow(2, N)

    if analytic:
        xi_curr = fb.compute_xi_max(Q)  # initialize at max possible xi
    else:
        xi_curr = 0.5

    r_psi = np.sqrt(0.5)
    sigma_curr = fb.compute_sigma_psi(xi_curr, Q, r=r_psi)  # corresponds to xi_curr

    xi, sigma = [], []
    factor = 1. / math.pow(2., 1. / Q)

    # geometric scaling
    while sigma_curr > sigma_low:
        xi.append(xi_curr)
        sigma.append(sigma_curr)
        xi_curr *= factor
        sigma_curr *= factor

    # affine scaling
    last_xi = xi[-1]
    num_intermediate = Q - 1
    for q in range(1, num_intermediate + 1):
        factor = (num_intermediate + 1. - q) / (num_intermediate + 1.)
        xi.append(factor * last_xi)
        sigma.append(sigma_low)

    return xi, sigma, sigma_low


@jit
def compute_battle_lemarie_parameters(N, Q, high_freq=0.5):
    xi_curr = high_freq
    xi, sigma = [], []
    factor = 1. / math.pow(2., 1. / Q)
    for nq in range(N * Q):
        xi.append(xi_curr)
        xi_curr *= factor

    return xi, sigma


# BL_XI0 = 0.7593990773014584
BL_XI0 = 0.75 * 1.012470304985129


@jit
def battle_lemarie_psi(N, Q, xi):
    if Q != 1:
        raise NotImplementedError("Scaling battle-lemarie wavelets to multiple wavelets per octave not implemented yet.")
    xi0 = BL_XI0  # mother wavelet center

    # frequencies for mother wavelet with 1 wavelet per octave
    abs_freqs = np.linspace(0, 1, N + 1)[:-1]
    # frequencies for wavelet centered in xi with 1 wavelet1 per octave
    freqs = abs_freqs * xi0 / xi
    # frequencies for wavelet centered in xi with Q wavelets per octave
    # freqs = xi0 + (xi_freqs - xi0) * Q

    num, den = b_function(freqs)
    num2, den2 = b_function(freqs / 2)
    numpi, denpi = b_function(freqs / 2 + 0.5)

    stable_den = np.empty_like(freqs)
    stable_den[freqs != 0] = np.sqrt(den[freqs != 0])  / (2 * np.pi * freqs[freqs != 0]) ** 4
    # protection in omega = 0
    stable_den[freqs == 0] = 2 ** (-4)


    mask = np.mod(freqs, 2) != 1
    stable_den[mask] *= np.sqrt(den2[mask] / denpi[mask])
    mask = np.mod(freqs, 2) == 1
    # protection in omega = 2pi [4pi]
    stable_den[mask] = np.sqrt(den2[mask]) / (np.pi * freqs[mask]) ** 4

    psi_hat = np.sqrt(numpi / (num * num2)) * stable_den
    psi_hat[freqs < 0] = 0

    return psi_hat

@jit
def battle_lemarie_phi(N, Q, xi_min):
    xi0 = BL_XI0  # mother wavelet center

    abs_freqs = np.fft.fftfreq(N)
    freqs = abs_freqs * xi0 / xi_min
    # freqs = xi_freqs * Q

    num, den = b_function(freqs)

    stable_den = np.empty_like(freqs)
    stable_den[freqs != 0] = np.sqrt(den[freqs != 0]) / (2 * np.pi * freqs[freqs != 0]) ** 4
    stable_den[freqs == 0] = 2 ** (-4)

    phi_hat = stable_den / np.sqrt(num)
    return phi_hat


@jit
def b_function(freqs, eps=1e-7):
    cos2 = np.cos(freqs * np.pi) ** 2
    sin2 = np.sin(freqs * np.pi) ** 2

    num = 5 + 30 * cos2 + 30 * sin2 * cos2 + 70 * cos2 ** 2 + 2 * sin2 ** 2 * cos2 + 2 * sin2 ** 3 / 3
    num /= 105 * 2 ** 8
    sin8 = sin2 ** 4

    return num, sin8



def compute_bump_steerable_parameters(N, Q, high_freq=0.5):
    return compute_battle_lemarie_parameters(N, Q, high_freq=high_freq)


def low_pass_constants(Q):
    """Function computing the ideal amplitude and variance for the low-pass of a bump
    wavelet dictionary, given the number of wavelets per scale Q.
    The amplitude and variance are computed by minimizing the frame error eta:
        1 - eta <= sum psi_la ** 2 <= 1 + eta
    Simple models are then fitted to compute those values quickly.
    The computation was done using gamma = 1.
    """
    ampl = -0.04809858889110362 + 1.3371665071917382 * np.sqrt(Q)
    xi2sigma = np.exp(-0.35365794431968484 - 0.3808886546835562 / Q)
    return ampl, xi2sigma


@jit
def bump_steerable_psi(N, Q, xi):
    abs_freqs = np.linspace(0, 1, N + 1)[:-1]
    psi = hwin((abs_freqs - xi) / xi, 1.)

    return psi


# @jit
# def bump_steerable_psi(N, Q, xi):
#     sigma = xi * BS_xi2sigma

#     abs_freqs = np.linspace(0, 1, N + 1)[:-1]
#     psi = hwin((abs_freqs - xi) / sigma, 1.)

#     return psi


@jit
def bump_steerable_phi(N, Q, xi_min):
    ampl, xi2sigma = low_pass_constants(Q)
    sigma = xi_min * xi2sigma

    abs_freqs = np.abs(np.fft.fftfreq(N))
    phi = ampl * np.exp(- (abs_freqs / (2 * sigma)) ** 2)

    return phi


# @jit
# def bump_steerable_phi(N, Q, xi_min):
#     sigma = xi_min * BS_xi2sigma

#     abs_freqs = np.abs(np.fft.fftfreq(N))
#     phi = hwin(abs_freqs / sigma, 1.)

#     return phi


@jit
def hwin(freqs, gamma1):
    psi_hat = np.zeros_like(freqs)
    idx = np.abs(freqs) < gamma1

    psi_hat[idx] = np.exp(1. / (freqs[idx] ** 2 - gamma1 ** 2))
    psi_hat *= np.exp(1 / gamma1 ** 2)

    return psi_hat


@jit
def compute_meyer_parameters(N, Q, high_freq):
    return compute_battle_lemarie_parameters(N, Q, high_freq=high_freq)


@jit
def meyer_psi(N, Q, xi):
    #if Q != 1:
    #    raise NotImplementedError("Scaling Meyer wavelets to multiple wavelets per octave not implemented yet.")

    # frequencies for mother wavelet with 1 wavelet per octave
    abs_freqs = np.linspace(-0.5, 0.5, N + 1)[:-1]
    psi = meyer_mother_psi(8/3 * np.pi * (abs_freqs) / xi)
    return np.fft.fftshift(psi)

@jit
def meyer_phi(N, Q, xi):
    abs_freqs = np.linspace(-0.5, 0.5, N + 1)[:-1]
    phi = meyer_mother_phi(8/3 * np.pi * (abs_freqs) / xi)
    return np.fft.fftshift(phi)


def nu(x):
    out = np.zeros(x.shape)
    idx = np.logical_and(0 < x, x < 1)
    out[idx] = x[idx]**4 * (35 - 84*x[idx] + 70*x[idx]**2 - 20*x[idx]**3)
    return out

def meyer_mother_psi(w):
    psi = np.zeros(w.shape) + 1j * np.zeros(w.shape)
    idx = np.logical_and(2*np.pi/3 < w, w < 4*np.pi/3)
    psi[idx] = np.sin(np.pi/2 * nu(3*np.abs(w[idx])/2/np.pi - 1))  / np.sqrt(2*np.pi) # * np.exp(1j*w[idx]/2)

    idx = np.logical_and(4*np.pi/3 < w, w < 8*np.pi/3)
    psi[idx] = np.cos(np.pi/2 * nu(3*np.abs(w[idx])/4/np.pi - 1))  / np.sqrt(2*np.pi) # * np.exp(1j*w[idx]/2)

    return 2 * psi

def meyer_mother_phi(w):
    phi = np.zeros(w.shape) + 1j * np.zeros(w.shape)
    idx = np.abs(w) < 2*np.pi/3
    phi[idx] = 1 / np.sqrt(2*np.pi)
    idx = np.logical_and(2*np.pi/3 < np.abs(w), np.abs(w) < 4*np.pi/3)
    phi[idx] = np.cos(np.pi/2 * nu(3*np.abs(w[idx])/2/np.pi - 1)) / np.sqrt(2*np.pi)
    return  phi * 2

# --------- Implementation of Custom Causal Wavelet functions -------------- #

"""
Note on the fft and ifft

Given a filter h and a signal x, the following relation holds:
    x_hat = np.fft.fft(x)
    h_hat = np.fft.fft(h)
    convolution = np.ifft(x_hat, h_hat)
    =>  convolution(T -t-1) = <np.flip(h), np.roll(x, t)>

"""


GLOBAL_CONST_REAL_IMAG_FLAG = ['Real', 'Imag']

def compute_haar_parameters(N, Q, high_freq = None):
    
    """
    Compute parameters for Haar Wavelet
    
    Args:
    -----
    N : integer, max scale of the wavelet filters

    Q : integer, wavelet per octave

    high_freq : None, for Haar wavelet, it's ignored


    Returns:
    --------
    xi : list of integer, for Haar wavelet, it's simply the scales
    
    sigma : list of float, for Haar wavelet, it's just an empty list
    
    """

    if high_freq is not None:
        print('high_freq parameter is ignored for Haar Wavelet.')
    
    if Q != 1:
        raise ValueError('Q cannot be different from 1, now Q = {} .'.format())

    xi_curr = high_freq
    xi, sigma = [], []
    
    xi = list(np.arange(N) + 1)

    return xi, sigma


def compute_gammatone_parameters(N, Q, high_freq = None):
    
    """
    Compute parameters for Haar Wavelet
    
    Args:
    -----
    N : integer, max scale of the wavelet filters

    Q : integer, wavelet per octave

    high_freq : None, for Haar wavelet, it's ignored


    Returns:
    --------
    xi : list of integer, for Haar wavelet, it's simply the scales
    
    sigma : list of float, for Haar wavelet, it's just an empty list
    
    """

    if high_freq is not None:
        print('high_freq parameter is ignored for Haar Wavelet.')
    
    if Q != 1:
        raise ValueError('Q cannot be different from 1, now Q = {} .'.format())

    xi_curr = high_freq
    xi, sigma = [], []
    
    xi = list(np.arange(N) + 1)

    return xi, sigma


def haar_psi(N, Q, xi, debug = False, norm_type = 'l2', return_fft = True):
    """
    Compute the Wavelet Filter Bank for Haar

    Args:
    -----
    N : integer
        the length of the input signal
    
    Q : integer
        the num of voices per octave
    
    xi : integer
         the scale factor of the wavelet 


    Returns:
    --------
    psi_hat : array-like of complex numbers
              the Fourier transform of the Haar Wavelet Filter
    """

    if norm_type != 'l2':
        raise ValueError('Normalization type cannot yet be different from l2, now norm_type = {}'.format(norm_type))
    
    if Q != 1:
        raise ValueError('Q cannot be different from 1, now Q = {} .'.format(Q))
    
    if 2**xi > N:
        raise ValueError('Support of the wavelet exceeds the signal length : 2**xi > N with xi = {}, N = {} .'.format(xi, N))

    # frequencies for mother wavelet with 1 wavelet per octave
    normalization_const = 2**(-xi / 2.0)
    times = np.zeros(N)
    psi = times
    psi[:2**(xi-1)] = 1
    psi[2**(xi-1):2**xi] = -1
    psi = psi * normalization_const
    
    psi_hat = np.fft.fft(psi)
    if debug:
        print('Wavelet filter : {}'.format(str(psi)))
        print('In Fourier : {}'.format(str(psi_hat)))
    
    if return_fft:
        return psi_hat
    else:
        return psi
    
    return None


def haar_phi(N, Q, xi, debug = False, norm_type = 'l2'):
    """
    Functions to compute the scaling function for Haar scaling filter

    Args:
    -----
    N : integer
        input signal's length
    
    Q : integer
        num of wavelets per octave
    
    xi : integer
         scale of the dilated wavelet

    debug : bool
            flag of the debug mode activation
    
    norm_type : str
                the type of normalization to be used, by defalt, it's l2 norm

    
    Returns:
    --------
    phi_hat : array-like of complex numbers
              the scaling filter of Haar in the Fourier domain

    
    """
    if norm_type != 'l2':
        raise ValueError('Normalization type cannot yet be different from l2, now norm_type = {}'.format(norm_type))
    
    if Q != 1:
        raise ValueError('Q cannot be different from 1, now Q = {} .'.format(Q))
    
    if 2**xi > N:
        raise ValueError('Support of the wavelet exceeds the signal length : 2**xi > N with xi = {}, N = {} .'.format(xi, N))
    
    normalization_const = 2**(-xi / 2.0)
    times = np.zeros(N)
    phi = times
    phi[:2**xi] = 1
    phi = phi * normalization_const
    
    phi_hat = np.fft.fft(phi)
    if debug:
        print('Wavelet filter : {}'.format(str(phi)))
        print('In Fourier : {}'.format(str(phi_hat)))
    return phi_hat


def haar_mother(x):
    normalization_const = np.sqrt(0.5)
    fx = (2 * (x <= 0.5).astype(int)) - 1
    fx = fx * normalization_const # normalize the mother wavelet
    return fx


def gammatone_psi(N, Q, j, central_freq = 0.8, order = 4, band_width = 0.8, 
                  using_analytical_form = False, norm_type = 'l2', 
                  debug = False, return_fft = True):
     
    # the other voice per octave is not yet supported
    if Q != 1:
        raise ValueError('Q cannot be different from 1, now Q = {}.'.format(Q))
     
    if using_analytical_form:
        psi_hat = 1j * w * np.math.factorial(order-1) / \
                (band_width + 1j * (w - 2 * np.pi * central_freq))
        psi = None
    else:
        t = np.arange(N)
        psi = 2**(-j / 2.0) * gammatone_mother(t / 2**j, central_freq, 
                                              order=order, 
                                              band_width=band_width, 
                                              phase=0)
        psi = psi / np.linalg.norm(psi)
        psi_hat = np.fft.fft(psi)
    
    if debug:
        print('In debug mode, return both time and frequency representations.')
        return psi, psi_hat
    
    if return_fft:
        return psi_hat
    else:
        return psi
    
    return None


def gammatone_phi(N, Q, xi, band_width = 0.1, order = 4, 
                  debug = False, norm_type = 'l2'):
    """
    Not properly implemented, the scaling function of Gammatone Wavelet needs to 
    be studied. 

    Args:
    -----
    N : integer
        input signal's length
    
    Q : integer
        num of wavelets per octave
    
    xi : integer
         scale of the dilated wavelet
    
    band_width : float
                the support of the mother wavelet, the typical support is 
                approximated by 1.0 / {band_width}
    
    order : integer, 
            order of the Gammatone filters
    
    debug : bool, 
            the flag for debug mode
    
    
    Returns:
    --------
    phi_hat : array-like, shape (N, )
              the scaling function of the Gammatone Wavelet Filters

    """
    w = np.arange(N) + 1
    band_width_scale = - band_width / 2**xi
    phi_hat = (1 - np.exp(-band_width_scale))**order / \
              (1 - np.exp(-band_width_scale) * np.exp(-1j * w))**order
    return phi_hat


def gammatone_mother(t, central_freq, order = 4, band_width = 1,
                     phase = 0, analytic = True, sampling_rate = 1):
    
    """
    The Gammatone Wavelet Mother Function
    
    Following the instruction of :
        https://scatteringm.readthedocs.io/en/latest/manual/wavelets.html
    
    ψ(t)=((−α+iξ)t^{N−1} + (N−1) * t^{N−2}) * exp(−αt) * exp(2iπξt)
    
    ψ(t) = 0 when t <= 0
    
    α = band_width
    ξ = central_freq
    N = order of the wavelet
    
    The integer N, called order in the specifications, is equal to 4 by 
    default. The bigger the N, the more symmetric (hence “Morlet-like”) the 
    wavelet will be. 
    
    
    Note:
    -----
    The attenuation parameter α can be automatically inferred from the 
    required quality factor, through a tedious closed-form equation. It needs 
    implementation. 
    
    The [0, 1/α] is roughly the support of ψ. 

    The ξ determines the central frequency in the Fourier plan. 
    

    Args:
    -----
    t : array-like, containing the time stamps 
    
    central_freq : float, the central frequency, for the sampling issue

    band_width : float, 1/{band_with} is roughly the size of the support of the 
                 mother wavelet
    
    order : integer, by default the order of the Gammatone wavelet

    phase : float, by default, the phase is set to be 0, i.e. no phase translation

    analytic : bool, by default, the real wavelet is used

    Returns:
    --------
    gt : array-like, containing the Gammatone function values in time

    """
    
    if analytic:
        print('Analytic Version of Gammatone is used.')
    else:
        print('Real Gammatone Wavelet is used.')
    
    if sampling_rate != 1:
        raise ValueError('Sampling Rate cannot be different from 1, now sampling_rate is {}'.format(sampling_rate))
        
    
    # adjust the mother function by sampling rate
    t = t * sampling_rate
    
    if not analytic:
        gt = t**(order - 1) * np.cos(2 * np.pi * central_freq * t + phase) * \
             np.exp(- 2 * np.pi * band_width * t)
    else:
        gt = ((-band_width + 1j * central_freq) * t**(order - 1) + (order - 1) *\
             t**(order - 2)) * np.exp(-band_width * t) * np.exp(2 * 1j * np.pi *\
             central_freq * t)
    
    return gt


def gammatone_band_width(N, Q, j):
    """
    
    """
    return None


def filter_banks(J, wavelet_type = 'haar', Q = 1, max_supp = None):
    # create a filter_bank for the wavelet transform
     
    print('Create a filter bank with wavelet of type {}'.format(wavelet_type))
     
    if max_supp is None:
        # if the maximum support size is not given, the default size of the 
        # support of the wavelet will be fixed as 2^J
        max_supp = 2**J
     
    filters = []
     
    if wavelet_type == 'haar':
        for jj in (np.arange(J)+1):
            x = np.arange(2**jj) + 1
            filter_j = 2**(-jj / 2) * haar_mother(x / 2**jj)
            filters.append(filter_j)
    elif wavelet_type == 'gammatone':
        return None
    else:
        error_msg = 'This wavelet type : {} is not yet supported.'.format(wavelet_type)
        raise NameError(error_msg)
     
    return filters


def foveal_filters(foveal_type = 'indicator'):
    """
    Implement the foveal wavelet

    Args:
    -----

    Returns:
    --------
    

    """

    if foveal_type is not None:
        raise ValueError('This kind of foveal wavelet [{}] is not supported!'.format(foveal_type))

    return None


def plot_wavelet_filters(filters, scaling_filter = None, scales = None, 
                         plot_complex = False):
    """
    Plot Wavelet Filters for real and complex Wavelets
    
    Args:
    -----
    filters : array-like of float or complex numbers
    
    scaling_filter : array-like of float of complex numbers

    scales : array-like of integers

    plot_complex : bool
                   if or not plot the real and imaginary part of complex filters
    
    Returns:
    --------
    
    
    Note:
    -----
    
    """
    filters = np.array(filters)
    n_filters = filters.shape[0]
    
    if isinstance(filters[0][0], np.complex):
        filters_values = np.abs(filters)
    else:
        filters_values = filters
    
    fig = plt.figure()
    
    ax_time = fig.add_subplot(2, 1, 1)
    ax_freq = fig.add_subplot(2, 1, 2)
    for i in range(n_filters):
        if scales is None:
            j = i
        else:
            j = scales[i]
        
        ax_time.plot(filters_values[i, :], label=str(j))
        ax_time.grid(linestyle='--')
        ax_freq.plot(np.abs(np.fft.fft(filters[i, :])), label=str(j))
        ax_freq.grid(linestyle='--')
    fig.show()
    
    if plot_complex:
        fig, ax = plt.subplots(2, 1, sharey=True)
        funcs = [np.real, np.imag]
        for c_iter, c_flag in enumerate(GLOBAL_CONST_REAL_IMAG_FLAG):
            for i in range(n_filters):
                ax[c_iter].plot(funcs[c_iter](filters[i, :]))
            ax[c_iter].grid(linestyle='--')
        fig.show()
        
    return None


# ------------ End of the Custom implementation ------------------- # 

if __name__ == "__main__":
    import matplotlib.pyplot as plt
    N = 5
    T = 2 ** (10)
    Q = 2
    freqs = np.fft.fftfreq(T)
    fact = math.pow(2, 1 / Q)
    high_freq = 0.55

    #wavelet_type = "battle-lemarie"
    #wavelet_type = "bump_steerable"
    wavelet_type = 'meyer'
    if wavelet_type == "meyer":
        print("Using center frequency {} for mother wavelet".format(BL_XI0))

        abs_freqs = np.linspace(0, 1, T + 1)[:-1]

        phi = battle_lemarie_phi(T, 1, fact ** (-N * Q)) * np.sqrt(Q)
        psi = [battle_lemarie_psi(T, 1, 0.45 * fact ** (-nq)) for nq in range(N * Q)]
        psi.append(phi)
        psi = np.stack(psi, axis=0)

        fxi = 0.34
        fpsi = battle_lemarie_psi(T, 1, fxi)
        mean = np.sum(abs_freqs * fpsi ** 2) / np.sum(fpsi ** 2)
        print("center frequency of {}: {}".format(fxi, mean))
        print("ratio: {}".format(mean / fxi))
        print("value at 0-: {:.2e}".format(psi[0, -1]))
    elif wavelet_type == 'bump_steerable':
        psi = [bump_steerable_psi(T, Q, high_freq * fact ** (-nq)) for nq in range(N * Q)]
        phi = bump_steerable_phi(T, Q, high_freq * fact ** (- N * Q + 1))
        psi = np.stack(psi + [phi], axis=0)
        # psi = np.stack(psi, axis=0)
        print(type(psi))
    elif wavelet_type == 'meyer':
        psi = [meyer_psi(T, Q, high_freq * fact ** (-nq)) for nq in range(N * Q)]
        phi = meyer_phi(T, Q, high_freq * fact ** (-N*Q + 1))
        psi = np.stack(psi + [phi], axis=0)


    abs_freqs = np.linspace(0, 1, T + 1)[:-1]
    # plt.figure()
    # plt.plot(abs_freqs, psi[0, :])
    # plt.show()

    plt.figure()
    for k in range(psi.shape[0]):
        plt.subplot(311)
        plt.plot(abs_freqs, np.real(psi[k, :]))
        plt.subplot(312)
        plt.plot(abs_freqs, np.imag(psi[k, :]))
        plt.subplot(313)
        plt.plot(abs_freqs, np.abs(psi[k, :]))



    plt.figure()
    plt.plot(abs_freqs, np.sum(np.abs(psi) ** 2, axis=0), color='r', linewidth=3)
    plt.show()

    # w = np.linspace(-np.pi, np.pi, 1000)
    # psi = np.abs(meyer_mother_psi(8/3*w))
    # phi = np.real(meyer_mother_phi(8/3*w))

    # plt.figure()
    # plt.plot(w, psi)
    # plt.plot(w, phi)
    # plt.show()


    # plt.figure()
    # plt.plot(abs_freqs, np.abs(meyer_mother_psi(3 * np.pi * (abs_freqs - 0.0)/1)))
    # plt.show()

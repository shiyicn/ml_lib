# load basic configurations
import platform
if platform.system() == 'Windows':
    exec(open("C:/Users/Huang Zuli/proj/ml_lib/signal/scat_basic.py").read())
else:
    exec(open("/Users/huangzuli/proj/ml_lib/signal/scat_basic.py").read())


tmp_dir_debug = os.path.join(tmp_dir_debug, 'vol_adj')


# ----------------- Currency Autoregressive with directly the price ---------------------------------- 
# set up internal parameters
LOWER_BOUND_DATE = create_date(1999, 1, 1) # start from 01/01/2000
DEBUG_MODE = False
FILTER_LENGTH = 20

market_types = ['G10', 'EM']
currencies_market = [os.listdir(os.path.join(root_path, market_type)) for market_type in market_types]

# the currencies to be excluded
currencies2exclude = ['USDMYR.csv', 'USDCNH.csv']

# initialize the pnls to save
save_pnls_spot_baseline = {}
save_pnls_spot_baseline['G10'] = []
save_pnls_spot_baseline['EM'] = []

save_pnls_spot = {}
save_pnls_spot['G10'] = []
save_pnls_spot['EM'] = []

INTERVAL_START = create_date(2000, 1, 1)
INTERVAL_END = create_date(2018, 12, 31)

learning_windows2compute = [5, 10]

# create the csv file to register the results
out_csv_file = {}
for market_type in market_types:
    path2write_metrics = os.path.join(tmp_dir_debug, 'res_metrics_{}.csv'.format(market_type))
    out_csv_file[market_type] = open(path2write_metrics, "w")
    out_csv_file[market_type].write(','.join([
            'LW', 'HR', 'l2-error', 'sharpe', 'Avg Vol', 'Std Vol', 'holdind period']) + '\n')

# compute regressions
for LEARNING_WINDOW in learning_windows2compute:

    # set the path to save the figures
    path2save_fig = os.path.join(tmp_dir_debug,
                                'report_fltr_{}_LW_{}.pdf'.format(FILTER_LENGTH, LEARNING_WINDOW))
    pdf2save = matplotlib.backends.backend_pdf.PdfPages(path2save_fig)
    print('Save all figures into the pdf file located in : {}'.format(path2save_fig))

    pnls_by_market = {}
    for iter_market, market_type in enumerate(market_types):
        hr_by_market = []
        sharpe_by_market = []
        l2_error_by_market = []
        vol_by_market = []
        h_period_by_market = []

        counting_currency = 0
        ccy_processed = []
        
        # register the result for the diversified PnL
        pnls_by_market[market_type] = []
        for currency in currencies_market[iter_market]:
            
            # USDMYR is contant for a long time, USDCNH is short 
            if currency in currencies2exclude: 
                continue
            
            counting_currency += 1 # how many currencies are processed
            ccy_processed.append(currency.replace('.csv', ''))
            
            if platform.system() == 'Windows':
                runfile('C:/Users/Huang Zuli/proj/ml_lib/demo/single_filter_demo.py', 
                        wdir='C:/Users/Huang Zuli/proj/ml_lib/demo')
            else:
                runfile('/Users/huangzuli/proj/ml_lib/demo/single_filter_demo.py', 
                        wdir='/Users/huangzuli/proj/ml_lib/demo')
            
            # spot pnl is calculated in the demo above
            pnls_by_market[market_type].append(pnl_spot)
            
            hr_by_market.append(accuracy)
            sharpe_by_market.append(sharpe)
            vol_by_market.append(vol_in_sharpe)
            l2_error_by_market.append(l2_error)
            h_period_by_market.append(holding_period)
        
        # compute stat
        metrics_to_log = ','.join([str(np.mean(hr_by_market)), 
                                   str(np.mean(l2_error_by_market)),
                                   str(np.mean(sharpe_by_market)), 
                                   str(np.mean(vol_by_market)), 
                                   str(np.std(vol_by_market)), 
                                   str(np.mean(h_period_by_market))])
        
        out_csv_file[market_type].write(','.join(['LW '+str(LEARNING_WINDOW), 
                                                 metrics_to_log]) + '\n')

        pnls_by_market[market_type] = combine(pnls_by_market[market_type])
        # save all pnl into a csv file according to its market type and currency, learning window
        pnls2save_tmp = pnls_by_market[market_type].cumsum()
        pnls2save_tmp.columns = ccy_processed
        pnls2save_tmp.to_csv(os.path.join(tmp_dir_debug, 
                                          'pnl_ccy_{}_LW_{}.csv'.format(
                                                market_type, LEARNING_WINDOW)))
        # save the average spot pnl
        save_pnls_spot[market_type].append(pnls_by_market[market_type].mean(axis=1))
    
    pdf2save.close()

# compute the baseline
pnls_by_market = {}
for iter_market, market_type in enumerate(market_types):
    pnls_by_market[market_type] = []
    
    hr_by_market = []
    sharpe_by_market = []
    l2_error_by_market = []
    vol_by_market = []
    h_period_by_market = []
    
    for currency in currencies_market[iter_market]:
        
        # USDMYR is contant for a long time, USDCNH is short 
        if currency in currencies2exclude: 
            continue
        
        if platform.system() == 'Windows':
            runfile('C:/Users/Huang Zuli/proj/ml_lib/demo/single_filter_baseline.py', 
                    wdir='C:/Users/Huang Zuli/proj/ml_lib/demo')
        else:
            runfile('/Users/huangzuli/proj/ml_lib/demo/single_filter_baseline.py', 
                    wdir='/Users/huangzuli/proj/ml_lib/demo')
        
        # spot pnl is calculated in the demo above
        pnls_by_market[market_type].append(pnl_spot_baseline)
        
        hr_by_market.append(accuracy_baseline)
        sharpe_by_market.append(sharpe_baseline)
        vol_by_market.append(vol_in_sharpe_baseline)
        l2_error_by_market.append(l2_error_baseline)
        h_period_by_market.append(holding_period_baseline)
    
    # generate the metrics summary to be logged
    metrics_to_log = ','.join([str(np.mean(hr_by_market)), 
                           str(np.mean(l2_error_by_market)),
                           str(np.mean(sharpe_by_market)), 
                           str(np.mean(vol_by_market)), 
                           str(np.std(vol_by_market)),
                           str(np.mean(h_period_by_market))])

    out_csv_file[market_type].write(','.join(['Baseline', 
                                             metrics_to_log]) + '\n')
    
    pnls_by_market[market_type] = combine(pnls_by_market[market_type])
    # save the average spot pnl
    save_pnls_spot[market_type].append(pnls_by_market[market_type].mean(axis=1))

for market_type in market_types:
    out_csv_file[market_type].close()

# save the average spot pnl
for market_type in market_types:
    save_pnls_spot[market_type] = combine(save_pnls_spot[market_type])
    colums_name = ['LW ' + str(kk) for kk in learning_windows2compute]
    colums_name.append('Baseline')
    save_pnls_spot[market_type].columns = colums_name

# save the spot pnl and the cumsum pnl into csv file
for market_type in market_types:
    save_pnls_spot[market_type].to_csv(os.path.join(tmp_dir_debug, 
                                       'res_spot_all_{}.csv'.format(market_type)))
    save_pnls_spot[market_type].dropna().cumsum().to_csv(os.path.join(tmp_dir_debug, 
                                       'res_pnl_all_{}.csv'.format(market_type)))

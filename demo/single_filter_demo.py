"""
# execute the basic code
import platform
if platform.system() == 'Windows':
    exec(open("C:/Users/Huang Zuli/proj/ml_lib/signal/scat_basic.py").read())
else:
    exec(open("/Users/huangzuli/proj/ml_lib/signal/scat_basic.py").read())

# set up internal parameters
LEARNING_WINDOW = 1250
LOWER_BOUND_DATE = create_date(2000, 1, 1) # start from 01/01/2000
FILTER_LENGTH = 20

market_type = 'EM'
currency = 'USDIDR.csv'

INTERVAL_START = create_date(2000, 1, 1)
INTERVAL_END = create_date(2004, 12, 31)

# flag to determine the internal behaviors
DEBUG_MODE = True
ADJUSTED_BY_VOL = True
"""

if DEBUG_MODE:
    # set the path to save the figures
    path2save_fig = os.path.join(tmp_dir_debug,
                                'report_fltr_{}_LW_{}.pdf'.format(FILTER_LENGTH, LEARNING_WINDOW))
    pdf2save = matplotlib.backends.backend_pdf.PdfPages(path2save_fig)
    print('Save all figures into the pdf file located in : {}'.format(path2save_fig))

# delete the .csv from currency file ticker
currency = currency.replace('.csv', '')
print('Start to test the currency {} with learning window size {}.'.format(
                                                    currency, LEARNING_WINDOW))

# generate the title of figure to be plotted
flag_of_title = market_type + ' ' + currency
# generate the path of the currency file to be loaded
file2load_path = os.path.join(root_path, market_type, currency + '.csv')
print('Load file from path {}.'.format(file2load_path))
 
data_loaded = pd.read_csv(file2load_path)
# filter the time series by the starting time, lower bound of date range
data_loaded = filter_by_time(data_loaded['Date'], data_loaded, lower_bound=LOWER_BOUND_DATE)

# set the index of loaded time series to excel format date
data_loaded.set_index('Date', inplace=True)
prices = data_loaded['PX_LAST'].to_frame()
returns = get_return(prices, mode='geometry')['PX_LAST'] # convert to daily log-difference return
returns.name = 'Return' # return must be uni-dimensional time-series

if not np.all(np.isnan(returns)):
    print('NaN value exists in returns, in total {}.'.format(np.sum(np.isnan(returns))))

# store the date ticker of the prices into datetime format
prices_dates = excel2pandas(returns.index)
T = len(returns) # total length of the time series

# calculate the next day return
next_return = returns.shift(periods=-1)
next_return.name = 'Next Return'

vol_spot = mf.volatility(returns)

# ----------------------------------------------------------------------------
# Benchmark 1:
fltrs = [np.ones(FILTER_LENGTH) / np.sqrt(FILTER_LENGTH)]

X_filtered = [mf.filtering(returns, fltr, mark_nan = True) for fltr in fltrs]
X_filtered = pd.DataFrame(X_filtered).T
X_filtered.columns = ['Filtered Return']

# compute volatility adjusted features
X_filtered_adjusted_by_vol = X_filtered / vol_spot.values
X_filtered_adjusted_by_vol.columns = ['Filtered Returns Adj']
returns_adjusted_by_vol = returns / vol_spot.values.squeeze()
returns_adjusted_by_vol.name = 'Returns Adj'
next_return_adjusted_by_vol = returns_adjusted_by_vol.shift(periods=-1)
next_return_adjusted_by_vol.name = 'Next Return Adj'

# Regression Part
r_hat_rolling, coefs_rolling = rolling_regression(X_filtered_adjusted_by_vol, 
                                                  next_return_adjusted_by_vol, 
                                                  flag_separate = True,
                                                  learning_window=LEARNING_WINDOW)
r_hat_rolling.columns = ['Reg LW {}'.format(LEARNING_WINDOW)]
coefs_rolling.columns = ['Coef LW {}'.format(LEARNING_WINDOW)]

# compute trading related stats
# if the adjusted return and the filtered return are used, no needs for the vol adjustment
positions, mu = generate_position_by_cap(r_hat_rolling, returns.to_frame(), 
                                         vol_adj_est_return=False, cap=3)
pnl, pnl_spot = cal_pnl(positions, returns)
trade = cal_trade(positions)

"""
# ---------------------------------------------------------------------------
# plot signal
plot_estimation(prices, dates = prices_dates, pdf2save = pdf2save, 
                            flag_of_title = flag_of_title, 
                            force_tilte = 'Price', 
                            force_ylabel = '$PX_t$')
plot_estimation(vol_spot, dates = prices_dates, pdf2save = pdf2save, 
                            flag_of_title = flag_of_title, 
                            force_tilte = 'Volatility', 
                            force_ylabel = '$\sigma_t$')
plot_estimation(X_filtered_adjusted_by_vol, dates = prices_dates, pdf2save = pdf2save, 
                            flag_of_title = flag_of_title, 
                            force_tilte = 'Feature', 
                            force_ylabel = '$\mu = (r \star \psi) / \sigma$')
plot_estimation(next_return_adjusted_by_vol, dates = prices_dates, pdf2save = pdf2save, 
                            flag_of_title = flag_of_title, 
                            force_tilte = 'Next Day Return Adj', 
                            force_ylabel = '$r_t/\sigma_t$')
plot_estimation(coefs_rolling, dates = prices_dates, pdf2save = pdf2save, 
                            flag_of_title = flag_of_title, 
                            force_tilte = 'Coef Reg', 
                            force_ylabel = '$\\beta_t$')
plot_estimation(mu, dates = prices_dates, pdf2save = pdf2save, 
                            flag_of_title = flag_of_title, force_tilte = 'Estimation Cap ', 
                            force_ylabel = '$\hat r_t$')
plot_estimation(positions, dates = prices_dates, pdf2save = pdf2save, 
                            flag_of_title = flag_of_title, force_tilte = '$Position$', 
                            force_ylabel = '$Pos_t$')
plot_estimation(trade, dates = prices_dates, pdf2save = pdf2save, 
                            flag_of_title = flag_of_title, force_tilte = '$Trade$', 
                            force_ylabel = '$trade_t$')
plot_estimation(pnl, dates = prices_dates, pdf2save = pdf2save, 
                            flag_of_title = flag_of_title, force_tilte = '$PnL$', 
                            force_ylabel = '$PnL_t$')
# ---------------------------------------------------------------------------
"""

# generate the interval filter
interval_filter = (prices_dates >= INTERVAL_START) & (prices_dates <= INTERVAL_END)
r_hat_filtered = filter_by_time(None, r_hat_rolling, filter_bound=interval_filter)
next_return_adj_filtered = filter_by_time(None, next_return_adjusted_by_vol, 
                                          filter_bound=interval_filter)
pnl_spot_filtered = filter_by_time(None, pnl_spot, filter_bound=interval_filter)
trade_filtered = filter_by_time(None, trade, filter_bound=interval_filter)
positions_filtered = filter_by_time(None, positions, filter_bound=interval_filter)

# compute the required statistics
l2_error = rmse(r_hat_filtered, next_return_adj_filtered)[0]
accuracy = hit_rate(r_hat_filtered, next_return_adj_filtered)[0]
sharpe = cal_sharpe_ratio(pnl_spot_filtered)
vol_in_sharpe = np.std(pnl_spot_filtered.dropna().values) # used for average vol and std of vol
holding_period = cal_holding_period(positions_filtered, trade_filtered)[0]

if DEBUG_MODE:
    res2save = [data_loaded['PX_LAST'], returns, next_return, X_filtered, vol_spot, 
                X_filtered_adjusted_by_vol, returns_adjusted_by_vol, next_return_adjusted_by_vol, 
                coefs_rolling, r_hat_rolling, mu, positions, trade, pnl_spot, pnl]
    result2debug = combine(res2save)
    result2debug.to_csv(os.path.join(tmp_dir_debug, 'reg_{}_LW_{}_debug.csv'.format(currency, LEARNING_WINDOW)))
    pdf2save.close()

# avoid generate the pycache file
import sys
sys.dont_write_bytecode = True

import numpy as np
import torch
import matplotlib.pyplot as plt
from time import time
import os
import scipy as sp
import pandas as pd

# Add path to soft ------------------------------------------------------------
import platform
if platform.system() == 'Windows':
    sys.path.append('C:\\Users\\Huang Zuli\\scoop\\lib_source\\signals\\PhaseHarmonic1D_CC')
else:
    sys.path.append('/Users/Huangzuli/proj/lib_sources/PhaseHarmonic1D_CC/')

if platform.system() == 'Windows':
    sys.path.append('C:\\Users\\Huang Zuli\\proj\\')
else:
    sys.path.append('/Users/Huangzuli/proj/')

from ml_lib.signal.scattering import *
from ml_lib.monte_carlo.simulations import poisson_proc
from ml_lib.signal.convert_date_time import *

from metric import PhaseHarmonicCoeff as PhaseHarmonicPrunedSeparated
#from metric import PhaseHarmonicPrunedSeparated
from simulation.fbm import fbm
from simulation.mrw import mrw, skewed_mrw
# -----------------------------------------------------------------------------

# -------------------- Set up parameters ----------------------------------- #
do_cuda = False

#----- Analysis parameters -----
T = 2**13  # Data length
J = 7      # Width of low-pass filter / number of scales in wavelet decomposition
Q = 1      # Number of voices per octave

# Parameters for phase harmonic coefficients
deltaj = 2   # Largest scale difference j - j' for harmonic coefficients
num_k_modulus = 2  # Number k of phase harmonics for mixed coefficients
compute_second_order = True

wavelet_type = 'morlet'
high_freq = 0.425  # for battle_lemarie, ignored for morlet

coeff_select = [['jeqmod', 'harmonic', 'mixed', 'harmod'], ['jeqmod']]

phe_params = {
    'coeff_select': coeff_select,
    'delta_j': deltaj, 'num_k_modulus': num_k_modulus,
    'wav_type': wavelet_type,
    'high_freq' : high_freq,
    'compute_second_order': compute_second_order, 
    'normalize_coefficients_p': True, 
    'remove_mean_p': True, 
    'wav_norm': 'l2'}
# Create analysis object
phi = PhaseHarmonicPrunedSeparated(J, Q, T,  **phe_params)
# -----------------------------------------------------------------------------

# ----------------------------------------------------------------------------
# ---------- Set up the working path -----------------------------------------
if platform.system() == 'Windows':
    datapath2save_root = 'C:\\Users\\Huang Zuli\\proj\\data\\results_produced\\'
    root_path = 'C:\\Users\\Huang Zuli\\proj\\data\\trading\\FXSpot\\'
else:
    datapath2save_root = '/Users/Huangzuli/proj/data/res/proc/'
    root_path = '/Users/Huangzuli/proj/data/trading/FXSpot'
# ----------------------------------------------------------------------------

# ----------------- Currency Scattering Demo ---------------------------------- 
# Process all available currencies

LOWER_BOUND_DATE = create_date(2000, 1, 1) # start from 01/01/2000

market_types = ['G10', 'EM']
currencies_market = [os.listdir(os.path.join(root_path, market_type)) for market_type in market_types]

max_k_diff = 3

for iter_market, market_type in enumerate(market_types):
    for currency in currencies_market[iter_market]:
        N_SIMULATIONS = 1
         
        # Set up the coefficients
        modulus_coefs = []
        sparsity_coefs = []
        phase_harmonics_corr_coefs = []
        modulus_corr_coefs = []
        mixed_covariance_coefs = []
        modulus_square_scat = []
        mixed_diff_coefs = []
         
        currency = currency.replace('.csv', '')
        print('Process {} in {} .'.format(currency, market_type))
         
        file2load_path = os.path.join(root_path, market_type, currency + '.csv')
        data_loaded = pd.read_csv(file2load_path)
        data_loaded = filter_by_time(data_loaded['Date'], data_loaded, lower_bound=LOWER_BOUND_DATE)
         
        data_loaded.set_index('Date', inplace=True)
        X_init = np.log2(data_loaded['PX_LAST'])
        X_init = (X_init.diff())
         
        # delet NaN from the currency data
        if not np.all(np.isnan(X_init)):
            print('NaN value exists, in total {}.'.format(np.sum(np.isnan(X_init))))
        
        X_dates = X_init.index.values[np.logical_not(np.isnan(X_init))]
        print("Compute From [{}] to [{}].".format(X_dates[0], X_dates[-1]))
        X_init = X_init[np.logical_not(np.isnan(X_init))]
         
        # delet NaN from the currency data
        if np.sum(np.isnan(X_init)) != 0:
            X_init = X_init[np.logical_not(np.isnan(X_init))]
        
        datapath2save = os.path.join('real', market_type, currency)
        flag2save = market_type + '_' + currency
         
        flagOfTitle = currency
         
        for x in cutProcessInTime(X_init, nCut=N_SIMULATIONS):
            T = len(x)
            # print(T)
            phi = PhaseHarmonicPrunedSeparated(J, Q, T,  **phe_params)
            idx_info, y_torch = analyze(x, J, phe_params, T = None, Q = 1, do_cuda = False)
            
            results = scatteringMoments(y_torch, idx_info, phi, J, order = 1, 
                                        deltaj = 2, set_of_coefs = 'all', 
                                        max_k_diff=max_k_diff)
            results['signal'] = X_init
            results['date'] = excel2pandas(X_dates)
            modulus_coefs.append(results['modulus_scat'])
            sparsity_coefs.append(results['sparsity'])
            phase_harmonics_corr_coefs.append(results['phase_harmonics_correlation'])
            modulus_corr_coefs.append(results['modulus_corr'])
            mixed_covariance_coefs.append(results['mixed_covariance'])
            modulus_square_scat.append(results['modulus_square_scat'])
            mixed_diff_coefs.append(results['mixed_by_diff'])
        
        # Plot and Post-process the obtained results
        coefs_dict = {
                'signal' : results['signal'], 
                'date' : results['date'], 
                'modulus_scat' : modulus_coefs, 
                'sparsity' : sparsity_coefs, 
                'phase_harmonics_correlation' : phase_harmonics_corr_coefs, 
                'modulus_corr' : modulus_corr_coefs, 
                'mixed_covariance' : mixed_covariance_coefs, 
                'modulus_square_scat' : modulus_square_scat,
                'mixed_by_diff' : mixed_diff_coefs
            }
        
        generate_report(datapath2save_root, datapath2save, flag2save, 
                    flagOfTitle, N_SIMULATIONS, coefs_dict, J, deltaj, 
                    max_k_diff)

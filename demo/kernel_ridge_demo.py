import os
import sys

lib_dir = os.path.expanduser('~/proj/ml_lib/')
sys.path.append(lib_dir)

import numpy as np
import matplotlib.pyplot as plt
from src.alastor.kernel.kernel_ridge import KernelRidge
from src.alastor.kernel.kernels import gaussian_kernel

N = 1000
sigma_noise = 0.1

X = np.linspace(0, 2 * np.pi, N).reshape(-1, 1)
y = np.sin(X)
y_noised = y + np.random.normal(0, sigma_noise, X.shape)

kr = KernelRidge()

sigma = 0.5
pen = 0.1
K = gaussian_kernel(X, sigma)
a = kr.fit(K, y_noised, pen)

y_predicted = K.dot(a)

plt.scatter(X, y_noised, marker='x', c = 'red')
plt.plot(X, y_predicted)
plt.show()

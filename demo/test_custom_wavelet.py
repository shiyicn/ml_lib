# avoid generate the pycache file
import sys
sys.dont_write_bytecode = True

import numpy as np
import torch
import matplotlib.pyplot as plt
from time import time
import os

# Add path to soft ------------------------------------------------------------
import platform
if platform.system() == 'Windows':
    sys.path.append('C:\\Users\\Huang Zuli\\proj\\ml_lib\\external_lib\\PhaseHarmonic1D_CC')
else:
    sys.path.append('/Users/Huangzuli/proj/ml_lib/external_lib/PhaseHarmonic1D_CC/')
# -----------------------------------------------------------------------------

import scipy as sp
import pandas as pd

from metric import PhaseHarmonicCoeff as PhaseHarmonicPrunedSeparated
#from metric import PhaseHarmonicPrunedSeparated
from simulation.fbm import fbm
from simulation.mrw import mrw, skewed_mrw

# ----------------------------------------------------------------------------
import platform
if platform.system() == 'Windows':
    sys.path.append('C:\\Users\\Huang Zuli\\proj\\')
else:
    sys.path.append('/Users/Huangzuli/proj/')

from ml_lib.signal.scattering import *
from ml_lib.monte_carlo.simulations import *
from ml_lib.signal.sum_dct import *
import ml_lib.signal.filter_bank_generator as fb
import ml_lib.signal.moving_filter as mf
# ----------------------------------------------------------------------------

# -------------------- Set up parameters ----------------------------------- #
do_cuda = False

#----- Analysis parameters -----
T = 2**8  # Data length
J = 3      # Width of low-pass filter / number of scales in wavelet decomposition
Q = 1      # Number of voices per octave

# Parameters for phase harmonic coefficients
deltaj = 2   # Largest scale difference j - j' for harmonic coefficients
num_k_modulus = 2  # Number k of phase harmonics for mixed coefficients
compute_second_order = True

wavelet_type = 'gammatone'
high_freq = 0.425  # for battle_lemarie, ignored for morlet

coeff_select = [['jeqmod', 'harmonic', 'mixed', 'harmod'], ['jeqmod']]

phe_params = {
    'coeff_select': coeff_select,
    'delta_j': deltaj, 'num_k_modulus': num_k_modulus,
    'wav_type': wavelet_type,
    'high_freq' : high_freq,
    'compute_second_order': compute_second_order, 
    'normalize_coefficients_p': True, 
    'remove_mean_p': True, 
    'wav_norm': 'l2'}
# Create analysis object
phi = PhaseHarmonicPrunedSeparated(J, Q, T,  **phe_params)
# -----------------------------------------------------------------------------

# ----------------------------------------------------------------------------
# ---------- Set up the working path -----------------------------------------
if platform.system() == 'Windows':
    datapath2save_root = 'C:\\Users\\Huang Zuli\\proj\\data\\results_produced\\'
    root_path = 'C:\\Users\\Huang Zuli\\proj\\data\\trading\\FXSpot\\'
    tmp_dir_debug = 'C:\\Users\\Huang Zuli\\proj\\tmp\\'
else:
    datapath2save_root = '/Users/Huangzuli/proj/data/res/proc/'
    root_path = '/Users/Huangzuli/proj/data/trading/FXSpot'
    tmp_dir_debug = '/Users/Huangzuli/proj/tmp/'
# ----------------------------------------------------------------------------

x = np.arange(T)
x_mra = mra(x, J, phe_params)

filters = fb.gammatone_psi(T, 1, 1, return_fft = False)

print(filters)
print(x_mra[0, :, :])
print(np.sum(np.roll(x, 1) * np.flip(filters)))

filters = fb.gen_filter_bank(2**8, 4, filter_type=wavelet_type)
fb.plot_wavelet_filters(filters)
filters_padded = fb.pad_zero_neg_axis(filters)

filters_padded_hat = np.fft.fft(filters_padded, axis=1)
filters_padded_hat = np.fft.fftshift(filters_padded_hat, axes=1)

freqs_filters = np.fft.fftfreq(filters_padded_hat.shape[1])
np.fft.fftshift(freqs_filters)

plt.plot(np.fft.fftshift(freqs_filters), filters_padded_hat)


# mf.filtering(x, filters[0, :])

# x_with_date = pd.Series(x)




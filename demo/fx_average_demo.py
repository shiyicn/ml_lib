# avoid generate the pycache file
import sys
import numpy as np
import torch
import matplotlib.pyplot as plt
import matplotlib.backends.backend_pdf
from time import time
import os
import scipy as sp
import pandas as pd

# Add path to soft ------------------------------------------------------------
import platform
if platform.system() == 'Windows':
    sys.path.append('C:\\Users\\Huang Zuli\\scoop\\lib_source\\signals\\PhaseHarmonic1D_CC')
else:
    sys.path.append('/Users/Huangzuli/proj/lib_sources/PhaseHarmonic1D_CC/')

if platform.system() == 'Windows':
    sys.path.append('C:\\Users\\Huang Zuli\\proj\\')
else:
    sys.path.append('/Users/Huangzuli/proj/')

from ml_lib.signal.scattering import *
from ml_lib.signal.convert_date_time import *
from ml_lib.signal.sum_dct import *

from metric import PhaseHarmonicCoeff as PhaseHarmonicPrunedSeparated
# -----------------------------------------------------------------------------

sys.dont_write_bytecode = True

# -------------------- Set up parameters ----------------------------------- #
do_cuda = False

# ----- Analysis parameters -----
T = 2**13  # Data length
J = 7      # Width of low-pass filter / number of scales in wavelet decomposition
Q = 1      # Number of voices per octave

# Parameters for phase harmonic coefficients
deltaj = 2   # Largest scale difference j - j' for harmonic coefficients
num_k_modulus = 2  # Number k of phase harmonics for mixed coefficients
compute_second_order = True

wavelet_type = 'haar'
high_freq = 0.425  # for battle_lemarie, ignored for morlet

coeff_select = [['jeqmod', 'harmonic', 'mixed', 'harmod'], ['jeqmod']]

phe_params = {
    'coeff_select': coeff_select,
    'delta_j': deltaj, 'num_k_modulus': num_k_modulus,
    'wav_type': wavelet_type,
    'high_freq' : high_freq,
    'compute_second_order': compute_second_order, 
    'normalize_coefficients_p': True, 
    'remove_mean_p': True, 
    'wav_norm': 'l2'}
# Create analysis object
phi = PhaseHarmonicPrunedSeparated(J, Q, T,  **phe_params)
# -----------------------------------------------------------------------------

# ----------------------------------------------------------------------------
# ---------- Set up the working path -----------------------------------------
if platform.system() == 'Windows':
    datapath2save_root = 'C:\\Users\\Huang Zuli\\proj\\data\\results_produced\\'
    root_path = 'C:\\Users\\Huang Zuli\\proj\\data\\trading\\FXSpot\\'
else:
    datapath2save_root = '/Users/Huangzuli/proj/data/res/proc/'
    root_path = '/Users/Huangzuli/proj/data/trading/FXSpot'
# ----------------------------------------------------------------------------

# ----------------- Currency Scattering Demo ---------------------------------- 
# Process all available currencies

LOWER_BOUND_DATE = create_date(2000, 1, 1) # start from 01/01/2000

market_types = ['G10', 'EM']
currencies_market = [os.listdir(os.path.join(root_path, market_type)) for market_type in market_types]

max_k_diff = 3

output_csv_path = '/Users/Huangzuli/proj/ml_lib/results/dct_currency_order_1_{}.csv'.format(wavelet_type)

out_csv_file = open(output_csv_path, "w")

head_csv_title = 'Type, log2 (2nd order moment), ,Sparsity, ,Modulus Correlation, ,Mixed Correlation, , Harmonic Correlation' + '\n'
# head_csv_title = 'Type, log2 (2nd order moment), Sparsity, Modulus Correlation, Mixed Correlation, Harmonic Correlation' + '\n'
out_csv_file.write(head_csv_title)

for iter_market, market_type in enumerate(market_types):
    # Set up the coefficients
    modulus_coefs = []
    sparsity_coefs = []
    phase_harmonics_corr_coefs = []
    modulus_corr_coefs = []
    mixed_covariance_coefs = []
    modulus_square_scat = []
    mixed_diff_coefs = []
    dct_coefs = []
    N_SIMULATIONS = len(currencies_market[iter_market])

    datapath2save = os.path.join('real', market_type)
    flag2save = market_type + '_' + 'avg'
        
    flagOfTitle = market_type + ' ' + 'Average'

    path2save_fig = os.path.join(datapath2save_root, datapath2save, 
                                    flag2save + '_' + 'summary.pdf')

    for currency in currencies_market[iter_market]:
         
        currency = currency.replace('.csv', '')
         
        file2load_path = os.path.join(root_path, market_type, currency + '.csv')
        print('Load file from path {}.'.format(file2load_path))

        data_loaded = pd.read_csv(file2load_path)
        data_loaded = filter_by_time(data_loaded['Date'], data_loaded, lower_bound=LOWER_BOUND_DATE)
         
        data_loaded.set_index('Date', inplace=True)
        X_init = np.log(data_loaded['PX_LAST'])
        X_init = (X_init.diff())
         
        # delete NaN from the currency data
        if not np.all(np.isnan(X_init)):
            print('NaN value exists, in total {}.'.format(np.sum(np.isnan(X_init))))
        
        X_dates = X_init.index.values[np.logical_not(np.isnan(X_init))]
        print("Compute From [{}] to [{}].".format(X_dates[0], X_dates[-1]))
        X_init = X_init[np.logical_not(np.isnan(X_init))]
         
        T = len(X_init)
        print('Process {} in {} of length {}.'.format(currency, market_type, T))
         
        phi = PhaseHarmonicPrunedSeparated(J, Q, T,  **phe_params)
        idx_info, y_torch = analyze(X_init, J, phe_params, T = None, Q = 1, do_cuda = False)
        
        results = scatteringMoments(y_torch, idx_info, phi, J-1, order = 1, 
                                    deltaj = 2, set_of_coefs = 'all', 
                                    max_k_diff=max_k_diff)
         
        results['signal'] = X_init
        results['date'] = excel2pandas(X_dates)
        modulus_coefs.append(results['modulus_scat'])
        sparsity_coefs.append(results['sparsity'])
        phase_harmonics_corr_coefs.append(results['phase_harmonics_correlation'])
        modulus_corr_coefs.append(results['modulus_corr'])
        mixed_covariance_coefs.append(results['mixed_covariance'])
        modulus_square_scat.append(results['modulus_square_scat'])
        mixed_diff_coefs.append(results['mixed_by_diff'])

        dct_coef_currency = dct_order(results, J-1, order=2)
        dct_coefs.append(dct_coef_currency)
        output_formatted = market_type + ' ' + currency + format_output_array(dct_coef_currency) + '\n'

        print('Write DCT coefficients : {} to file {} '.format(output_formatted, output_csv_path))
        out_csv_file.write(output_formatted)
    
    coefs_dict = {
        'signal': results['signal'],
        'date': results['date'],
        'modulus_scat': modulus_coefs,
        'sparsity': sparsity_coefs,
        'phase_harmonics_correlation': phase_harmonics_corr_coefs,
        'modulus_corr': modulus_corr_coefs,
        'mixed_covariance': mixed_covariance_coefs,
        'modulus_square_scat': modulus_square_scat,
        'mixed_by_diff': mixed_diff_coefs
    }
    
    generate_report(datapath2save_root, datapath2save, flag2save, 
                    flagOfTitle, N_SIMULATIONS, coefs_dict, J-1, deltaj, 
                    max_k_diff, config_params=phe_params)
    
    output_formatted = market_type + format_output_array(np.mean(dct_coefs, axis=0)) + '\n'

    print('Write DCT coefficients : {} to file {} '.format(output_formatted, output_csv_path))
    out_csv_file.write(output_formatted)

out_csv_file.close()

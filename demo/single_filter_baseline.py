"""
import platform
if platform.system() == 'Windows':
    exec(open("C:/Users/Huang Zuli/proj/ml_lib/signal/scat_basic.py").read())
else:
    exec(open("/Users/huangzuli/proj/ml_lib/signal/scat_basic.py").read())

# set up internal parameters
LEARNING_WINDOW = 6
LOWER_BOUND_DATE = create_date(2000, 1, 1) # start from 01/01/2000
FILTER_LENGTH = 20

market_type = 'G10'
currency = 'USDJPY.csv'

INTERVAL_START = create_date(2000, 1, 1)
INTERVAL_END = create_date(2004, 12, 31)

"""

currency = currency.replace('.csv', '')
print('Start to test the currency {} for baseline trend following.'.format(currency))

flag_of_title = market_type + ' ' + currency
file2load_path = os.path.join(root_path, market_type, currency + '.csv')
print('Load file from path {}.'.format(file2load_path))
 
data_loaded = pd.read_csv(file2load_path)
# filter the time series by the starting time
data_loaded = filter_by_time(data_loaded['Date'], data_loaded, lower_bound=LOWER_BOUND_DATE)

# set the index of loaded time series to excel format date
data_loaded.set_index('Date', inplace=True)
prices = data_loaded['PX_LAST'].to_frame()
returns = get_return(prices, mode='geometry')['PX_LAST'] # convert to daily log-difference return
returns.name = 'Return'

if not np.all(np.isnan(returns)):
    print('NaN value exists in returns, in total {}.'.format(np.sum(np.isnan(returns))))

X_dates = excel2pandas(returns.index)
T = len(returns) # total length of the time series

# calculate the next day return
next_return = returns.shift(periods=-1)
next_return.name = 'Next Return'

vol_spot = mf.volatility(returns)

# ----------------------------------------------------------------------------
# Benchmark 1:
fltrs = [np.ones(FILTER_LENGTH) / np.sqrt(FILTER_LENGTH)]
 
X_filtered = [mf.filtering(returns, fltr, mark_nan = True) for fltr in fltrs]
X_filtered = pd.DataFrame(X_filtered).T
X_filtered.columns = ['Filtered Return']

r_hat_avg = X_filtered.copy()
r_hat_avg.columns = ['Baseline']

## Start to compute the stat
positions_baseline, mu_baseline = generate_position_by_cap(r_hat_avg, 
                                                           returns.to_frame(), 
                                                           cap=3)
pnl_baseline, pnl_spot_baseline = cal_pnl(positions_baseline, returns)
trade_baseline = cal_trade(positions_baseline)

# start to generate the statistics according to the filtering interval
interval_filter = (X_dates >= INTERVAL_START) & (X_dates <= INTERVAL_END)
# filter the result by the time interval
r_hat_filtered = filter_by_time(None, r_hat_avg, filter_bound=interval_filter)
next_return_filtered = filter_by_time(None, next_return, filter_bound=interval_filter)
pnl_spot_baseline_filtered = filter_by_time(None, pnl_spot_baseline, filter_bound=interval_filter)
trade_filtered = filter_by_time(None, trade_baseline, filter_bound=interval_filter)
positions_filtered = filter_by_time(None, positions_baseline, filter_bound=interval_filter)

l2_error_baseline = rmse(r_hat_filtered, next_return_filtered)[0]
accuracy_baseline = hit_rate(r_hat_filtered, next_return_filtered)[0]
sharpe_baseline = cal_sharpe_ratio(pnl_spot_baseline_filtered)
vol_in_sharpe_baseline = np.std(pnl_spot_baseline_filtered.dropna().values) # used for average vol and std of vol
holding_period_baseline = cal_holding_period(positions_filtered, trade_filtered)[0]

if DEBUG_MODE:
    res2save = [data_loaded['PX_LAST'], returns, next_return, X_filtered, vol_spot, 
                mu_baseline, positions_baseline, pnl_spot_baseline, pnl_baseline]
    result2debug = combine(res2save)
    result2debug.to_csv(os.path.join(tmp_dir_debug, 'tf_{}_debug.csv'.format(currency)))


# avoid generate the pycache file
import sys
sys.dont_write_bytecode = True

from tqdm import tqdm
import os

# Add path to soft ------------------------------------------------------------
import platform
if platform.system() == 'Windows':
    sys.path.append('C:\\Users\\Huang Zuli\\scoop\\lib_source\\signals\\pyscatwave\\phaseexp\\')
    sys.path.append('C:\\Users\\Huang Zuli\\scoop\\lib_source\\signals\\pyscatwave\\')
else:
    sys.path.append('/Users/Huangzuli/proj/lib_sources/pyscatwave/phaseexp')
    sys.path.append('/Users/Huangzuli/proj/lib_sources/pyscatwave/')
# -----------------------------------------------------------------------------

import numpy as np
import torch
import matplotlib.pyplot as plt
from time import time
import scipy as sp
import pandas as pd

from metric import PhaseHarmonicPrunedSeparatedFullMix as PhaseHarmonicPrunedSeparated
#from metric import PhaseHarmonicPrunedSeparated
from simulation.fbm import fbm
from simulation.mrw import mrw, skewed_mrw

# ----------------------------------------------------------------------------
import platform
if platform.system() == 'Windows':
    sys.path.append('C:\\Users\\Huang Zuli\\proj\\')
else:
    sys.path.append('/Users/Huangzuli/proj/')

from ml_lib.signal.scattering import *
from ml_lib.monte_carlo.simulations import poisson_proc
# ----------------------------------------------------------------------------

# -------------------- Set up parameters ----------------------------------- #
do_cuda = False

#----- Analysis parameters -----
T = 2**13  # Data length
J = 7      # Width of low-pass filter / number of scales in wavelet decomposition
Q = 1      # Number of voices per octave

# Parameters for phase harmonic coefficients
deltaj = 2   # Largest scale difference j - j' for harmonic coefficients
num_k_modulus = 2  # Number k of phase harmonics for mixed coefficients
compute_second_order = True

wavelet_type = 'morlet'
high_freq = 0.425  # for battle_lemarie, ignored for morlet

coeff_select = [['jeqmod', 'harmonic', 'mixed', 'harmod'], ['jeqmod']]

phe_params = {
    'coeff_select': coeff_select,
    'delta_j': deltaj, 'num_k_modulus': num_k_modulus,
    'wav_type': wavelet_type,
    'high_freq' : high_freq,
    'compute_second_order': compute_second_order, 
    'normalize_coefficients_p': True, 
    'remove_mean_p': True, 
    'wav_norm': 'l2'}
# Create analysis object
phi = PhaseHarmonicPrunedSeparated(J, Q, T,  **phe_params)
# -----------------------------------------------------------------------------

# ----------------------------------------------------------------------------
# ---------- Set up the working path -----------------------------------------
if platform.system() == 'Windows':
    datapath2save_root = 'C:\\Users\\Huang Zuli\\proj\\data\\results_produced\\'
    root_path = 'C:\\Users\\Huang Zuli\\proj\\data\\trading\\FXSpot\\'
else:
    datapath2save_root = '/Users/Huangzuli/proj/data/res/proc/'
    root_path = '/Users/Huangzuli/proj/data/trading/FXSpot'
# ----------------------------------------------------------------------------

# ----------------- Simulation Scattering Demo -------------------------------

N_SIMULATIONS = 2

# Set up the coefficients
modulus_coefs = []
sparsity_coefs = []
phase_harmonics_corr_coefs = []
modulus_corr_coefs = []
mixed_covariance_coefs = []
modulus_square_scat = []

T = 2**13

for kIter in tqdm(range(N_SIMULATIONS)):
    # Simulate a Stochastic Process
    x = np.random.randn(T); datapath2save = 'gn'; flag2save = 'gn_battle'; flagOfTitle = 'GN'
    # x = fbm(T+1, 0.8); x = np.diff(x); datapath2save = 'fbm'; flag2save = 'fbm_lrd'; flagOfTitle = 'FBM'
    # x = fbm(T+1, 0.2); x = np.diff(x); datapath2save = 'fbm; flag2save = 'fbm_srd'; flagOfTitle = 'FBM'
    # x = mrw(T+1, 0.6, 0.2, T); x = np.diff(x); datapath2save = 'mrw'; flag2save = 'mrw'; flagOfTitle = 'MRW'  #  H > 0.5,  lambda in [sqrt(0.01) , sqrt(0.1)]
    # x = poisson_proc(T, 25); x = np.diff(x); datapath2save = 'poisson'; flag2save = 'poisson'; flagOfTitle = 'Poisson'
    # x = skewed_mrw(T+1, 0.4, 0.2, T, K0=0.1, alpha=0.3, beta=0.1); x = np.diff(x); datapath2save = 'smrw'; flag2save = 'smrw_ill'; flagOfTitle = 'SMRW H=0.4'  #  H > 0.5,  lambda in [sqrt(0.01) , sqrt(0.1)]  -- K0 \approx 0.1 -- alpha \approx 0.3 -- beta \approx 0.1
    idx_info, y_torch = analyze(x, J, phe_params, T = None, Q = 1, do_cuda = False)
     
    results = scatteringMoments(y_torch, idx_info, phi, J, order = 1, deltaj = 2, set_of_coefs = 'all')
     
    modulus_coefs.append(results['modulus_scat'])
    sparsity_coefs.append(results['sparsity'])
    phase_harmonics_corr_coefs.append(results['phase_harmonics_correlation'])
    modulus_corr_coefs.append(results['modulus_corr'])
    mixed_covariance_coefs.append(results['mixed_covariance'])
    modulus_square_scat.append(results['modulus_square_scat'])

# ----------------------------------------------------------------------------
# Plot and Post-process the obtained results
# ----------------------------------------------------------------------------

# check if a file path exists
path_temp = os.path.join(datapath2save_root, datapath2save)
if not os.path.isdir(path_temp):
    os.mkdir(path_temp)

# ----------------------------------------------------------------------------
# visualize the process 
# plot initial signal
path2save_fig = os.path.join(datapath2save_root, datapath2save, flag2save + '_' + 'proc.pdf')
plot_scattering_signal(x, flagOfTitle=flagOfTitle, path2save_fig = path2save_fig)

# ----------------------------------------------------------------------------
# visualize the result of 
path2save_fig = os.path.join(datapath2save_root, datapath2save, flag2save + '_' + 'modulus_square.pdf')
plot_scattering_second_moment(modulus_square_scat, J, N_SIMULATIONS, 
                            flagOfTitle = flagOfTitle, path2save_fig = path2save_fig)
# ----------------------------------------------------------------------------

# ----------------------------------------------------------------------------
# plot sparse coefficients
path2save_fig = os.path.join(datapath2save_root, datapath2save, flag2save + '_' + 'sparsity.pdf')
plot_sparsity(sparsity_coefs, J, N_SIMULATIONS, 
                            flagOfTitle = flagOfTitle, path2save_fig = path2save_fig)
# ----------------------------------------------------------------------------

# ----------------------------------------------------------------------------
# plot modulus correlation coefficients
path2save_fig = os.path.join(datapath2save_root, datapath2save, flag2save + '_' + 'corr_modulus.pdf')
plot_modulus_corr(modulus_corr_coefs, deltaj, N_SIMULATIONS, 
                flagOfTitle = flagOfTitle, path2save_fig = path2save_fig)
# ----------------------------------------------------------------------------

# ----------------------------------------------------------------------------
# plot phase harmonics, correlation
# plot the real and imaginary part of the correlation
    
REAL_IMAG = ['Real', 'Imag']

path2save_fig = os.path.join(datapath2save_root, datapath2save, flag2save + '_' + 'corr_harmonic.pdf')
plot_harmonic_corr(phase_harmonics_corr_coefs, deltaj, N_SIMULATIONS, 
                flagOfTitle = flagOfTitle, path2save_fig = path2save_fig)
# ----------------------------------------------------------------------------

# ----------------------------------------------------------------------------
# plot mixed correlation coefficients according to j'
path2save_fig = os.path.join(datapath2save_root, datapath2save, flag2save + '_' + 'corr_mixed.pdf')
plot_mixed_corr(mixed_covariance_coefs, N_SIMULATIONS, J, 
            flagOfTitle = flagOfTitle, path2save_fig = path2save_fig, 
            offset = -1, y_limit = [-1., 1.])

# ----------------------------------------------------------------------------
# visualize the result of 
path2save_fig = os.path.join(datapath2save_root, datapath2save, flag2save + '_' + 'modulus.pdf')
plot_scattering_first_order_modulus(modulus_coefs, N_SIMULATIONS, J, 
                                flagOfTitle = flagOfTitle, path2save_fig = path2save_fig)
# ----------------------------------------------------------------------------

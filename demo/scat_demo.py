# avoid generate the pycache file
import sys
sys.dont_write_bytecode = True

import numpy as np
import torch
import matplotlib.pyplot as plt
from time import time
import os

# Add path to soft ------------------------------------------------------------
import platform
if platform.system() == 'Windows':
    sys.path.append('C:\\Users\\Huang Zuli\\proj\\ml_lib\\external_lib\\PhaseHarmonic1D_CC')
else:
    sys.path.append('/Users/Huangzuli/proj/ml_lib/external_lib/PhaseHarmonic1D_CC/')
# -----------------------------------------------------------------------------

import scipy as sp
import pandas as pd

from metric import PhaseHarmonicCoeff as PhaseHarmonicPrunedSeparated
#from metric import PhaseHarmonicPrunedSeparated
from simulation.fbm import fbm
from simulation.mrw import mrw, skewed_mrw

# ----------------------------------------------------------------------------
import platform
if platform.system() == 'Windows':
    sys.path.append('C:\\Users\\Huang Zuli\\proj\\')
else:
    sys.path.append('/Users/Huangzuli/proj/')

from ml_lib.signal.scattering import *
from ml_lib.monte_carlo.simulations import *
from ml_lib.signal.sum_dct import *
# ----------------------------------------------------------------------------

# -------------------- Set up parameters ----------------------------------- #
do_cuda = False

#----- Analysis parameters -----
T = 2**13  # Data length
J = 7      # Width of low-pass filter / number of scales in wavelet decomposition
Q = 1      # Number of voices per octave

# Parameters for phase harmonic coefficients
deltaj = 2   # Largest scale difference j - j' for harmonic coefficients
num_k_modulus = 2  # Number k of phase harmonics for mixed coefficients
compute_second_order = True

wavelet_type = 'gammatone'
high_freq = 0.425  # for battle_lemarie, ignored for morlet

coeff_select = [['jeqmod', 'harmonic', 'mixed', 'harmod'], ['jeqmod']]

phe_params = {
    'coeff_select': coeff_select,
    'delta_j': deltaj, 'num_k_modulus': num_k_modulus,
    'wav_type': wavelet_type,
    'high_freq' : high_freq,
    'compute_second_order': compute_second_order, 
    'normalize_coefficients_p': True, 
    'remove_mean_p': True, 
    'wav_norm': 'l2'}
# Create analysis object
phi = PhaseHarmonicPrunedSeparated(J, Q, T,  **phe_params)
# -----------------------------------------------------------------------------

# ----------------------------------------------------------------------------
# ---------- Set up the working path -----------------------------------------
if platform.system() == 'Windows':
    datapath2save_root = 'C:\\Users\\Huang Zuli\\proj\\data\\results_produced\\'
    root_path = 'C:\\Users\\Huang Zuli\\proj\\data\\trading\\FXSpot\\'
else:
    datapath2save_root = '/Users/Huangzuli/proj/data/res/proc/'
    root_path = '/Users/Huangzuli/proj/data/trading/FXSpot'
# ----------------------------------------------------------------------------

# ----------------- Simulation Scattering Demo -------------------------------

N_SIMULATIONS = 100

max_k_diff = 3

# Set up the coefficients
modulus_coefs = []
sparsity_coefs = []
phase_harmonics_corr_coefs = []
modulus_corr_coefs = []
mixed_covariance_coefs = []
modulus_square_scat = []
mixed_diff_coefs = []

#T = 5000

dct_coefs_result = []

for kIter in range(N_SIMULATIONS):
    # x_signed = 2 * (np.random.random(T) <= 0.5) - 1
    # Simulate a Stochastic Process
    # x = np.random.randn(T); datapath2save = 'gn'; flag2save = 'gn ' + wavelet_type; flagOfTitle = 'GN'
    # x = fbm(T+1, 0.8); x = np.diff(x); datapath2save = 'fbm'; flag2save = 'fbm_lrd'; flagOfTitle = 'FBM H = 0.8'
    # H_exp_hur = 0.8; x = fbm(T+1, H_exp_hur); x = np.diff(x); datapath2save = 'fbm'; flag2save = 'fbm '+wavelet_type; flagOfTitle = 'FBM H = ' + str(H_exp_hur)
    # x = mrw(T+1, 0.6, 0.2, T); x = np.diff(x); datapath2save = 'mrw'; flag2save = 'mrw'; flagOfTitle = 'MRW' #  H > 0.5,  lambda in [sqrt(0.01) , sqrt(0.1)]
    x = poisson_proc(T, 25); x = np.diff(x); datapath2save = ''; flag2save = 'poisson '+wavelet_type; flagOfTitle = 'Poisson'
    # x = skewed_mrw(T+1, 0.6, 0.2, T, K0=0.1, alpha=0.3, beta=0.1); x = np.diff(x); datapath2save = 'smrw'; flag2save = 'smrw ' + wavelet_type; flagOfTitle = 'SMRW H=0.6'  #  H > 0.5,  lambda in [sqrt(0.01) , sqrt(0.1)]  -- K0 \approx 0.1 -- alpha \approx 0.3 -- beta \approx 0.1
    # x = sign_proc(T, 25, p = 0.5); x = np.diff(x); datapath2save = 'sgn_poisson'; flag2save = 'sgn_poisson'; flagOfTitle = 'Sign Poisson'
    # x = piece_wise_constant(T, fixed_duration=22); x = np.diff(x); datapath2save = 'piece_wise_constant'; flag2save = 'piece_wise_constant_fixed'; flagOfTitle = 'Piece Wise Const Duration 20'
    # x = periodic_cosine(T, period=5, upper_phase=np.pi); x = np.diff(x); datapath2save = 'periodic_cos'; flag2save = 'periodic_cos_5'; flagOfTitle = 'Periodic Cosine Period 5'
    # x = random_phase(T); x = np.diff(x); datapath2save = ''; flag2save = 'random_phase'; flagOfTitle = 'Random Phase'
    # x = np.random.random(T); datapath2save = ''; flag2save = 'uniform'; flagOfTitle = 'uniform'
    
    # x = x_signed * x

    datapath2save = 'summary' # common path to save summary result

    print('Compute iteration [{}/{}] for {} .'.format(kIter, N_SIMULATIONS, flagOfTitle))
    
    idx_info, y_torch = analyze(x, J, phe_params, T = None, Q = 1, do_cuda = False)
    # wav_coef = mra(x, J, phe_params)
     
    results = scatteringMoments(y_torch, idx_info, phi, J, order = 1, 
                                deltaj = 2, set_of_coefs = 'all', 
                                max_k_diff=max_k_diff)
    
    dct_coefs_result.append(dct_order(results, J)) 
     
    modulus_coefs.append(results['modulus_scat'])
    sparsity_coefs.append(results['sparsity'])
    phase_harmonics_corr_coefs.append(results['phase_harmonics_correlation'])
    modulus_corr_coefs.append(results['modulus_corr'])
    mixed_covariance_coefs.append(results['mixed_covariance'])
    mixed_diff_coefs.append(results['mixed_by_diff'])
    modulus_square_scat.append(results['modulus_square_scat'])

# ----------------------------------------------------------------------------
# Plot and Post-process the obtained results
# ----------------------------------------------------------------------------
coefs_dict = {
        'signal' : x, 
        'date' : None, 
        'modulus_scat' : modulus_coefs, 
        'sparsity' : sparsity_coefs, 
        'phase_harmonics_correlation' : phase_harmonics_corr_coefs, 
        'modulus_corr' : modulus_corr_coefs, 
        'mixed_covariance' : mixed_covariance_coefs, 
        'modulus_square_scat' : modulus_square_scat, 
        'mixed_by_diff' : mixed_diff_coefs
    }

generate_report(datapath2save_root, datapath2save, flag2save, 
            flagOfTitle, N_SIMULATIONS, coefs_dict, J, deltaj, max_k_diff, 
            config_params=phe_params)

dct_coefs_result = np.array(dct_coefs_result)
dct_coefs_avg = np.mean(dct_coefs_result, axis=0)

print('Result for the simulation {}.'.format(flagOfTitle))
print(format_output_array(dct_coefs_avg))


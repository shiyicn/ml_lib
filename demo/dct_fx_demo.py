# avoid generate the pycache file
import sys
sys.dont_write_bytecode = True

import numpy as np
import torch
import matplotlib.pyplot as plt
from time import time
import os

# Add path to soft ------------------------------------------------------------
import platform
if platform.system() == 'Windows':
    sys.path.append('C:\\Users\\Huang Zuli\\scoop\\lib_source\\signals\\PhaseHarmonic1D_CC')
else:
    sys.path.append('/Users/Huangzuli/proj/lib_sources/PhaseHarmonic1D_CC/')
# -----------------------------------------------------------------------------

import scipy as sp
import pandas as pd

from metric import PhaseHarmonicCoeff as PhaseHarmonicPrunedSeparated
#from metric import PhaseHarmonicPrunedSeparated
from simulation.fbm import fbm
from simulation.mrw import mrw, skewed_mrw

# ----------------------------------------------------------------------------
import platform
if platform.system() == 'Windows':
    sys.path.append('C:\\Users\\Huang Zuli\\proj\\')
else:
    sys.path.append('/Users/Huangzuli/proj/')

from ml_lib.signal.scattering import *
from ml_lib.monte_carlo.simulations import poisson_proc
from ml_lib.signal.sum_dct import dct_summarizer, filter_dct
from ml_lib.trading.lib import combine
# ----------------------------------------------------------------------------

# -------------------- Set up parameters ----------------------------------- #
do_cuda = False

#----- Analysis parameters -----
T = 2**13  # Data length
J = 7      # Width of low-pass filter / number of scales in wavelet decomposition
Q = 1      # Number of voices per octave

# Parameters for phase harmonic coefficients
deltaj = 2   # Largest scale difference j - j' for harmonic coefficients
num_k_modulus = 2  # Number k of phase harmonics for mixed coefficients
compute_second_order = True

wavelet_type = 'morlet'
high_freq = 0.425  # for battle_lemarie, ignored for morlet

coeff_select = [['jeqmod', 'harmonic', 'mixed', 'harmod'], ['jeqmod']]

phe_params = {
    'coeff_select': coeff_select,
    'delta_j': deltaj, 'num_k_modulus': num_k_modulus,
    'wav_type': wavelet_type,
    'high_freq' : high_freq,
    'compute_second_order': compute_second_order, 
    'normalize_coefficients_p': True, 
    'remove_mean_p': True, 
    'wav_norm': 'l2'}
# Create analysis object
phi = PhaseHarmonicPrunedSeparated(J, Q, T,  **phe_params)
# -----------------------------------------------------------------------------

# ----------------------------------------------------------------------------
# ---------- Set up the working path -----------------------------------------
if platform.system() == 'Windows':
    datapath2save_root = 'C:\\Users\\Huang Zuli\\proj\\data\\results_produced\\'
    root_path = 'C:\\Users\\Huang Zuli\\proj\\data\\trading\\FXSpot\\'
else:
    datapath2save_root = '/Users/Huangzuli/proj/data/res/proc/'
    root_path = '/Users/Huangzuli/proj/data/trading/FXSpot'
# ----------------------------------------------------------------------------

# ----------------- Currency Scattering Demo ---------------------------------- 
# Process all available currencies

market_types = ['G10', 'EM']
currencies_market = [os.listdir(os.path.join(root_path, market_type)) for market_type in market_types]

# create a dictionary to store 
dct_result_fx = {}
for dct_index in range(2):
    dct_result_fx[dct_index] = pd.DataFrame()

for iter_market, market_type in enumerate(market_types):
    for currency in currencies_market[iter_market]:
        N_SIMULATIONS = 4
         
        # Set up the coefficients
        modulus_coefs = []
        sparsity_coefs = []
        phase_harmonics_corr_coefs = []
        modulus_corr_coefs = []
        mixed_covariance_coefs = []
        modulus_square_scat = []
         
        currency = currency.replace('.csv', '')
        print('Process {} in {} .'.format(currency, market_type))
         
        file2load_path = os.path.join(root_path, market_type, currency + '.csv')
        data_loaded = pd.read_csv(file2load_path)
         
        X_init = np.log(data_loaded['PX_LAST'].values)
        X_init = np.diff(X_init)
         
        # delete NaN from the currency data
        if np.sum(np.isnan(X_init)) != 0:
            X_init = X_init[np.logical_not(np.isnan(X_init))]
         
        datapath2save = os.path.join('real',market_type, currency)
        flag2save = market_type + '_' + currency
         
        flagOfTitle = currency
         
        for x in cutProcessInTime(X_init, nCut=N_SIMULATIONS):
            T = len(x)
            phi = PhaseHarmonicPrunedSeparated(J, Q, T,  **phe_params)
            idx_info, y_torch = analyze(x, J, phe_params, T = None, Q = 1, do_cuda = False)
            
            results = scatteringMoments(y_torch, idx_info, phi, J, order = 1, deltaj = 2, set_of_coefs = 'all')
            results['signal'] = x
            modulus_coefs.append(results['modulus_scat'])
            sparsity_coefs.append(results['sparsity'])
            phase_harmonics_corr_coefs.append(results['phase_harmonics_correlation'])
            modulus_corr_coefs.append(results['modulus_corr'])
            mixed_covariance_coefs.append(results['mixed_covariance'])
            modulus_square_scat.append(results['modulus_square_scat'])
          
        for dct_index in range(2):
            # start to compute the DCT coefficients
            modulus_square_scat_avg = np.log2(
                                        np.mean(modulus_square_scat, axis=0)[:J]
                                        )
            m2_avg_dct = dct_summarizer(modulus_square_scat_avg)
             
            sparsity_avg = np.mean(sparsity_coefs, axis=0)[:J]
            sparsity_avg_dct = dct_summarizer(1.0 / sparsity_avg)
             
            modulus_corr_dct = {}
             
            for dj in range(deltaj):
                # remove the last scale
                modulus_corr_dj = [x[dj][1][:-1, 0] for x in modulus_corr_coefs]
                modulus_corr_dj = np.array(modulus_corr_dj)
                modulus_corr_dj_avg = np.mean(modulus_corr_dj, axis=0)
                modulus_corr_dj_avg_dct = dct_summarizer(modulus_corr_dj_avg)
                modulus_corr_dct[dj+1] = modulus_corr_dj_avg_dct
              
            for dct_index in range(2):
                dct_result_fx[dct_index][currency] = [
                                                m2_avg_dct[dct_index], 
                                                sparsity_avg_dct[dct_index], 
                                                modulus_corr_dct[1][dct_index]
                                                ]

index_dct_names = {0:'Moment2', 1:'Sparsity', 2:'Modulus Correlation k = 1'}

for dct_index in range(2):
    dct_result_fx[dct_index].rename(index=index_dct_names, inplace=True)

for dct_index in range(2):
    path2save_csv = os.path.join(datapath2save_root, 'real', 'dct' + '_' + str(dct_index) + '.csv')
    dct_result_fx[dct_index].T.to_csv(path2save_csv, sep=';')

# combine the dct result
res_in_list = [dct_result_fx[dct_index].T for dct_index in range(2)]
res_dct = pd.concat(res_in_list, axis=1)

# avoid generate the pycache file
import sys
sys.dont_write_bytecode = True

import numpy as np
import torch
import matplotlib.pyplot as plt
from time import time
import os


# Add path to soft -----------------------------------------------------------
import platform
if platform.system() == 'Windows':
    print('Windows soft path need to be appended manually.')
else:
    sys.path.append('/Users/Huangzuli/proj/lib_sources/PhaseHarmonic1D_CC/')
# ----------------------------------------------------------------------------

import scipy as sp
import pandas as pd

from metric import PhaseHarmonicCoeff as PhaseHarmonicPrunedSeparated
#from metric import PhaseHarmonicPrunedSeparated
from simulation.fbm import fbm
from simulation.mrw import mrw, skewed_mrw

# ----------------------------------------------------------------------------
import platform
if platform.system() == 'Windows':
    sys.path.append('C:\\Users\\Huang Zuli\\proj\\')
else:
    sys.path.append('/Users/Huangzuli/proj/')

from ml_lib.signal.scattering import *
from ml_lib.monte_carlo.simulations import poisson_proc
from ml_lib.signal.sum_dct import dct_summarizer, filter_dct
# ----------------------------------------------------------------------------

# -------------------- Set up parameters ----------------------------------- #
do_cuda = False

#----- Analysis parameters -----
T = 2**13  # Data length
J = 7      # Width of low-pass filter / number of scales in wavelet decomposition
Q = 1      # Number of voices per octave

# Parameters for phase harmonic coefficients
deltaj = 2   # Largest scale difference j - j' for harmonic coefficients
num_k_modulus = 2  # Number k of phase harmonics for mixed coefficients
compute_second_order = True

wavelet_type = 'morlet'
high_freq = 0.425  # for battle_lemarie, ignored for morlet

coeff_select = [['jeqmod', 'harmonic', 'mixed', 'harmod'], ['jeqmod']]

phe_params = {
    'coeff_select': coeff_select,
    'delta_j': deltaj, 'num_k_modulus': num_k_modulus,
    'wav_type': wavelet_type,
    'high_freq' : high_freq,
    'compute_second_order': compute_second_order, 
    'normalize_coefficients_p': True, 
    'remove_mean_p': True, 
    'wav_norm': 'l2'}
# Create analysis object
phi = PhaseHarmonicPrunedSeparated(J, Q, T,  **phe_params)
# -----------------------------------------------------------------------------

# ----------------------------------------------------------------------------
# ---------- Set up the working path -----------------------------------------
if platform.system() == 'Windows':
    datapath2save_root = 'C:\\Users\\Huang Zuli\\proj\\data\\results_produced\\'
    root_path = 'C:\\Users\\Huang Zuli\\proj\\data\\trading\\FXSpot\\'
else:
    datapath2save_root = '/Users/Huangzuli/proj/data/res/proc/'
    root_path = '/Users/Huangzuli/proj/data/trading/FXSpot'
# ----------------------------------------------------------------------------

# ----------------- Simulation Scattering Demo -------------------------------

N_SIMULATIONS = 10

# Set up the coefficients
modulus_coefs = []
sparsity_coefs = []
phase_harmonics_corr_coefs = []
modulus_corr_coefs = []
mixed_covariance_coefs = []
modulus_square_scat = []

T = 2**13

dct_result = pd.DataFrame()

for kIter in tqdm(range(N_SIMULATIONS)):
    # Simulate a Stochastic Process
    x = np.random.randn(T); datapath2save = 'gn'; flag2save = 'gn'; flagOfTitle = 'GN'
    # x = fbm(T+1, 0.8); x = np.diff(x); datapath2save = 'fbm'; flag2save = 'fbm_lrd'; flagOfTitle = 'FBM'
    # x = fbm(T+1, 0.2); x = np.diff(x); datapath2save = 'fbm; flag2save = 'fbm_srd'; flagOfTitle = 'FBM'
    # x = mrw(T+1, 0.6, 0.2, T); x = np.diff(x); datapath2save = 'mrw'; flag2save = 'mrw'; flagOfTitle = 'MRW'  #  H > 0.5,  lambda in [sqrt(0.01) , sqrt(0.1)]
    # x = poisson_proc(T, 25);# x = np.diff(x); datapath2save = 'poisson'; flag2save = 'poisson'; flagOfTitle = 'Poisson'
    # x = skewed_mrw(T+1, 0.4, 0.2, T, K0=0.1, alpha=0.3, beta=0.1); x = np.diff(x); datapath2save = 'smrw'; flag2save = 'smrw_ill'; flagOfTitle = 'SMRW H=0.4'  #  H > 0.5,  lambda in [sqrt(0.01) , sqrt(0.1)]  -- K0 \approx 0.1 -- alpha \approx 0.3 -- beta \approx 0.1
    idx_info, y_torch = analyze(x, J, phe_params, T = None, Q = 1, do_cuda = False)
     
    results = scatteringMoments(y_torch, idx_info, phi, J, order = 1, deltaj = 2, set_of_coefs = 'all')
     
    modulus_coefs.append(results['modulus_scat'])
    sparsity_coefs.append(results['sparsity'])
    phase_harmonics_corr_coefs.append(results['phase_harmonics_correlation'])
    modulus_corr_coefs.append(results['modulus_corr'])
    mixed_covariance_coefs.append(results['mixed_covariance'])
    modulus_square_scat.append(results['modulus_square_scat'])

# start to compute the DCT coefficients
modulus_square_scat_avg = np.log2(
                            np.mean(modulus_square_scat, axis=0)[:J]
                            )
m2_avg_dct = dct_summarizer(modulus_square_scat_avg)

sparsity_avg = np.mean(sparsity_coefs, axis=0)[:J]
sparsity_avg_dct = dct_summarizer(sparsity_avg)

modulus_corr_dct = {}

for dj in range(deltaj):
    # remove the last scale
    modulus_corr_dj = [x[dj][1][:-1, 0] for x in modulus_corr_coefs]
    modulus_corr_dj = np.array(modulus_corr_dj)
    modulus_corr_dj_avg = np.mean(modulus_corr_dj, axis=0)
    modulus_corr_dj_avg_dct = dct_summarizer(modulus_corr_dj_avg)
    modulus_corr_dct[dj+1] = modulus_corr_dj_avg_dct


dct_result['Moment2'] = m2_avg_dct
dct_result['Sparsity'] = sparsity_avg_dct
for dj in range(deltaj):
    dct_result['Modulus Correlation k = ' + str(dj+1)] = modulus_corr_dct[dj+1]

path2save_csv = os.path.join(datapath2save_root, datapath2save, flag2save + '_' + 'dct.csv')
dct_result.to_csv(path2save_csv, sep = ';')

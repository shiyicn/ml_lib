corr_history = []

for i in range(100):
    # print('Iter [{}]'.format(i))
    x = sign_proc(T, 25); x = np.diff(x)
    # x = poisson_proc(T, 25); x = np.diff(x)
    x_mra = mra(x, J, phe_params)
    x_mra = x_mra[..., 0] + x_mra[..., 1] * 1j
    corr_current = np.corrcoef(
                    phase_harmonic_transform(x_mra[0, :], order = 0), 
                    phase_harmonic_transform(x_mra[0, :], order = -2))
    
    corr_history.append(corr_current[0, 1])


print(np.sum(corr_history) / 100)


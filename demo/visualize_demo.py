import platform

from sklearn.decomposition import PCA
from sklearn.preprocessing import StandardScaler
import pandas as pd
import numpy as np
import os

import seaborn as sns
sns.set(style="darkgrid")

import matplotlib.pyplot as plt

output_csv_path = '/Users/Huangzuli/proj/ml_lib/results/dct_currency_order_1_gammatone.csv'

# ---------- Set up the working path -----------------------------------------
if platform.system() == 'Windows':
    datapath2save_root = 'C:\\Users\\Huang Zuli\\proj\\data\\results_produced\\'
    root_path = 'C:\\Users\\Huang Zuli\\proj\\data\\trading\\FXSpot\\'
else:
    datapath2save_root = '/Users/Huangzuli/proj/data/res/proc/'
    root_path = '/Users/Huangzuli/proj/data/trading/FXSpot'
# ----------------------------------------------------------------------------

dct_currencies = pd.read_csv(output_csv_path, index_col=False)
dct_currencies.set_index('Type', inplace=True)
currency_name = dct_currencies.index.values
filter_market = ((currency_name != 'EM') & (currency_name != 'G10'))
dct_currencies = dct_currencies.iloc[filter_market, :]

x = StandardScaler().fit_transform(dct_currencies)

scaling_factor = np.ones(x.shape[1])
_index = (dct_currencies.columns.values == 'Sparsity')
scaling_factor[_index] = 16.0
_index = (dct_currencies.columns.values == 'Modulus Correlation')
scaling_factor[_index] = 10.0

dct_currencies = dct_currencies / scaling_factor.reshape(1, -1)

csv2save_path = os.path.join(datapath2save_root, 'scaled_currencies.csv')
dct_currencies.to_csv()

scaled_dct_currencies = np.mean(dct_currencies, axis=0)

total_num_of_components = x.shape[1]

pca = PCA(n_components=total_num_of_components)
principalComponents = pca.fit_transform(x)

dct_currencies['P1'] = principalComponents[:, 0]
dct_currencies['P2'] = principalComponents[:, 1]

market_types = ['G10', 'EM']
market_flag = ['EM' in cur for cur in dct_currencies.index.values]
market_flag = np.array(market_flag)
market_flag = market_flag.astype(int)

colors_set = np.array(['red', 'green'])

fig, ax = plt.subplots()
for iter_index, color in enumerate(colors_set):
    ax.scatter(dct_currencies['P1'][iter_index == market_flag], 
                dct_currencies['P2'][iter_index == market_flag], 
                c = color, 
                label = market_types[iter_index])

for i, txt in enumerate(dct_currencies.index.values):
    txt = txt.replace('EM ', '')
    txt = txt.replace('G10 ', '')
    ax.annotate(txt, (dct_currencies['P1'][i], dct_currencies['P2'][i]), size = 5)

# plot the first 2 principal components
ax.set_title('First 2 Principal Components')
ax.set_xlabel('PC1')
ax.set_ylabel('PC2')
ax.legend()
file2save_path = os.path.join(datapath2save_root, 'pca.pdf')
fig.savefig(file2save_path)
fig.show()

# plot the eigenvalues for each principal components
eigenvalues = pca.explained_variance_
n_features = eigenvalues.shape[0]
fig, ax = plt.subplots()
ax.plot(np.arange(n_features)+1, eigenvalues)
ax.set_title('Eigenvalues of PCA')
ax.xaxis.set_ticks(np.arange(n_features)+1)
file2save_path = os.path.join(datapath2save_root, 'eig.pdf')
fig.savefig(file2save_path)

fig, ax = plt.subplots()
coeff_tmp = pca.components_[:2, :].T
features_name = dct_currencies.columns.values
for iter_index in range(total_num_of_components):
    ax.arrow(0, 0, coeff_tmp[iter_index, 0], coeff_tmp[iter_index, 1], 
    color = 'r', width = .002, head_width = .015)
    txt = features_name[iter_index]
    ax.annotate(txt, (coeff_tmp[iter_index, 0], coeff_tmp[iter_index, 1]), size = 5)
ax.scatter(coeff_tmp[:, 0], coeff_tmp[:, 1], s = 1)
file2save_path = os.path.join(datapath2save_root, 'contribution.pdf')
fig.savefig(file2save_path)
fig.show()

fig, ax = plt.subplots()
vratio  = pca.explained_variance_ratio_
ax.plot(np.arange(vratio.shape[0])+1, np.cumsum(vratio))
ax.xaxis.set_ticks(np.arange(vratio.shape[0])+1)
ax.set_title('Explained Variance Ratio')
file2save_path = os.path.join(datapath2save_root, 'vratio.pdf')
fig.savefig(file2save_path)
fig.show()

